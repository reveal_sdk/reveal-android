package com.stepleaderdigital.reveal;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by bobby on 5/5/16.
 */
public class RevealLocationService extends LocationCallback
        implements Reveal.LocationService,
                   GoogleApiClient.ConnectionCallbacks,
                   GoogleApiClient.OnConnectionFailedListener
{

    // Constant for amount of time we allow locations to be stale for (in minutes)
    public double locationValidTime = 60.0;

    private Location currentLocation;
    private Location lastLocationReported;
    private double locationTime = 0;

    private LocationRequest locationRequest;
    private PendingResult<Status> locationResult;
    private GoogleApiClient googleApiClient;
    private Context applicationContext;
    private ScheduledExecutorService noLocationTimerExecutionScheduler = Executors.newScheduledThreadPool(1);
    private Reveal.LocationService.OnValidLocationCallback onValidLocationCallback;
    private boolean locationDiscoveryActive = false;

    private ArrayList<Reveal.LocationService.OnValidLocationCallback> pendingLocationCallbacks = new ArrayList<Reveal.LocationService.OnValidLocationCallback>();

    /**
     * Get the current location
     *
     *  @param context The current Android context
     *
     * @return the location
     */
    public Location getCurrentLocation(Context context ) {
        return currentLocation;
    }

    /**
     *  Start monitoring location services. Note that the passed in
     *  context will be saved, so do not use a transient context, if
     *  in question , use the application context.
     *
     *  @param context The application context
     */
    public void startLocationMonitoring( Context context ) {
        applicationContext = context;
        if (Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_FINE_LOCATION)
                || Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_COARSE_LOCATION) ) {
            Reveal.getInstance().setLocationSharingEnabled(true);
        }

        if ( googleApiClient == null )
            startGoogleApi( context );
        else
            internalStartLocationMonitoring( context );
        }

    /**
     *  stop monitoring location changes
     *
     *  @param context The current Android context
     */
    public void stopLocationMonitoring( Context context ) {
        if ( googleApiClient != null ) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
        else {
            Reveal.log( "Attempting to stop location monitoring with no Google API available enabled=" + Reveal.getInstance().getLocationSharingEnabled().toString(), "WARNING", "LOCATION" );

            if ( locationRequest != null )
                Reveal.log( "       locationRequest: " + locationRequest.toString(), "WARNING", "LOCATION" );
        }

        // if we have no location service then assume we need to clear all the elements
        // this is a bit of a stop gap
        locationRequest = null;
        googleApiClient = null;
        Reveal.getInstance().setLocationSharingEnabled(false);
        Reveal.log("Stop monitoring location", "STATE");

        this.locationDiscoveryActive = false;
    }

    /**
     *  Allows functions that need a valid location to wait for a valid location to be available (address if possible)
     *  If there is already a valid location available, then the callback returns immediately, otherwise, the callback waits until
     *  there is a valid location or a timeout, in which case the best location we can find will be used
     *
     *  @param callback The method to call when a valid location is available
     */
    public void waitForValidLocation( Reveal.LocationService.OnValidLocationCallback callback ) {
        if ( isLocationCurrent() ) {
            Reveal.log( "We have a current location so no need to wait", "STATE" );
            if ( callback != null )
                callback.onLocationFound();
        }
        else {
            if ( callback != null ) {
                Reveal.log( "We need to request a new location", "STATE");

                synchronized( this ) {
                    pendingLocationCallbacks.add(callback);
                }

                timeOut(locationValidTime, new Reveal.LocationService.OnValidLocationCallback() {
                    @Override
                    public void onLocationFound() {
                        RevealLocationService.this.firePendingCallbacks();
                    }
                });
            }
        }
    }

    private boolean isLocationCurrent() {
        boolean result = false;
        double now = System.currentTimeMillis() / 1000.0;

        if ( ( now - locationTime ) < locationValidTime )
            result = true;

        return result;
    }

    private synchronized void internalStartLocationMonitoring( Context context ) {
        this.locationRequest = new LocationRequest();
        this.locationRequest.setInterval( 5 * 60 * 1000 );
        this.locationRequest.setFastestInterval( 1 * 60 * 1000);
        this.locationRequest.setSmallestDisplacement( 100.0F );
        this.locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);

        if ( this.googleApiClient.isConnected() ) {

            try {
                if (Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_FINE_LOCATION)
                        || Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_COARSE_LOCATION)  ) {
                    this.locationResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                            this.googleApiClient, this.locationRequest, this, Looper.getMainLooper());
                    Reveal.log( "Start monitoring location", "STATE" );
                }
            } catch (IllegalStateException except) {
                Reveal.log("Google API connection has not been established yet, locations will not be provided until a connection has been established",
                        "WARNING", "STATE" );
            } catch (SecurityException securityException) {
                Reveal.log("Old android permissions scheme used to setup permissions and not handling shutoff of permissions properly in app.. stopping location monitoring",
                        "WARNING", "STATE" );
                this.stopLocationMonitoring(context);
            }
        }
    }

    /// Start up google play services location notifications
    private synchronized void startGoogleApi( Context context ) {
        Reveal.log( "Starting Google location services", "STATE" );
        this.googleApiClient = new GoogleApiClient.Builder( context )
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        this.googleApiClient.connect();
    }

    private void firePendingCallbacks() {
        ArrayList<Reveal.LocationService.OnValidLocationCallback> callbacks = null;

        synchronized ( this ) {
            callbacks = pendingLocationCallbacks;
            pendingLocationCallbacks = new ArrayList<Reveal.LocationService.OnValidLocationCallback>();
        }

        if ( !isLocationCurrent() && (callbacks.size() > 0) ) {
            // This always gets called even on a success, so it is only a time out if we didn't
            // get a valid location before it gets here, otherwise no need to report it
            Reveal.log( "Waiting for location timed out without a valid location", "STATE" );
        }

        for(Reveal.LocationService.OnValidLocationCallback callback : callbacks ) {
            if ( callback != null )
                callback.onLocationFound();
        }

        Boolean needUpdate = true;

        if ( ( this.currentLocation != null ) && ( this.lastLocationReported != null ) ) {
            if ( this.currentLocation.distanceTo( this.lastLocationReported ) < 100.0 ) {
                needUpdate = false;

                // distance reporting debug
//                Reveal.log( "firePendingCallbacks discarded location update\ncurrentLocation: " + this.currentLocation
//                        + "\nclastLocationReported: " + this.lastLocationReported
//                + "\ndistance: " + this.currentLocation.distanceTo( this.lastLocationReported ), "LOC" );
            }
        }

        if ( needUpdate ) {
            // commented out this code and returned it to reporting every google update (including duplicates)
//            Reveal.log("New Location reported: " + this.currentLocation + "\n" + this.currentAddress, "LOCATION");
//            // Track location updates when we get a new location
//            Reveal.RevealLocation loc = new Reveal.RevealLocation();
//
//            loc.setLocation(new Reveal.GlobalLocation(this.currentLocation));
//
//            Reveal.getInstance().recordEvent("Location Updated");
//            Reveal.getInstance().getDwellManager().addEvent(loc);
//
//            Reveal.log( "firePendingCallbacks added event: " + loc, "LOC");

            this.lastLocationReported = currentLocation;
            if (onValidLocationCallback != null) {
                onValidLocationCallback.onLocationFound();
            }
        }
    }

    // ============================================================================================
    //      Google Play Service callbacks
    // ============================================================================================

    @Override
    public void onConnected(Bundle connectionHint) {
        if ( applicationContext != null ) {
            if ( Reveal.selfPermissionGranted( applicationContext, Manifest.permission.ACCESS_FINE_LOCATION)
                    || Reveal.selfPermissionGranted(applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) ) {
                Reveal.log( "Connected to google location API, locations will now be provided", "STATE" );
                Reveal.getInstance().setLocationSharingEnabled( true );
                Reveal.getInstance().setStatus( Reveal.STATUS_LOCATION, Reveal.STATUS_SUCCEED, "Connected to google location API" );

                try {
                    Location location = LocationServices.FusedLocationApi.getLastLocation(
                            this.googleApiClient);


                    if (location != null) {
                        this.currentLocation = location;
                        this.onLocationChanged(location);
                    }

                    RevealLocationService.this.internalStartLocationMonitoring(applicationContext);

                } catch (SecurityException securityException) {
                    Reveal.log("Old android permissions scheme used to setup permissions and not handling shutoff of permissions properly in app.. stopping location monitoring",
                            "ERROR", "STATE");
                    Reveal.getInstance().setStatus( Reveal.STATUS_LOCATION, Reveal.STATUS_FAIL, "Security: " + securityException );
                    this.stopLocationMonitoring(applicationContext);
                }
            }
        }
    }

    @Override
    public void onLocationAvailability(LocationAvailability locationAvailability ) {
        super.onLocationAvailability( locationAvailability );
        Reveal.log( "onLocationAvailability( " + locationAvailability + " ) called", "DEBUG", "STATE" );

        this.locationDiscoveryActive =  locationAvailability.isLocationAvailable();

        if ( this.locationDiscoveryActive )
            Reveal.getInstance().setStatus( Reveal.STATUS_LOCATION, Reveal.STATUS_SUCCEED, "Location available" );
        else
            Reveal.getInstance().setStatus( Reveal.STATUS_LOCATION, Reveal.STATUS_FAIL, "No location available" );
    }

    @Override
    public void onLocationResult(LocationResult locationResult ) {
        super.onLocationResult( locationResult );

        Reveal.log( "onLocationResult( " + locationResult + " ) called", "DEBUG", "STATE" );

        // Retaining the old call so switching back is no issue
        // if we find any issues, we should refactor this later
        onLocationChanged( locationResult.getLastLocation() );
    }

    @Override
    public void onConnectionSuspended (int cause) {
        Reveal.getInstance().setLocationSharingEnabled( false );
        Reveal.log( "Google API suspended with cause: " + cause, "WARNING", "STATE" );
        Reveal.getInstance().setStatus( Reveal.STATUS_LOCATION, Reveal.STATUS_FAIL, "Google API suspended with cause: " + cause );
    }

    @Override
    public void onConnectionFailed (ConnectionResult result) {
        Reveal.log( "Google API failed with result: " + result.toString(), "ERROR", "STATE" );
        Reveal.getInstance().setStatus( Reveal.STATUS_LOCATION, Reveal.STATUS_FAIL, "Google API failed: " + result.toString() );
    }

    public void onLocationChanged(Location location) {
        if ( location != null ) {

            // We are filtering the location so we don't send it to reveal if it is
            double now = System.currentTimeMillis() / 1000.0;

            // save the location to be used for future requests
            this.currentLocation = location;

            // if we don't have one then set it to current
            if (this.lastLocationReported == null)
                this.lastLocationReported = location;

            // make sure they have moved far enough to count this one
            if ( (location.distanceTo(this.lastLocationReported) >= 100.0)
                    || (this.locationTime < 1.0) ) {  // send it the first time even if we haven't
                // moved since last start
                Reveal.log("Google gave us a new location:" + location, "STATE");

                // Track location updates when we get a new location
                Reveal.RevealLocation loc = new Reveal.RevealLocation();
                loc.setLocation(new Reveal.GlobalLocation(location));

                this.lastLocationReported = location;
                Reveal.getInstance().recordEvent("Location Updated");
                Reveal.getInstance().addEvent( Reveal.getInstance().getApplicationContext(), loc );
                Reveal.getInstance().setStatus(Reveal.STATUS_LOCATION, Reveal.STATUS_SUCCEED, "Got location: " + location);
            } else {
                Reveal.getInstance().setStatus(Reveal.STATUS_LOCATION, Reveal.STATUS_SUCCEED, "Got location: " + location + " (similar)");
            }

            this.locationTime = now;

            // Notify any callbacks waiting that we have a location even if not significant enough to
            // save it. So that any pending beacon requests get satisfied.
            firePendingCallbacks();
        }
    }


    private void timeOut(final double seconds, final Reveal.LocationService.OnValidLocationCallback callback ) {

        final Runnable timerRunnable = new Runnable() {
            @Override
            public void run() {
                boolean updated = false;

                // if there are no pending callbacks then there is no need to check - we succeeded
                synchronized ( this ) {
                    updated = true;
                }

                // if we have a current time and it has been updated recently then we got a location
                if ( RevealLocationService.this.currentLocation != null && !updated ) {
                    double now = System.currentTimeMillis();
                    long diff = (long) ( ( now - RevealLocationService.this.currentLocation.getTime() ) / 1000 );

                    if ( (double) diff <= seconds ) {
                        updated = true;
                    }
                    else
                        Reveal.getInstance().setStatus(Reveal.STATUS_LOCATION, Reveal.STATUS_IN_PROGRESS, "Waiting for valid location timed out, current age " + diff + " seconds");
                }

                if ( !updated ) {
                    Reveal.log("Waiting for valid location timed out", "OPT");
                }

                if ( callback != null )
                    callback.onLocationFound();
            }
        };

        // TODO: we should save the return value and stop/start it as needed - but the threading
        //       implications encourage us to caution
        this.noLocationTimerExecutionScheduler.schedule( timerRunnable, (long) seconds, TimeUnit.SECONDS);
    }

    public double getLocationValidTime() {
        return locationValidTime;
    }

    public void setLocationValidTime(double locationValidTime) {
        this.locationValidTime = locationValidTime;
    }

    public Reveal.LocationService.OnValidLocationCallback getLocationUpdated() {
        return onValidLocationCallback;
    }

    public void setOnValidLocationCallback(Reveal.LocationService.OnValidLocationCallback onValidLocationCallback) {
        this.onValidLocationCallback = onValidLocationCallback;
    }

    public boolean isLocationDiscoveryActive() {
        return locationDiscoveryActive;
    }

    public void setLocationDiscoveryActive(boolean locationDiscoveryActive) {
        this.locationDiscoveryActive = locationDiscoveryActive;
    }
}
