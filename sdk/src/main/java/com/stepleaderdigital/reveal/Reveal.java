package com.stepleaderdigital.reveal;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PowerManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static android.R.attr.targetSdkVersion;
import static com.stepleaderdigital.reveal.Reveal.RevealEvent.EventState.CLOSED;
import static com.stepleaderdigital.reveal.Reveal.RevealEvent.EventState.OPEN;
import static com.stepleaderdigital.reveal.Reveal.RevealEvent.EventType.BEACON;
import static com.stepleaderdigital.reveal.Reveal.RevealEvent.EventType.ENTER;

/**
 * Created by seandoherty on 1/13/15.
 */
public class Reveal  implements Application.ActivityLifecycleCallbacks {

    /**
     * Select which type of server you want to log into
     */
    public enum ServiceType {
        /**
         * SANDBOX - for testing only
         */
        SANDBOX,
        /**
         * PRODUCTION - for live operation
         */
        PRODUCTION
    }

    // tag for requesting bluetooth status
    public static final String STATUS_BLUETOOTH = "bluetooth";

    // tag for requesting location status
    public static final String STATUS_LOCATION = "location";

    // flag to retrieve network status
    public static final String STATUS_NETWORK = "network";

    // flag to retrieve memory status
    public static final String STATUS_MEMORY = "memory";

    // flag to retrieve memory status
    public static final String STATUS_SCAN = "scan";

    // The last attempt failed
    public static final int STATUS_FAIL = 0;

    // The last attempt was successful
    public static final int STATUS_SUCCEED = 1;

    // The last attemped is still in progress
    public static final int STATUS_IN_PROGRESS = 2;

    public interface OnBeaconFoundListener {
        /**
         * This method is called whenever a beacon is found and added to the cache
         *
         * @param beaconProperties the beacon
         */
        void onBeaconDiscovered(Map<String, Object> beaconProperties);

        /**
         * The specified beacon has been removedc from the cache - either due to time or no
         * longer being visible to the device
         *
         * @param beaconIdentifier the id of the beacon being removed
         */
        void onBeaconLeave(String beaconIdentifier);
    }

    public interface OnLocationFoundListener {
        /**
         * This method is called whenever a location is added to the queue
         *
         * @param location the location info
         */
        void onLocationDiscovered(GlobalLocation location);
    }

    public interface BeaconService {
        /**
         * start monitoring for beacons.
         *
         * @param properties A list of configuration settings for the Beacon Service
         */
        public void startBeaconScanning(Context context, BeaconScanningProperties properties);

        /**
         * stop monitoring for beacons
         */
        public void stopBeaconScanning(Context context);

        public void setBeaconFoundListener(OnBeaconFoundListener beaconFoundListener);

        // This is a temporary method to retrieve all of the current beacons
        // it will be replaced by a proper delegate notification mechanism
        public List<RevealBeacon> getBeacons();

        // this is called on every restart to allow the manager to setup it's state
        public void setupIfNeeded();
    }

    public interface JSONAble {
        /**
         * Serialize to JSON
         *
         * @return A JSONObject representing the object or a null if an error occurs
         */
        JSONObject getJSON();

        /**
         * Serialize to JSON
         *
         * @param includeExtras Include items not normally sent to the server
         * @return A JSONObject representing the object or a null if an error occurs
         */
        JSONObject getJSON(boolean includeExtras);

        /**
         * deserilizw an object from JSON.
         *
         * @param json The JSONObject representing the item
         * @return Returns true if successful and false if an error occurred (the exception should
         *         be logged in the console)
         */
        boolean setJSON(JSONObject json);
    }

    public interface LocationService {

        interface OnValidLocationCallback {
            void onLocationFound();
        }

        /**
         * Get the current location
         *
         * @return the location
         */
        Location getCurrentLocation(Context context);

        /**
         *  Start monitoring location services.
         */
        void startLocationMonitoring(Context context);

        /**
         *  Stop monitoring location changes
         */
        void stopLocationMonitoring(Context context);

        /**
         *  Allows functions that need a valid location to wait for a valid location to be available (address if possible)
         *  If there is already a valid location available, then the callback returns immediately, otherwise, the callback waits until
         *  there is a valid location or a timeout, in which case the best location we can find will be used
         *
         *  @param callback The method to call when a valid location is available
         */
        void waitForValidLocation(OnValidLocationCallback callback);

        double getLocationValidTime();
        void setLocationValidTime(double locationValidTime);

        void setOnValidLocationCallback(OnValidLocationCallback callback);

        boolean isLocationDiscoveryActive();
    }

    public interface UserLogListener {
        void logMessage(String message, String type, String group);
        void logDevice( BluetoothItem item, PDUiBeacon beacon );
    }

    /**
     * Beacon Service settings
     */
    public static class BeaconScanningProperties {
        /**
         * Interval for scanning in seconds, default to 3 minutes.
         */
        private Integer scanInterval = 180;
        /**
         * Duration of scanning in seconds, default to 30 seconds.
         */
        private Integer scanDuration = 30;
        /**
         * Additional manufacturer codes to scan for (should come from the server)
         */
        private List<Integer> additionalSecureCastManufacturerCodes = new ArrayList<Integer>();
        /**
         * Flag to determine if we are allowing scanning to happen while the app is in the background
         */
        private Boolean isBackgroundScanningEnabled = true;

        /**
         * how long to wait for a location
         */
        private double locationTimeOut = 30.0;

        public Integer getEddystoneTimeout() {
            return eddystoneTimeout;
        }

        public void setEddystoneTimeout(Integer eddystoneTimeout) {
            this.eddystoneTimeout = eddystoneTimeout;
        }

        /**
         * The time before sending an incomplete eddystone packet to the server.
         *
         * Note that if this time is less than the scan interval you will get false partials. That
         * is to say that you will get the beacon reported incomplete and then re-reported with
         * all the data.
         */
        private Integer eddystoneTimeout = 220;

        /**
         * List of iBeacon UUID's to limit results to
         */
        private List<String> debugUUIDs = new ArrayList<String>();

        private double cacheTTL = 60.0 * 60.0 * 1.0;

        public Integer getScanInterval() {
            return scanInterval;
        }

        public void setScanInterval(Integer scanInterval) {
            this.scanInterval = scanInterval;
        }

        public Integer getScanDuration() {
            return scanDuration;
        }

        public void setScanDuration(Integer scanDuration) {
            this.scanDuration = scanDuration;
        }

        public List<Integer> getAdditionalSecureCastManufacturerCodes() {
            return additionalSecureCastManufacturerCodes;
        }

        public void setAdditionalSecureCastManufacturerCodes(List<Integer> additionalSecureCastManufacturerCodes) {
            this.additionalSecureCastManufacturerCodes = additionalSecureCastManufacturerCodes;
        }

        public Boolean getBackgroundEnabled() {
            return isBackgroundScanningEnabled;
        }

        public void setBackgroundEnabled(Boolean backgroundEnabled) {
            isBackgroundScanningEnabled = backgroundEnabled;
        }

        public List<String> getDebugUUIDs() {
            return debugUUIDs;
        }

        public void setDebugUUIDs(List<String> debugUUIDs) {
            this.debugUUIDs = debugUUIDs;
        }

        public double getCacheTTL() {
            return cacheTTL;
        }

        public void setCacheTTL(double cacheTTL) {
            this.cacheTTL = cacheTTL;
        }

        public double getLocationTimeOut() {
            return locationTimeOut;
        }

        public void setLocationTimeOut(double locationTimeOut) {
            this.locationTimeOut = locationTimeOut;
        }


    }

    public static class AdUtils {

        public static final String PREFS_NAME = "adid";
        private static final String KEY_ID     = "key_adid";

        /**
         * Retrieves advertising id if not restricted.
         * @param context Context
         */
        public static void setupAdvertisingId(final Context context) {
            getAdvertisingId( context );
        }

        /**
         * Returns Advertising Id if not restricted.
         * @param context the active context
         * @return the ad id or an empty string if none available
         */
        public static String getAdvertisingId(final Context context) {
            if (context == null) {
                return "";
            }

            // refresh it for next use
            new Thread(new Runnable() {
                public void run() {
                    AdvertisingIdClient.Info adInfo = null;
                    try {
                        adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (adInfo != null) {
                        String adid = "";
                        if (!adInfo.isLimitAdTrackingEnabled()) {
                            adid = adInfo.getId();
                        }

                        // save it for future use
                        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(KEY_ID, adid);
                        editor.apply();
                    }
                }
            }).start();

            // return the answer we got last time
            SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE );

            return sharedPreferences.getString(KEY_ID, "Unknown");
        }

    }

    public static class RevealStatus {
        private int status = 0;
        private String name = "unknown";
        private String message = "";

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            String result = message;

            if ( result == null )
                result = "Status: " + status;

            return result;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class RevealLogger {
        private static final String LOG_TAG               = "Reveal";
        private static final int    STACK_TRACE_LEVELS_UP = 5;
        private static boolean verboseLoggingEnabled = false;


        public static void d(Object object) {
            if ( Reveal.getInstance().getIsDebug() ) {
                Log.d(LOG_TAG, getClassNameMethodNameAndLineNumber() + object);
            }
        }

        public static void v(Object object) {
            if ( Reveal.getInstance().getIsDebug()&&verboseLoggingEnabled) {
                Log.v(LOG_TAG, getClassNameMethodNameAndLineNumber() + object);
            }
        }

        public static void e(Object object) {
            if ( Reveal.getInstance().getIsDebug()) {
                Log.e(LOG_TAG, getClassNameMethodNameAndLineNumber() + object);
            }
        }


        public static void w(Object object) {
            if ( Reveal.getInstance().getIsDebug()) {
                Log.w(LOG_TAG, getClassNameMethodNameAndLineNumber() + object);
            }
        }

        public static void d() {
            if (Reveal.getInstance().getIsDebug()) {
                Log.d(LOG_TAG, getClassNameMethodNameAndLineNumber());
            }
        }

        public static void i(Object object) {
            if ( Reveal.getInstance().getIsDebug()) {
                Log.d(LOG_TAG, getClassNameMethodNameAndLineNumber() + object);
            }
        }

        public static void i() {
            if (Reveal.getInstance().getIsDebug()) {
                Log.i(LOG_TAG, getClassNameMethodNameAndLineNumber());
            }
        }

        public static void v() {
            if (Reveal.getInstance().getIsDebug()&&verboseLoggingEnabled) {
                Log.v(LOG_TAG, getClassNameMethodNameAndLineNumber());
            }
        }

        public static void e() {
            if (Reveal.getInstance().getIsDebug()) {
                Log.e(LOG_TAG, getClassNameMethodNameAndLineNumber());
            }
        }


        /**
         * Send a debug log message to the logger.
         *
         * @param message The message you would like logged. This message may contain string formatting
         *                which will be replaced with values from args.
         * @param args    Arguments for string formatting.
         */
        public static void d( String message, Object... args) {
            if ( Reveal.getInstance().getIsDebug())
                Log.d( LOG_TAG, formatString( message, args ) );
        }

        public static void e(String message, Object... args) {
            if ( Reveal.getInstance().getIsDebug())
                Log.e( LOG_TAG, formatString( message, args ) );
        }

        public static void w( String message, Object... args) {
            if ( Reveal.getInstance().getIsDebug())
                Log.w( LOG_TAG, formatString( message, args ) );
        }

        public static void i(String message, Object... args) {
            if ( Reveal.getInstance().getIsDebug()&&verboseLoggingEnabled)
                Log.i( LOG_TAG, formatString( message, args ) );
        }

        public static void w(Throwable t, String message, Object... args)
        {
            if ( Reveal.getInstance().getIsDebug())
                Log.w( LOG_TAG, formatString(message, args), t );
        }

        /**
         * Send a error log message.
         *
         * @param message The message you would like logged. This message may contain string formatting
         *                which will be replaced with values from args.
         * @param t       An exception to log.
         * @param args    Arguments for string formatting.
         * @see android.util.Log#e(String, String, Throwable)
         * @see java.util.Formatter
         * @see String#format(String, Object...)
         */
        public static void e(Throwable t, String message, Object... args)
        {
            if ( Reveal.getInstance().getIsDebug())
                Log.e( LOG_TAG, formatString(message, args), t );
        }

        public static String formatString(String message, Object... args) {
            // If no varargs are supplied, treat it as a request to log the string without formatting.
            return args.length == 0 ? message : String.format(message, args);
        }

        private static int getLineNumber() {
            return Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getLineNumber();
        }

        private static String getClassName() {
            String fileName = Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getFileName();
            return fileName.substring(0, fileName.length() - 5);
        }

        private static String getMethodName() {
            return Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getMethodName();
        }

        private static String getClassNameMethodNameAndLineNumber() {
            return "[" + getClassName() + "." + getMethodName() + "()-" + getLineNumber() + "]: ";
        }

        /**
         * Indicates whether verbose logging is enabled.   If not, expensive calculations to create
         * log strings should be avoided.
         * @return
         */
        public static boolean isVerboseLoggingEnabled() {
            return verboseLoggingEnabled;
        }

        /**
         * Sets whether verbose logging is enabled.  If not, expensive calculations to create
         * log strings should be avoided.
         *
         * @param enabled
         */
        public static void setVerboseLoggingEnabled(boolean enabled) {
            verboseLoggingEnabled = enabled;
        }
    }

    public static class Utils {

        @SuppressLint({ "InlinedApi", "NewApi" })
        public static boolean isBluetoothLeSupported(Context context) {
            boolean result = false;

            if (Build.VERSION.SDK_INT >= 18) {
                if ( context != null ) {
                    PackageManager packageManager = context.getPackageManager();

                    if ( packageManager != null )
                        result = packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
                }
            }
            return result;
        }

        @SuppressLint({ "InlinedApi", "NewApi" })
        public static BluetoothAdapter getBluetoothAdapter(Context context) {
            if (Build.VERSION.SDK_INT >= 18 && isBluetoothLeSupported(context)) {
                return BluetoothAdapter.getDefaultAdapter();
            }
            return null;
        }

        public static boolean isBluetoothEnabled(Context context) {
            boolean enabled = false;

            try {
                if (Reveal.selfPermissionGranted(context, Manifest.permission.BLUETOOTH)) {
                    BluetoothAdapter adapter = getBluetoothAdapter(context);
                    if (adapter != null) {
                        enabled = adapter.isEnabled();
                    }
                }
            }
            catch( SecurityException e ) {
                // http://stackoverflow.com/questions/33245977/java-lang-securityexception-bluetooth-permission-crash-samsung-devices-only
                Reveal.RevealLogger.d( "Bluetooth access denied (KNOX)");
            }

            return enabled;
        }

        public static int swap16( int source ) {
            return ((source & 0xff) << 8) | ((source & 0xff00) >> 8);
        }

        /**
         * Create initial json info required to obtain beacon setup.
         * @param context Context
         * @return json String
         */
        public static String getJsonInitInfo(Context context) {
            JSONObject json = getBaseJsonObject(context);
            try {
                json.put("locale", Locale.getDefault());

                Location location = Reveal.getInstance().locationService.getCurrentLocation( context );
                if (location != null) {
                    JSONObject loc = getLocationJson( context, null );
                    if ( loc != null )
                        json.put( "location", loc );
                }
                else {
                    Reveal.RevealLogger.w( "Sending/info  response to the server without a location");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            if ( context != null ) {

                if ( Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_WIFI_STATE) ) {
                    if (Utils.isWiFi(context)) {
                        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                        String ssid = wifiInfo.getSSID();

                        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
                            ssid = ssid.substring(1, ssid.length() - 1);
                        }

                        try {
                            if (wifiInfo.getSSID() != null)
                                json.put("currentSSID", ssid);

                            if (wifiInfo.getBSSID() != null)
                                json.put("currentBSSID", wifiInfo.getBSSID());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else {
                    try {
                        json.put("currentSSID", "*** NO ACCESS_WIFI_STATE PERMISSION ***");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            return json.toString();
        }

        /**
         * Create initial json info required to obtain beacon setup.
         * @param context Context
         * @return json String
         */
        public static String getJsonAppInfo(Context context, List<String> appNames) {
            JSONObject json = new JSONObject();
            try {
                json.put("device_id", getDeviceId(context));
                json.put("app_ids", new JSONArray(appNames));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return json.toString();
        }

        // return an entire batch eventCache JSON  object string
        public static String getJsonBatch(Context context, List<RevealEvent> events) {
            JSONObject json = getBaseJsonObject(context );

            JSONArray beacons = new JSONArray();
            JSONArray locations = new JSONArray();

            for( RevealEvent event : events ) {
                RevealEvent.EventType eventType = event.getEventType();

                switch ( eventType ) {
                    case BEACON:
                        RevealBeacon beacon = (RevealBeacon) event;
                        JSONObject jbeacon = beacon.getJSON();
                        Reveal.RevealLogger.d( "[NAME]='event' [TYPE]='JSON' [ID]='beacon'\n" + jbeacon );
                        beacons.put( jbeacon );
                        break;

                    case ENTER:
                        RevealLocation loc = (RevealLocation) event;
                        JSONObject jloc = loc.getJSON();
                        Reveal.RevealLogger.d( "[NAME]='event' [TYPE]='JSON' [ID]='location'\n" + jloc );
                        locations.put( jloc );
                        break;

                    case WIFI_ENTER:
//                        RevealWiFi wifi = (RevealWiFi) event;
//                        JSONObject jwifi = new JSONObject();
//                        addJsonWifi( context, jwifi, wifi );
//                        wifis.put( jwifi );
                        break;

                    default:
                        Reveal.RevealLogger.w("Unknown event type " + eventType + " encountered");
                        break;
                }
            }

            if ( beacons.length() > 0 ) {
                try {
                    json.put("beacons", beacons);
                    Reveal.log( "Adding " + beacons.length() + " beacons to batch", "STATE" );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if ( locations.length() > 0 ) {
                try {
                    json.put("locations", locations);
                    Reveal.log( "Adding " + locations.length() + " locations to batch", "STATE" );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

//            if ( wifis.length() > 0 ) {
//                try {
//                    json.put("networks", wifis);
//                    Reveal.log( "Adding " + wifis.length() + " wifis to batch", "STATE" );
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }

            if ( context != null ) {
                if ( Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_WIFI_STATE) ) {
                    if (Utils.isWiFi(context)) {
                        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

                        String ssid = wifiInfo.getSSID();

                        if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
                            ssid = ssid.substring(1, ssid.length() - 1);
                        }

//                        try {
//                            if (wifiInfo.getSSID() != null)
//                                json.put("currentSSID", ssid);
//
//                            if (wifiInfo.getBSSID() != null)
//                                json.put("currentBSSID", wifiInfo.getBSSID());
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }
                }
                else {
                    try {
                        json.put("currentSSID", "*** NO ACCESS_WIFI_STATE PERMISSION ***");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            return json.toString();
        }

        // when we support the next version we can use this code to get locale correcly and eliminate the deprication warnings maybe
        //@TargetApi(24)
//        public static  Locale getCurrentLocale(){
//            if (Build.VERSION.SDK_INT >= 24){
//                return Reveal.getInstance().applicationContext.getResources().getConfiguration().getLocales().get(0);
//            } else{
//                // noinspection deprecation
//                return Reveal.getInstance().applicationContext.getResources().getConfiguration().locale;
//            }
//        }


        /**
         * Creates base JSONObject with basic device info with beacon based location.
         * @param context Context
         * @return JSONObject
         */
        private static JSONObject getBaseJsonObject(Context context) {
            JSONObject json = new JSONObject();
            try {
                Calendar cal = Calendar.getInstance();
                TimeZone tz = cal.getTimeZone();
                String timeZone = tz.getID();

                json.put("device_id", getDeviceId(context));
                json.put("os", getOsName());
                json.put("bluetooth_enabled", isBluetoothEnabled(context));
                json.put("supports_ble", isBluetoothLeSupported(context));
                json.put("con_type", connectionType(context));
                json.put("sdk_version", Reveal.getVersion());
                json.put("version", Build.VERSION.RELEASE);
                json.put( "locale", context.getResources().getConfiguration().locale);
                json.put("device", Build.MANUFACTURER + " " + Build.MODEL);

                ApplicationInfo appInfo =  context.getApplicationInfo();
                String appId = appInfo.packageName;
                if ( appId != null )
                    json.put("app_id", appId );

                json.put("app_version", getAppVersion(context));
                json.put("time_zone", timeZone);

                String adId = Reveal.AdUtils.getAdvertisingId(context);
                if (!TextUtils.isEmpty(adId)) {
                    json.put("idfa", adId);
                }

                if ( Reveal.getInstance().getLocationSharingEnabled() )
                    json.put( "locationSharingEnabled", true );
                else
                    json.put( "locationSharingEnabled", false );
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Reveal.RevealLogger.d( "JSON:\n" + json );

            return json;
        }

        /**
         * Returns the network type being used.
         *
         *  Responses:
         *      null - no connectivity
         *      "wifi" - connected via a wifi network
         *      "mobile" - mobile (cell) communication network
         *      "wimax" - WiMax network
         *
         * @param context the Android context
         *
         * @return the networking type
         */
        public static String connectionType(Context context) {
            // NOTE: this belongs in network class but is placed here because
            // this is where the parameters are built
            String result = null;

            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = null;
            if (connectivityManager != null) {

                // android.permission.ACCESS_NETWORK_STATE
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE)== PackageManager.PERMISSION_GRANTED) {
                    networkInfo = connectivityManager.getActiveNetworkInfo();

                    if ( networkInfo != null ) {
                        if (networkInfo.isAvailable()) {
                            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI)
                                result = "wifi";
                            else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE)
                                result = "mobile";
                            else if (networkInfo.getType() == ConnectivityManager.TYPE_WIMAX)
                                result = "wimax";
                            else
                                result = "unknown";
                        }
                    }
                } else {
                    result = "unknown";
                }
            }

            if ( result != null ) {
                Reveal.RevealLogger.v( "NetworkType: " + result );
            }

            return result;
        }

        public static boolean magicCheck(Parcel in, int magic, int version, String name) {
            boolean result = false;
            int storedType = in.readInt();

            if ( storedType == magic ) {
                int storedVersion = in.readInt();

                if (storedVersion == version) {
                    result = true;
                }
                else
                    Reveal.RevealLogger.w("Encountered wrong version " + name +" expecting " + version + " not " + storedVersion );
            }
            else
                Reveal.RevealLogger.w("Parcel does not contain a valid " + name + " magic constant is " + storedType + " not " + magic );


            return result;
        }

        /**
         * Check to see if the current connection is a wifi connection
         *
         * @param context The application context
         * @return true if is wif, false otherwise
         */
        public static boolean isWiFi( Context context ) {
            boolean result = false;
            String type = connectionType( context );

            if ( type != null ) {
                if ( type.compareTo("wifi") == 0)
                    result = true;
            }

            return result;
        }

        private static JSONObject getLocationJson(Context context, RevealBeacon beacon) {
            return getLocationJson( context, beacon, false );
        }

        private static JSONObject getLocationJson(Context context, RevealBeacon beacon, boolean includeDiscovery ) {
            GlobalLocation location = null;
            if (beacon != null) {
                location = beacon.getLocation();
            }

            if (location == null) {
                location = new GlobalLocation(getLastKnownLocation(context));
            }

            return getLocationJson( context, location, includeDiscovery );
        }

        private static JSONObject getLocationJson(Context context, GlobalLocation location, boolean includeDiscovery ) {

            JSONObject jsonObject = new JSONObject();
            if (location != null) {
                try {
                    // Calculate the age from the current time and beacon time
                    Date date = new Date();
                    double now = date.getTime() / 1000.0;
                    double beaconTime = location.getTime() / 1000.0;
                    double age = ( beaconTime - now );

                    jsonObject.put("lat", location.getLatitude());
                    jsonObject.put("lon", location.getLongitude());
                    jsonObject.put("time", (System.currentTimeMillis() - location.getTime()));
                    jsonObject.put("altitude", location.getAltitude());
                    jsonObject.put("accuracy", location.getAccuracy());
                    jsonObject.put("provider", location.getProvider());
                    jsonObject.put("speed", location.getSpeed());
                    jsonObject.put("accuracy", location.getAccuracy());
                    jsonObject.put("age", age );
                    if ( includeDiscovery )
                        jsonObject.put("discoveryTime", location.getTime());

                    return jsonObject;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        private static String getOsName() {
            if (isKindle()) {
                return "kindle";
            }
            return "android";
        }

        private static boolean isKindle() {
            return Build.MANUFACTURER.toUpperCase(Locale.US).equals("AMAZON") && (Build.MODEL.toUpperCase(Locale.US).startsWith("KF") || Build.MODEL.toUpperCase(Locale.US).contains("KINDLE"));
        }

        private static String getAppVersion(Context context) {
            try {
                return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            return "unknown";
        }

        public static String getAdvertisingId(final Context context)
        {
            return Reveal.AdUtils.getAdvertisingId( context );
        }

        public static String getDeviceId(Context context) {
            String deviceId = getAdvertisingId( context );

            if ( deviceId == null) {
                deviceId = createDeviceIdMD5Hash(deviceId);
            }

            return deviceId;
        }

        public static String createDeviceIdMD5Hash(final String inputString) {
            String deviceId = inputString;
            try {
                MessageDigest digest = MessageDigest.getInstance("MD5");
                digest.update(("android" + deviceId).getBytes());
                byte[] messageDigest = digest.digest();
                StringBuilder hexString = new StringBuilder();
                for (int i = 0; i < messageDigest.length; i++) {
                    hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
                }
                deviceId = hexString.toString();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return deviceId;
        }

        public static Location getLastKnownLocation(Context context) {
            if (context != null && ( Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_FINE_LOCATION) ) ) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                if (locationManager != null) {
                    //noinspection AndroidLintMissingPermission
                    Location lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (lastLocation == null) {
                        //noinspection AndroidLintMissingPermission
                        lastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                    return lastLocation;
                }
            }
            return null;
        }
    }

    public static class RevealNetworkClient {

        public static final String   SEGMENT_INFO   = "/api/v3/info";
        public static final String   SEGMENT_BEACON = "/api/v3/event/rawbeacon";
        public static final String   SEGMENT_BATCH = "/api/v3/event/batch";

        private static final ExecutorService executor       = Executors.newFixedThreadPool(3);

        public static void registerDevice(final Context context, final RevealNetworkClientCallback callback) {
            String json = Reveal.Utils.getJsonInitInfo(context);

            execute(RevealNetworkClient.SEGMENT_INFO, json, callback);
        }

        public static void sendBatch(final Context context, List<RevealEvent> eventList, final RevealNetworkClientCallback callback) {
            String json = Reveal.Utils.getJsonBatch(context, eventList);
            execute(RevealNetworkClient.SEGMENT_BATCH, json, callback);
        }

        private static void execute(final String segment, final String json, final RevealNetworkClientCallback callback) {
            Reveal.log("Service call made to segment " + segment + ", callback: " + callback + ", json: " + json, "COMM");
            final Handler handler = new Handler(Looper.getMainLooper());
            try {
                executor.execute(new RevealNetworkClient.Task(handler, segment, json, callback));
            }
            catch ( Exception ex ) {
                Reveal.log( "Network api exception: " + ex.toString(), "ERROR", "COMM" );

                Reveal.getInstance().setStatus( STATUS_NETWORK, 0, ex.toString() );
            }
        }

        private static class Task implements Runnable {

            private Handler               handler  = null;
            private String                segment  = null;
            private String                json     = null;
            private RevealNetworkClientCallback callback = null;

            public Task(Handler handler, String segment, String json, RevealNetworkClientCallback callback) {
                this.handler = handler;
                this.segment = segment;
                this.json = json;
                this.callback = callback;
            }

            @Override
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
                HttpURLConnection urlConnection = null;
                int responseCode = 0;
                InputStream in = null;
                try {
                    Reveal.getInstance().setStatus( STATUS_NETWORK, STATUS_IN_PROGRESS, "Sending: " + segment);
                    URL link = new URL(Reveal.getInstance().getAPIBaseURL() + segment);
                    urlConnection = (HttpURLConnection) link.openConnection();
                    urlConnection.setReadTimeout(3000);
                    urlConnection.setConnectTimeout(3000);
                    // This will use the default chunking size
                    urlConnection.setChunkedStreamingMode(0);
                    urlConnection.addRequestProperty("X-API-KEY", Reveal.getInstance().getAPIKey());
                    urlConnection.addRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Connection", "keep-alive");
                    urlConnection.setRequestProperty("Connection", "close");
                    urlConnection.setRequestMethod("POST");

                    urlConnection.setDoInput(true);
                    urlConnection.setDoOutput(true);

                    OutputStream os = urlConnection.getOutputStream();
                    os.write( json.getBytes(Charset.forName("UTF-8")) );
                    os.close();
                    Reveal.log( "Sent to server endpoint: " + segment + "\n" + json, "COMM" );

                    urlConnection.connect();
                    responseCode = urlConnection.getResponseCode();
                    Reveal.log("Connection to server successful with response code "+responseCode);

                    if ( responseCode < 300 )
                        in = new BufferedInputStream(urlConnection.getInputStream());
                    else
                        in = new BufferedInputStream(urlConnection.getErrorStream());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        total.append(line);
                    }
                    String content = total.toString();
                    in.close();

                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        int length = content.length();

                        Reveal.getInstance().setStatus( STATUS_NETWORK, STATUS_SUCCEED, "SUCCESS: " + segment + " Returned " + length + " bytes");

                        if ( length > 0 ) {
                            Reveal.log( "HTTP response content: " + content, "COMM" );
                            JSONObject jsonObject = new JSONObject(content);
                            Reveal.getInstance().recordEvent(  segment );
                            success(callback, jsonObject);
                        }
                        else {
                            Reveal.log( "HTTP response 200 OK with no content", "COMM" );
                            Reveal.getInstance().recordEvent( segment );

                            // we didn't get any data so we'll call success with a null to indicate
                            // success but no JSON
                            success( callback, null );
                        }
                    }
                    else {
                        Reveal.log( "HTTP Response " + responseCode + ": " + content, "ERROR", "COMM");
                        failed(callback, content);

                        Reveal.getInstance().setStatus( STATUS_NETWORK, STATUS_FAIL, "HTTP Response " + responseCode + ": " + content );
                    }

                } catch (final Exception ex) {
                    Reveal.log( "HTTP Error " + responseCode + ": " + ex.toString(), "error", "COMM" );
                    Reveal.getInstance().recordEvent( segment, false );

                    Reveal.getInstance().setStatus( STATUS_NETWORK, STATUS_FAIL, "HTTP Error " + responseCode + ": " + ex.toString() );

                    failed(callback, ex.toString());
                } finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (urlConnection != null)
                        urlConnection.disconnect();

                    Reveal.logMemoryUsage( "Web Service callback" );
                }

            }

            private void success(final Reveal.RevealNetworkClientCallback callback, final JSONObject response) {
                if (callback != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onSuccess(response);
                        }
                    });
                }
            }

            private void failed(final RevealNetworkClientCallback callback, final String response) {
                if (callback != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onFailure(response);
                        }
                    });
                }
            }
        }
    }

    public static class RevealWiFi implements Parcelable, RevealEvent {

        protected GlobalLocation     location;
        protected long               discoveryTime;
        private long                 lastSeen;
        private String               currentSSID;
        private String               currentBSSID;
        private String               notes;
        private long                 lastNotificationTime;
        private EventState           eventState = OPEN;

        protected final int          magic = 9002;
        protected final int          beaconSchemaVersion = 1;

        /**
         * The network name.
         */
        protected String SSID;

        /**
         * The address of the access point.
         */
        protected String BSSID;

//        /**
//         * Describes the authentication, key management, and encryption schemes
//         * supported by the access point.
//         */
//        protected String capabilities;
//        /**
//         * The detected signal level in dBm, also known as the RSSI.
//         *
//         * <p>Use {@link android.net.wifi.WifiManager#calculateSignalLevel} to convert this number into
//         * an absolute signal level which can be displayed to a user.
//         */
//        protected int level;
//        /**
//         * The primary 20 MHz frequency (in MHz) of the channel over which the client is communicating
//         * with the access point.
//         */
//        protected int frequency;
//
//        /**
//         * Indicates venue name (such as 'San Francisco Airport') published by access point; only
//         * available on passpoint network and if published by access point.
//         */
//        protected String venueName;
//
//        /**
//         * Indicates passpoint operator name published by access point.
//         */
//        protected String operatorFriendlyName;

        public RevealWiFi() {
            super();

            Date date = new Date();
            discoveryTime = date.getTime() / 1000L;
        }

        public double getDwellTime() {
            return Math.abs( lastSeen - discoveryTime );
        }

        @Override
        public long getLastSeen() {
            return lastSeen;
        }

        @Override
        public void setLastSeen(long lastSeen) {
            this.lastSeen = lastSeen;
        }

        public String getIdentifier() {
            return BSSID;
        }

        public EventType getEventType()
        {
            return EventType.WIFI_ENTER;
        }

        @Override
        public long getLastNotificationTime() {
            return lastNotificationTime;
        }

        @Override
        public void setLastNotificationTime(long lastNotificationTime) {
            this.lastNotificationTime = lastNotificationTime;
        }

        @Override
        public EventState getEventState() {
            return eventState;
        }

        @Override
        public void setEventState(EventState eventState) {
            this.eventState = eventState;
        }

        private RevealWiFi(Parcel in) {
            if ( Utils.magicCheck( in, this.magic, this.beaconSchemaVersion, "RevealWiFi" )) {
                if ( in.readInt() == 1 )
                    location = in.readParcelable(getClass().getClassLoader());
                discoveryTime = in.readLong();
                SSID = in.readString();
                BSSID = in.readString();
            }
        }

        public void writeToParcel(Parcel out, int flags) {

            out.writeInt( magic );
            out.writeInt( beaconSchemaVersion );
            if ( location != null ) {
                out.writeInt(1);
                out.writeParcelable(location, 0);
            }
            else
                out.writeInt( 0 );
            out.writeLong(discoveryTime);
            out.writeString(SSID);
            out.writeString(BSSID);
        }

        public static final Creator<RevealWiFi> CREATOR = new Creator<RevealWiFi>() {
            public RevealWiFi createFromParcel(Parcel in) {
                return new RevealWiFi(in);
            }
            public RevealWiFi[] newArray(int size) {
                return new RevealWiFi[size];
            }
        };

        @Override
        public int describeContents() {
            return hashCode();
        }


        @Override
        public long getDiscoveryTime() {
            return discoveryTime;
        }

        public void setDiscoveryTime(long discoveryTime) {
            this.discoveryTime = discoveryTime;
        }

        public GlobalLocation getLocation() {
            return location;
        }

        public void setLocation(GlobalLocation location) {
            this.location = location;
        }

        public String getCurrentSSID() {
            return currentSSID;
        }

        public void setCurrentSSID(String currentSSID) {
            this.currentSSID = currentSSID;
        }

        public String getCurrentBSSID() {
            return currentBSSID;
        }

        public void setCurrentBSSID(String currentBSSID) {
            this.currentBSSID = currentBSSID;
        }

        @Override
        public String getNotes() {
            return notes;
        }

        @Override
        public void setNotes(String notes) {
            this.notes = notes;
        }
    }

    public static class RevealLocation extends RevealEventBase implements Parcelable, JSONAble, RevealEvent {
        private GlobalLocation       location;
        private long                 discoveryTime;
        private long                 lastSeen;
        private String               currentSSID;
        private String               currentBSSID;
        private String               notes;
        private long                 lastNotificationTime;
        private EventState           eventState = CLOSED;

        protected final int          magic = 9001;
        protected final int          beaconSchemaVersion = 1;

        public RevealLocation() {
            super();

            Date date = new Date();
            discoveryTime = date.getTime() / 1000L;

        }

        /**
         * Serialize to JSON
         *
         * @return A JSONObject representing the object or a null if an error occurs
         */
        @Override
        public JSONObject getJSON() {
            return getJSON( false );
        }

        /**
         * Serialize to JSON
         *
         * @param includeExtras Include items not normally sent to the server
         * @return A JSONObject representing the object or a null if an error occurs
         */
        public JSONObject getJSON( boolean includeExtras ) {
            JSONObject result = getBaseJSON( this, includeExtras );

            try {
                result.put("location", location.getJSON( includeExtras ));

            }
            catch ( JSONException e ) {
                e.printStackTrace();
            }

            return result;
        }

        /**
         * deserialize an object from JSON.
         *
         * @param json The JSONObject representing the item
         * @return Returns true if successful and false if an error occurred (the exception should
         *         be logged in the console)
         */
        @Override
        public boolean setJSON( JSONObject json ) {
            boolean result = false;

            setBaseJSON( this, json );

            try {
                if (json.has("location")) {
                    JSONObject locJSON = json.getJSONObject( "location" );
                    GlobalLocation loc = new GlobalLocation();
                    loc.setJSON( locJSON );

                    setLocation( loc );
                }

                result = true;
            }
            catch( JSONException e ) {
                e.printStackTrace();
            }

            return result;
        }


        public EventType getEventType()
        {
            return EventType.ENTER;
        }

        public GlobalLocation getLocation() {
            return location;
        }

        public String getCurrentSSID() {
            return currentSSID;
        }

        public void setCurrentSSID(String currentSSID) {
            this.currentSSID = currentSSID;
        }

        public String getCurrentBSSID() {
            return currentBSSID;
        }

        public void setCurrentBSSID(String currentBSSID) {
            this.currentBSSID = currentBSSID;
        }


        public double getDwellTime() {
            return Math.abs( lastSeen - discoveryTime );
        }

        @Override
        public long getLastNotificationTime() {
            return lastNotificationTime;
        }

        @Override
        public void setLastNotificationTime(long lastNotificationTime) {
            this.lastNotificationTime = lastNotificationTime;
        }

        @Override
        public EventState getEventState() {
            return eventState;
        }

        @Override
        public void setEventState(EventState eventState) {
            this.eventState = eventState;
        }

        @Override
        public long getLastSeen() {
            return lastSeen;
        }

        @Override
        public void setLastSeen(long lastSeen) {
            this.lastSeen = lastSeen;
        }

        public void setDiscoveryTime(long discoveryTime) {
            this.discoveryTime = discoveryTime;
        }

        public String getIdentifier() {
            return "location";
        }

        public void setLocation(GlobalLocation location) {
            this.location = location;
        }
        private RevealLocation(Parcel in) {
            if ( Utils.magicCheck( in, this.magic, this.beaconSchemaVersion, "RevealLocation" )) {
                if (in.readInt() == 1)
                        location = in.readParcelable(getClass().getClassLoader());
                    discoveryTime = in.readLong();
                }
        }

        @Override
        public String getNotes() {
            return notes;
        }

        @Override
        public void setNotes(String notes) {
            this.notes = notes;
        }

        public void writeToParcel(Parcel out, int flags) {
            out.writeInt( magic );
            out.writeInt( beaconSchemaVersion );
            if ( location != null ) {
                out.writeInt( 1 );
                out.writeParcelable(location, 0);
            }
            else
                out.writeInt( 0 );

            out.writeLong( discoveryTime );
        }

        public static final Creator<RevealLocation> CREATOR = new Creator<RevealLocation>() {
            public RevealLocation createFromParcel(Parcel in) {
                return new RevealLocation(in);
            }
            public RevealLocation[] newArray(int size) {
                return new RevealLocation[size];
            }
        };

        @Override
        public int describeContents() {
            return hashCode();
        }


        @Override
        public long getDiscoveryTime() {
            return discoveryTime;
        }

    }

    public static class GlobalLocation implements JSONAble, Parcelable {

        private double latitude;
        private double longitude;
        private double altitude;
        private long time;
        private double speed;
        private double bearing;
        private float accuracy;
        private String provider;
        private long lastNotificationTime;

        public GlobalLocation() {

        }

        public GlobalLocation( Location gps ) {
            if ( gps != null ) {
                this.latitude = gps.getLatitude();
                this.longitude = gps.getLongitude();
                this.altitude = gps.getAltitude();
                this.speed = gps.getSpeed();
                this.accuracy = gps.getAccuracy();
                this.provider = gps.getProvider();
                this.bearing = gps.getBearing();
                this.time = gps.getTime();
            }
        }

        private GlobalLocation(Parcel in) {
            this.latitude = in.readDouble();
            this.longitude = in.readDouble();
            this.altitude = in.readDouble();
            this.speed = in.readDouble();
            this.accuracy = in.readFloat();
            this.provider = in.readString();
            this.bearing = in.readDouble();
            this.time = in.readLong();
        }

        public void writeToParcel(Parcel out, int flags) {
            out.writeDouble(this.latitude);
            out.writeDouble(this.longitude);
            out.writeDouble(this.altitude);
            out.writeDouble(this.speed);
            out.writeFloat(this.accuracy);
            out.writeString(this.provider);
            out.writeDouble(this.bearing);
            out.writeLong(this.time);
        }

        public static final Creator<GlobalLocation> CREATOR = new Creator<GlobalLocation>() {
            public GlobalLocation createFromParcel(Parcel in) {
                return new GlobalLocation(in);
            }
            public GlobalLocation[] newArray(int size) {
                return new GlobalLocation[size];
            }
        };



        /**
         * Serialize to JSON
         *
         * @return A JSONObject representing the object or a null if an error occurs
         */
        @Override
        public JSONObject getJSON() {
            return getJSON( false );
        }

        /**
         * Serialize to JSON
         *
         * @param includeExtras Include items not normally sent to the server
         * @return A JSONObject representing the object or a null if an error occurs
         */
        public JSONObject getJSON( boolean includeExtras ) {
            JSONObject result = new JSONObject();

            try {
                // Calculate the age from the current time and beacon time
                Date date = new Date();
                double now = date.getTime() / 1000.0;
                double beaconTime = getTime() / 1000.0;
                double age = ( beaconTime - now );

                result.put("lat", getLatitude());
                result.put("lon", getLongitude());
                result.put("time", (System.currentTimeMillis() - getTime()));
                result.put("altitude", getAltitude());
                result.put("accuracy", getAccuracy());
                result.put("provider", getProvider());
                result.put("speed", getSpeed());
                result.put("age", age );
                if ( includeExtras )
                    result.put("discoveryTime", getTime());


            }
            catch ( JSONException e ) {
                e.printStackTrace();
            }

            return result;
        }

        /**
         * deserialize an object from JSON.
         *
         * @param json The JSONObject representing the item
         * @return Returns true if successful and false if an error occurred (the exception should
         *         be logged in the console)
         */
        @Override
        public boolean setJSON( JSONObject json ) {
            boolean result = false;

            try {
                if (json.has("lat"))
                    setLatitude(json.getDouble("lat"));

                if (json.has("lon"))
                    setLongitude( json.getDouble("lat"));

                if (json.has("time"))
                    setTime( json.getLong("time"));

                if (json.has("altitude"))
                    setAltitude( json.getDouble("time"));

                if (json.has("accuracy"))
                    setAccuracy( (float) json.getDouble( "accuracy"));

                if (json.has("provider"))
                    setProvider( json.getString("provider"));

                if (json.has("speed"))
                    setSpeed( json.getDouble("speed"));

                if (json.has("discoveryTime"))
                    setTime( json.getLong("discoveryTime"));

                result = true;
            }
            catch( JSONException e ) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        public int describeContents() {
            return hashCode();
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getAltitude() {
            return altitude;
        }

        public void setAltitude(double altitude) {
            this.altitude = altitude;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public double getSpeed() {
            return speed;
        }

        public void setSpeed(double speed) {
            this.speed = speed;
        }

        public double getBearing() {
            return bearing;
        }

        public void setBearing(double bearing) {
            this.bearing = bearing;
        }

        public float getAccuracy() {
            return accuracy;
        }

        public void setAccuracy(float accuracy) {
            this.accuracy = accuracy;
        }

        public String getProvider() {
            return provider;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }

        public long getLastNotificationTime() {
            return lastNotificationTime;
        }

        public void setLastNotificationTime(long lastNotificationTime) {
            this.lastNotificationTime = lastNotificationTime;
        }
    }

    public static class RevealAddress implements Parcelable {
        private String street;
        private String city;
        private String state;
        private String zip;
        private String country;

        public HashMap<String, Object> getProperties() {
            HashMap<String, Object> result = new HashMap<String, Object>();

            result.put( "street", street );
            result.put( "city", city );
            result.put( "state", state );
            result.put( "zip", zip );
            result.put( "country", country );

            return result;
        }

        public RevealAddress() {

        }

        private RevealAddress(Parcel in) {
            street = in.readString();
            city = in.readString();
            state = in.readString();
            zip = in.readString();
            country = in.readString();
        }

        public void writeToParcel(Parcel out, int flags) {
            out.writeString(street);
            out.writeString(city);
            out.writeString(state);
            out.writeString(zip);
            out.writeString(country);
        }

        public static final Creator<RevealAddress> CREATOR = new Creator<RevealAddress>() {
            public RevealAddress createFromParcel(Parcel in) {
                return new RevealAddress(in);
            }
            public RevealAddress[] newArray(int size) {
                return new RevealAddress[size];
            }
        };

        @Override
        public int describeContents() {
            return hashCode();
        }


        @Override
        public String toString() {
            String result = "";

            if ( street != null )
                result = result + street;

            if ( city != null ) {
                if ( result.length() > 0 )
                    result = result + ", ";
                result = result + city;
            }

            if ( state != null ) {
                if ( result.length() > 0 )
                    result = result + ", ";
                result = result + state;
            }

            if ( zip != null ) {
                if ( result.length() > 0 )
                    result = result + " ";
                result = result + zip;
            }

            if ( country != null ) {
                if ( result.length() > 0 )
                    result = result + ", ";
                result = result + country;
            }

            return result;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }
    }

    // ******************************************************************************************
    // *                            Beacons                                                     *
    // ******************************************************************************************

    public interface RevealNetworkClientCallback {
        void onSuccess(JSONObject response);
        void onFailure(String response);
    }


    // The PDU is the building block of advertising packets.
    // Each PDU consist of a length followed by a type and a number of bytes equal to the
    // length - 1
    //
    // +--------+--------+--------+-------+-------+-------+-------+--   --*------+-------+
    // | length |  type  | data                                      ...                 |
    // +--------+--------+--------+-------+-------+-------+-------+--   --*------+-------+
    //
    // Any number of the types can be appended until you reach the limit of the advertising
    // packet
    //
    // @see: http://www.argenox.com/a-ble-advertising-primer/
    //       Also: http://j2abro.blogspot.com/2014/06/understanding-bluetooth-advertising.html
    public static class PDU {
        // PDU Ad types from https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile
        // Note: This could be an enum but then you wouldn't be able to reference unknown types,
        //       Once we know the list is exhaustive then we should change it.
        static final int FLAGS = 0x01;
        static final int UUID16_INCOMPLETE = 0x02;
        static final int UUID16 = 0x03;
        static final int UUID32_INCOMPLETE = 0x06;
        static final int UUID128 = 0x07;
        static final int SHORT_NAME = 0x08;
        static final int COMPLETE_NAME = 0x09;
        static final int TX_POWER = 0x0a;
        static final int SERVICE_DATA = 0x16;
        static final int APPEARANCE = 0x19;
        static final int MANUFACTURER_SPECIFIC_DATA = 0xff;

        // PDU flags bits
        static final int FLAGS_LE_LIMITED_DISCOVERABLE = 0x01;
        static final int FLAGS_LE_GENERAL_DISCOVERABLE = 0x02;
        static final int FLAGS_BE_EDR_SUPPORTED = 0x04;
        static final int FLAGS_CONTROLLER_DUAL = 0x08;
        static final int FLAGS_HOST_DUAL = 0x10;

        private byte length = 0;
        private int type = 0;
        private ArrayList<Byte> payload = new ArrayList<Byte>();

        @Override
        public String toString() {
            String result = "";

            switch( type ) {
                case FLAGS:
                    result = result + "FLAGS";
                    break;

                case UUID16_INCOMPLETE:
                    result = result + "UUID16-I";
                    break;

                case UUID16:
                    result = result + "UUID16";
                    break;

                case UUID32_INCOMPLETE:
                    result = result + "UUID32-I";
                    break;

                case UUID128:
                    result = result + "UUID128";
                    break;

                case SHORT_NAME:
                    result = result + "NAME-S";
                    break;

                case COMPLETE_NAME:
                    result = result + "NAME";
                    break;

                case TX_POWER:
                    result = result + "TX";
                    break;

                case SERVICE_DATA:
                    result = result + "SERVICE-DATA";
                    break;

                case APPEARANCE:
                    result = result + "APPEARANCE";
                    break;

                case MANUFACTURER_SPECIFIC_DATA:
                    String mfgName = manufacturerName( int16at(0));
                    result = result + "DATA-" + mfgName;
                    break;

                default:
                    result = result + "PDU-(" + type + ")";
                    break;
            }

            switch( type ) {
                case SHORT_NAME:
                case COMPLETE_NAME:
                    result = result + ": " + getString();
                    break;

                default:
                    result = result + ": " + getHex();
                    break;
            }

            return result;
        }

        /**
         * Create a list of PDU blocks from an array
         * @param data the raw bytes recieved
         * @return A list og PDU's that are present
         */
        public static ArrayList<PDU> PDUList( byte[] data ) {
            int current = 0;
            boolean done = false;
            ArrayList<PDU> result = new ArrayList<PDU>();

            while (!done) {
                if ( current >= data.length )
                    done = true;
                else {
                    byte length = data[current];

                    if (length < 1)
                        done = true;
                    else if ((length + current) >= data.length)
                        done = true; // should we throw an error?
                    else {
                        PDU pdu = new PDU(data, current);
                        result.add(pdu);
                        current = current + pdu.getLength() + 1;
                    }
                }
            }

            return result;
        }

        /**
         * Create a PDU object from a byte array representation of the advertising data
         *
         * @param data the data
         * @param start the offset in the data to start from
         */
        PDU( byte[] data, int start ) {
            length = data[start];
            type = data[start + 1] & 0xff;

            for( int index = 0 ; index < length - 1 ; index ++ ) {
                payload.add( data[start+2+index] );
            }
        }

        public String getHex() {
                String result = "";

                for ( byte charecter : payload ) {
                    String byteHex = Integer.toHexString(((int) charecter) & 0xff);

                    if (byteHex.length() == 1 )
                        byteHex = "0" + byteHex;

                    result = result + byteHex;
                }

            return result;
        }

        public String getString() {
            String result = "";

            for ( byte charecter : payload ) {
                result = result + (char) charecter;
            }

            return result;
        }

        public String getUUID16At( int index ) {
            return getHexAt( index, 4 ) + "-" +
                    getHexAt( index + 4, 2 ) + "-" +
                    getHexAt( index + 6, 2 ) + "-" +
                    getHexAt( index + 8, 2 ) + "-" +
                    getHexAt( index + 10, 6 ) ;
        }

        public String getHexAt( int index, int length ) {
            String result = "";

            for( int i=0 ; i< length ; i++ ) {
                int charecter = int8at( index+i );
                String byteHex = Integer.toHexString( charecter );

                if (byteHex.length() == 1 )
                    byteHex = "0" + byteHex;

                result = result + byteHex;
            }

            return result;
        }

        public int int8at( int index ) {
            int result = 0;

            if ( index < payload.size() )
                result = payload.get( index ) & 0xff;

            return result;
        }

        public int int16at( int index ) {
            int low = int8at( index );
            int high = int8at( index + 1 ) << 8;

            return high + low;
        }

        public int int16FlippedAt( int index ) {
            int low = int8at( index + 1 );
            int high = int8at( index ) << 8;

            return high + low;
        }

        public byte[] bytesAt( int index ) {
            return bytesAt(index, payload.size() - index );
        }

        public byte[] bytesAt( int index, int size ) {
            byte[] byteArray = new byte[payload.size()];

            for( int i=0 ; i<size ; i++ )
                byteArray[i] = payload.get(i+index);

            return byteArray;
        }

        /**
         * Get manufacturer name from number using data obtained from: https://www.bluetooth.com/specifications/assigned-numbers/company-identifiers
         *
         * @param code the code you are looking
         * @return the name that matches the code
         */
        public static String manufacturerName( int code ) {
            // TODO - long term this should be a list pushed down from reveal server
            String result = null;

            switch( code ) {
                case PDUiBeacon.Apple:
                    result = "iBeacon";
                    break;

                case 117:
                    result = "Samsung Electronics Co. Ltd.";
                    break;

                case 138:
                    result = "Jawbone";
                    break;

                case 249:
                    result = "StickNFind";
                    break;

                case PDUiBeacon.Gimbal:
                    result = "Gimbal";
                    break;

                case 197:
                    result = "Onset Computer Corporation";
                    break;

                case PDUiBeacon.Swirl:
                    result = "Swirl";
                    break;

                case 272:
                    result = "Nippon Seiki Co.";
                    break;

                case PDUiBeacon.Radius:
                    result = "Radius";
                    break;

                case PDUiBeacon.Estimote:
                    result = "Estimote";
                    break;

                default:
                    result = "Unknown" + code;
                    break;
            }

            return result;
        }

        public byte getLength() {
            return length;
        }

        public void setLength(byte length) {
            this.length = length;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public ArrayList<Byte> getPayload() {
            return payload;
        }

        public void setPayload(ArrayList<Byte> payload) {
            this.payload = payload;
        }
    }

    public static class BluetoothItem {
        private String address;
        private String name;
        private String hex;
        private String beaconType;
        private int rssi;
        private Map<String, Object> beacon;
        private ArrayList<String> uuids;
        private ArrayList<Reveal.PDU> pdus;
        private BluetoothDevice device;
        private HashMap<String,Object> userData = new HashMap<String, Object>();

        public boolean isBeacon() {
            boolean result = false;

            if ( beacon != null )
                if ( beacon.size() > 0 )
                    result = true;

            return result;
        }

        public ArrayList<PDU> getPDUTypes( int[] types ) {
            ArrayList<PDU> result = new ArrayList<PDU>();

            for( PDU pdu: pdus ) {
                for( int type: types )
                    if ( type == pdu.type )
                        result.add( pdu );
            }

            return result;
        }

        public PDU getPDUType( int type ) {
            PDU result = null;
            int[] typeArray = { type };
            ArrayList<PDU> matches = getPDUTypes( typeArray );

            if ( matches.size() > 0 )
                result = matches.get( 0 );

            return result;
        }

        @Override
        public String toString() {
            String result = this.address;

            if ( beaconType != null )
                result = result + beaconType;

            if ( pdus != null ) {
                for( PDU pdu : pdus ) {
                    result = result + "\n   " + pdu;
                }
            }

            return result;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getHex() {
            return hex;
        }

        public void setHex(String hex) {
            this.hex = hex;
        }

        public int getRssi() {
            return rssi;
        }

        public void setRssi(int rssi) {
            this.rssi = rssi;
        }

        public Map<String, Object> getBeacon() {
            return beacon;
        }

        public void setBeacon(Map<String, Object> beacon) {
            this.beacon = beacon;
        }

        public String getBeaconType() {
            return beaconType;
        }

        public void setBeaconType(String beaconType) {
            this.beaconType = beaconType;
        }

        public ArrayList<String> getUuids() {
            return uuids;
        }

        public void setUuids(ArrayList<String> uuids) {
            this.uuids = uuids;
        }

        public ArrayList<Reveal.PDU> getPdus() {
            return pdus;
        }

        public void setPdus(ArrayList<Reveal.PDU> pdus) {
            this.pdus = pdus;
        }

        public BluetoothDevice getDevice() {
            return device;
        }

        public void setDevice(BluetoothDevice device) {
            this.device = device;
        }

        public HashMap<String, Object> getUserData() {
            return userData;
        }

        public void setUserData(HashMap<String, Object> userData) {
            this.userData = userData;
        }
    }

    public static class PDUiBeacon {
        public final int LMP = 0xffff;

        public static final int Apple = 76;
        public static final int Estimote = 349;
        public static final int Swirl = 181;
        public static final int Gimbal = 140;
        public static final int Radius = 280;

        private int manufacturerCode = LMP;
        private String manufacturer;
        private String uuid;
        private int major;
        private int minor;
        private int ind0;
        private int ind1;
        private int power;
        private int flags;
        private BluetoothItem device;
        private ArrayList<Byte> serviceData;
        private int serviceUUID;
        private String url;

        PDUiBeacon( BluetoothItem device ) {
            this.setDevice( device );
        }

        public boolean isValid() {
            boolean result = true;

            // is this a valid iBeacon? must be discoverable and not EDR dual mode compatible
            // note: this may eliminate simulated beacons using an iPhone so we may want to
            //       find a better filter down the road
            if ( manufacturerCode == LMP )
                result = false;
            else if ( ( flags & ( PDU.FLAGS_LE_GENERAL_DISCOVERABLE | PDU.FLAGS_LE_LIMITED_DISCOVERABLE ) ) == 0 )
                result = false;
            else if ( ( flags & ( PDU.FLAGS_CONTROLLER_DUAL | PDU.FLAGS_HOST_DUAL ) )!= 0 )
                result = false;

            return result;
        }

        public int getManufacturerCode() {
            return manufacturerCode;
        }

        public void setManufacturerCode(int manufacturerCode) {
            this.manufacturerCode = manufacturerCode;
        }

        public String getManufacturer() {
            return manufacturer;
        }

        public void setManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
        }

        public BluetoothItem getDevice() {
            return device;
        }

        public void clear() {
            this.manufacturerCode = LMP;
            this.manufacturer = null;
        }

        /**
         * Create a beacon based on the bluetooth device provided
         *
         * @param device the potential beacon device
         */
        public void setDevice(BluetoothItem device) {
            boolean needClear = true;
            this.device = device;

            if ( this.device != null ) {
                PDU ad0 = this.device.getPDUType( PDU.FLAGS );
                PDU ad1 = this.device.getPDUType( PDU.MANUFACTURER_SPECIFIC_DATA );

                PDU serviceData = this.device.getPDUType( PDU.SERVICE_DATA );

                if ( serviceData == null )
                    serviceData = this.device.getPDUType( PDU.MANUFACTURER_SPECIFIC_DATA );

                if ( serviceData != null ) {
                    this.serviceData = serviceData.getPayload();
                }

                // some beacons provide a service id so capture it here - note that
                // null is not an error
                PDU serviceUUID = this.device.getPDUType(PDU.UUID16 );
                if ( serviceUUID != null ) {
                    this.serviceUUID = serviceUUID.int16at(0);
                }


                // Check for iBeacon here
                if ( ( ad0 != null ) && (ad1 != null ) ) {
                    this.flags = ad0.int8at( 0 );
                    // I believe all iBeacons contain the same length, but we are going to err
                    // towards getting too many beacons instead of missing some
                    if ( ad1.getLength() >= 23 ) {
                        needClear = false;

                        this.manufacturerCode = ad1.int16at( 0 );
                        this.manufacturer = PDU.manufacturerName( this.manufacturerCode );
                        this.ind0 = ad1.int8at( 2 );
                        this.ind1 = ad1.int8at( 3 );
                        this.uuid = ad1.getUUID16At( 4 );
                        this.major = ad1.int16FlippedAt( 20 );
                        this.minor = ad1.int16FlippedAt( 22 );
                        this.power = ad1.int8at( 24 );
                    }
                }
            }

            if ( needClear )
                clear();
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public int getMajor() {
            return major;
        }

        public void setMajor(int major) {
            this.major = major;
        }

        public int getMinor() {
            return minor;
        }

        public void setMinor(int minor) {
            this.minor = minor;
        }

        public int getInd0() {
            return ind0;
        }

        public void setInd0(int ind0) {
            this.ind0 = ind0;
        }

        public int getInd1() {
            return ind1;
        }

        public void setInd1(int ind1) {
            this.ind1 = ind1;
        }

        public int getPower() {
            return power;
        }

        public void setPower(int power) {
            this.power = power;
        }

        public int getLMP() {
            return LMP;
        }

        public ArrayList<Byte> getServiceData() {
            return serviceData;
        }

        public void setServiceData(ArrayList<Byte> serviceData) {
            this.serviceData = serviceData;
        }

        public int getServiceUUID() {
            return serviceUUID;
        }

        public void setServiceUUID(int serviceUUID) {
            this.serviceUUID = serviceUUID;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getFlags() {
            return flags;
        }

        public void setFlags(int flags) {
            this.flags = flags;
        }
    }

    public static class RevealEventBase {

        /**
         * Serialize to JSON
         *
         * @param event
         * @param includeExtras
         *
         * @return A JSONObject representing the object or a null if an error occurs
         *
         * @note: this method should be getJSON but is renamed due to java static inheritance
         *        rules
         */
        public JSONObject getBaseJSON( RevealEvent event, boolean includeExtras ) {
            JSONObject result = new JSONObject();

            try {
                result.put("dwellTime", event.getDwellTime());
                result.put("lastSeenTime", event.getLastSeen());
                result.put("discoveryTime", event.getDiscoveryTime());

                RevealEvent.EventState state = event.getEventState();

                if (state != null) {
                    switch (state) {
                        case OPEN:
                            result.put("event_state", "OPEN");
                            break;

                        case CLOSED:
                            result.put("event_state", "CLOSED");
                            break;

                        default:
                            result.put("event_state", "unknown");
                            break;
                    }
                }

                try {
                    String ssid = event.getCurrentSSID();
                    if ( ssid != null)
                        result.put("currentSSID", ssid );

                    ssid = event.getCurrentBSSID();
                    if ( ssid != null)
                        result.put("currentBSSID", ssid);
                }
                catch ( JSONException e ) {
                    e.printStackTrace();
                }

                // Thes items aren't normally transmitted to reveal but need to be saved when
                // serializing the objects for storage locally
                if ( includeExtras ) {
                    result.put("lastNotificationTime", event.getLastNotificationTime() );
                    result.put("notes", event.getNotes() );
                    result.put("eventType", event.getEventType() );
                }
            }
            catch ( JSONException e ) {
                RevealLogger.e( "getBaseJSON() JSONException: " + e );
                e.printStackTrace();
            }

            return result;
        }

        /**
         * deserialize an object from JSON.
         *
         * @param json The JSONObject representing the item
         *
         * @note: this method should be setJSON but is renamed due to java static inheritance
         *        rules
         */
        public void setBaseJSON( RevealEvent event, JSONObject json ) {
            try {
                if (json.has("lastSeenTime"))
                    event.setLastNotificationTime( json.getLong( "lastSeenTime" ));

                if (json.has("discoveryTime"))
                    event.setDiscoveryTime( json.getLong( "discoveryTime" ));

                if ( json.has("event_state")) {
                    String state = json.getString( "event_state" );

                    if ( state != null ) {
                        if ( state.equalsIgnoreCase("OPEN") )
                            event.setEventState(RevealEvent.EventState.OPEN );
                        else
                            event.setEventState(RevealEvent.EventState.CLOSED );
                    }
                }

                if (json.has("currentSSID"))
                    event.setCurrentSSID( json.getString( "currentSSID" ));

                if (json.has("currentBSSID"))
                    event.setCurrentBSSID( json.getString( "currentBSSID" ));
            }
            catch( JSONException e ) {
                RevealLogger.e( "getBaseJSON() JSONException: " + e );
                e.printStackTrace();
            }
        }

    }

    public static class RevealBeacon extends RevealEventBase implements Parcelable, RevealEvent, JSONAble {
        public static final String[] PROXIMITY           = { "unknown", "immediate", "near", "far" };

        private String               address;
        private String               name;
        private String               proximityUUID;
        private String               major;
        private String               minor;
        private int                  txPower;
        private int                  proximity;
        private double               accuracy;
        private int                  rssi;
        private GlobalLocation       location;
        private String               beaconType;
        private String               url;
        private double               distance;
        private byte[]               payload;
        private int                  beaconTypeCode;
        private boolean              complete;
        private String               identifier;
        private CountDownTimer       sendTimer;
        private long                 discoveryTime;
        private long                 lastSeen;
        private String               currentSSID;
        private String               currentBSSID;
        private RevealAddress        streetAddress;
        private String               notes;
        private long                 lastNotificationTime;
        private EventState           eventState = OPEN;
        private int                  sendCount = 0;

        protected final int          magic = 9000;
        protected final int          beaconSchemaVersion = 1;

        public RevealBeacon() {
            Date date = new Date();
            discoveryTime = date.getTime() / 1000L;
        }

        public RevealBeacon( PDUiBeacon pduBeacon ) {
            Date date = new Date();
            discoveryTime = date.getTime() / 1000L;

            minor = "" + pduBeacon.getMinor();
            major = "" + pduBeacon.getMajor();
            identifier = pduBeacon.getUuid();
            proximityUUID = pduBeacon.getUuid();
            txPower = pduBeacon.getPower();
            name = pduBeacon.getDevice().getName();
            address = pduBeacon.getDevice().getAddress();
        }

        @Override
        public EventType getEventType() {
            return BEACON;
        }

        public int getBeaconTypeCode() {
            return beaconTypeCode;
        }

        /**
         * Calculate the accuracy value (this is really a distance, but keeping to the names
         * apple used)
         *
         * @param txPower the power level for this device
         * @param rssi the signal strength of the detected beacon
         *
         * @return a distance representing the radius size indicating the potential location of
         *         the beacon
         */
        public static double calculateAccuracy(int txPower, double rssi) {

            double result = Double.MAX_VALUE;
            double accuracy = -1;

            // this code borrowed from alt beacon so I have no explanation for the constant choices
            if ( (rssi != 0) && ( txPower != 0 ) ) {
                double ratio = rssi * 1.0 / txPower;
                if (ratio < 1.0) {
                    accuracy = Math.pow(ratio, 10);
                }
                else {
                    accuracy = (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
                }
            }

            // if the value is not infinite then attempt to limit the precision, otherwise return
            // the max value
            if ( !Double.isInfinite( accuracy ) ) {

                try {
                    result = Double.valueOf(new DecimalFormat("#.00").format(accuracy));
                }
                catch ( NumberFormatException e ) {
                    result = Double.MAX_VALUE;
                }
            }

            return result;
        }

        /**
         * Determine the time since discovery of the beacon
         *
         * @return age in seconds
         */
        public long age() {
            Date date = new Date();

            return ( date.getTime() / 1000L ) - discoveryTime;
        }

        /**
         * Serialize to JSON
         *
         * @return A JSONObject representing the object or a null if an error occurs
         */
        @Override
        public JSONObject getJSON() {
            return getJSON( false );
        }

        /**
         * Serialize to JSON
         *
         * @param includeExtras Include items not normally sent to the server
         * @return A JSONObject representing the object or a null if an error occurs
         */
        public JSONObject getJSON( boolean includeExtras ) {
            JSONObject result = getBaseJSON( this, includeExtras );

            try {
                String major = getMajor();
                String minor = getMinor();

                result.put("name", getName());
                result.put("txpower", getTxPower());
                result.put("beacon_uuid", getProximityUUID());

                if (major != null)
                    result.put("beacon_major", major);

                if (minor != null)
                    result.put("beacon_minor", minor);

                result.put("beacon_mac", getAddress());
                result.put("beacon_proximity", RevealBeacon.PROXIMITY[getProximity()]);
                result.put("beacon_accuracy", getAccuracy());
                result.put("beacon_distance", getDistance());
                result.put("beacon_type", getBeaconType());
                result.put("beacon_rssi", getRssi());

                if (getUrl() != null)
                    result.put("beacon_url", getUrl());

                result.put("beacon_vendor", getBeaconTypeCode());
                result.put("dwellTime", getDwellTime());
                result.put("lastSeenTime", getLastSeen());
                result.put("discoveryTime", getDiscoveryTime());
                result.put("proximityInt", getProximity() );

                // TODO: these duplicated fields should be eliminated after determining which
                //       ones Reveal uses
                result.put("type", getBeaconType());
                result.put("rssi", getRssi());
                result.put("proximity", RevealBeacon.PROXIMITY[getProximity()]);
                result.put("accuracy", getAccuracy());
                result.put("identity", getProximityUUID());

                if ( major != null )
                    result.put("major", major );

                if ( minor != null )
                    result.put("minor", minor );

                GlobalLocation location = getLocation();
                if (location != null) {
                    result.put("location", location.getJSON( includeExtras ));
                }
                else {
                    Reveal.RevealLogger.w( "Sending response to the server without a location");
                }

                byte[] data = getPayload();

                if ( data != null ) {
                    data = getPayload();
                    String encoded = Base64.encodeToString(data, 0, data.length, Base64.NO_WRAP);

                    result.put("beacon_payload", encoded);
                }

                String type = getBeaconType();

                if ( type != null ) {
                    if (type.equals("SecureCast")) {
                        try {
                            String prox = getProximityUUID();

                            if (prox.startsWith("0x"))
                                prox = prox.substring(2);

                            if (prox.length() <= 8) {
                                long value = Long.parseLong(prox, 16);
                                result.put("beacon_vendor_key", value);
                                result.put("key", value);
                            }
                        } catch (Exception e) {
                            Reveal.RevealLogger.e("ERROR converting value: beacon \""
                                    + getProximityUUID() + "\": " + e);
                        }

                    }
                }

            }
            catch ( JSONException e ) {

            }

            return result;
        }

        /**
         * deserialize an object from JSON.
         *
         * @param json The JSONObject representing the item
         * @return Returns true if successful and false if an error occurred (the exception should
         *         be logged in the console)
         */
        @Override
        public boolean setJSON( JSONObject json ) {
            boolean result = false;

            setBaseJSON( this, json );

            try {
                if (json.has("name"))
                    setName(json.getString("name"));

                if (json.has("txpower"))
                    setTxPower(json.getInt("txpower"));

                if (json.has("beacon_uuid"))
                    setProximityUUID(json.getString("beacon_uuid"));

                if (json.has("beacon_major"))
                    setMajor(json.getString("beacon_major"));

                if (json.has("beacon_minor"))
                    setMinor(json.getString("beacon_minor"));

                if (json.has("beacon_mac"))
                    setAddress(json.getString("beacon_mac"));

                if (json.has("proximityInt"))
                    setProximity(json.getInt("proximityInt"));

                if (json.has("beacon_accuracy"))
                    setAccuracy(json.getDouble("beacon_accuracy"));

                if (json.has("beacon_distance"))
                    setDistance(json.getDouble("beacon_distance"));

                if (json.has("beacon_type"))
                    setBeaconType(json.getString("beacon_type"));

                if (json.has("beacon_rssi"))
                    setRssi(json.getInt("beacon_rssi"));

                if (json.has("beacon_url"))
                    setUrl(json.getString("beacon_url"));

                if (json.has("beacon_vendor"))
                    setBeaconTypeCode(json.getInt("beacon_vendor"));

                if (json.has("beacon_payload")) {
                    String base64String =json.getString("beacon_payload");
                    if ( base64String != null ) {
                        setPayload( Base64.decode( base64String, Base64.DEFAULT ) );
                    }
                }

                // NOTE: not including the following as they are duplicates:
                //       type, rssi, proximity, accuracy, identity, major, minor

                result = true;
            }
            catch( JSONException e ) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            RevealBeacon beacon = (RevealBeacon) o;

            return address != null ? address.equals(beacon.address) : beacon.address == null;

        }

        @Override
        public int hashCode() {
            return address != null ? address.hashCode() : 0;
        }

        EventType eventType()
        {
            return BEACON;
        }

        /**
         * Device URL
         *
         * @return The URL encoded in the beacon (id1)
         */
        public String getUrl() {
            return this.url;
        }

        /**
         * The type of beacon as a string
         *
         * @return the name or Unknown code #
         */
        public String getBeaconType() {
            String result = null;

            if ( this.beaconType != null )
                result = this.beaconType;
            else
                result = "Unknown (" + this.getBeaconTypeCode() + ")";

            return result;
        }

        public HashMap<String, Object> getProperties() {
            HashMap<String, Object> result = new HashMap<String, Object>();

            result.put( "bluetoothAddress", address );
            result.put( "name", name );
            result.put( "beacon_uuid", proximityUUID );
            result.put( "major", major );
            result.put( "minor", minor );
            result.put( "txPower", txPower );
            result.put( "beacon_proximity", proximity );
            result.put( "beacon_accuracy", accuracy );
            result.put( "url", url );
            result.put( "beacon_rssi", rssi );
            result.put( "beacon_type", beaconType );
            result.put( "distance", distance );
            result.put( "payload", payload );
            result.put( "discoveryTime", discoveryTime );
            result.put( "dwellTime", getDwellTime() );
            result.put( "lastSeen", lastSeen );
            result.put( "currentSSID", currentSSID );
            result.put( "currentBSSID", currentBSSID );
            result.put( "identifier", identifier );
            result.put( "notes", notes );

            if ( location != null ) {
                HashMap<String, Object> locationMap = new HashMap<String, Object>();

                locationMap.put( "lon", location.getLongitude() );
                locationMap.put( "lat", location.getLatitude() );

                result.put( "location", locationMap );


            }

            if ( streetAddress != null )
                result.put( "address", streetAddress.getProperties() );

            return result;
        }

        @Override
        public double getDwellTime() {
            return Math.abs( lastSeen - discoveryTime );
        }

        @Override
        public long getLastSeen() {
            return lastSeen;
        }

        @Override
        public void setLastSeen(long lastSeen) {
            this.lastSeen = lastSeen;
        }

        private RevealBeacon(Parcel in) {
            if ( Utils.magicCheck( in, this.magic, this.beaconSchemaVersion, "RevealBeacon" )) {
                address = in.readString();
                name = in.readString();
                proximityUUID = in.readString();
                major = in.readString();
                minor = in.readString();
                txPower = in.readInt();
                proximity = in.readInt();
                accuracy = in.readDouble();
                rssi = in.readInt();
                url = in.readString();
                distance = in.readDouble();
                discoveryTime = in.readLong();
                currentSSID = in.readString();
                currentBSSID = in.readString();
                identifier = in.readString();
                String payloadString = in.readString();

                if (payloadString != null)
                    payload = hexStringToByteArray(payloadString);

                notes = in.readString();
                if ( in.readInt() == 1 )
                    location = GlobalLocation.CREATOR.createFromParcel(in);
                //location = in.readParcelable(getClass().getClassLoader());
            }
        }

        public void writeToParcel(Parcel out, int flags) {
            out.writeInt( magic );
            out.writeInt( beaconSchemaVersion );
            out.writeString(address);
            out.writeString(name);
            out.writeString(proximityUUID);
            out.writeString(major);
            out.writeString(minor);
            out.writeInt(txPower);
            out.writeInt(proximity);
            out.writeDouble(accuracy);
            out.writeInt(rssi);
            out.writeString(url);
            out.writeDouble(distance);
            out.writeLong( discoveryTime );
            out.writeString(currentBSSID);
            out.writeString(currentBSSID);
            out.writeString(identifier);

            String payloadString  = "";
            if ( payload != null )
                payloadString = bytesToHex( payload );
            out.writeString( payloadString );

            String notesString = "";
            if ( notes != null )
                notesString = notes;
            out.writeString( notesString );

            if ( location != null ) {
                out.writeInt( 1 );
                out.writeParcelable(location, 0);
            }
            else
                out.writeInt( 0 );
        }

        public static final Creator<RevealBeacon> CREATOR = new Creator<RevealBeacon>() {
            public RevealBeacon createFromParcel(Parcel in) {
                return new RevealBeacon(in);
            }
            public RevealBeacon[] newArray(int size) {
                return new RevealBeacon[size];
            }
        };

        @Override
        public int describeContents() {
            return hashCode();
        }

        public String beaconID() {
            StringBuilder builder = new StringBuilder();

            if ( proximityUUID != null )
                builder.append(proximityUUID);

            if ( major != null ) {
                builder.append(" (").append(major);

                if ( minor != null )
                    builder.append("/").append(minor);

                builder.append(")");
            }

            if ( ( url == null ) && ( major == null ) && ( address != null ))
                builder.append(" ").append( address );

            return builder.toString();
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append(beaconType).append(" ").append( beaconID() );

            if ( name != null )
                builder.append(" name: ").append(name);

            if ( url != null )
                builder.append(" ").append( url );

            if ( payload != null )
                builder.append(" payload: ").append( bytesToHex( payload ) );

            return builder.toString();
        }

        public String fullDescription() {
            StringBuilder builder = new StringBuilder();
            builder.append("Beacon [address: ").append(address)
                    .append(", name: ").append(name)
                    .append(", type: " ).append(beaconType)
                    .append(", proximityUUID: ").append(proximityUUID)
                    .append(", major: ").append(major)
                    .append(", minor: ").append(minor)
                    .append(", txPower: ").append(txPower)
                    .append(", proximity: ").append(proximity)
                    .append(", distance: ").append(distance)
                    .append(", accuracy: ").append(accuracy)
                    .append(", rssi: ").append(rssi)
                    .append(", location: ").append(location)
                    .append(", url: ").append( url )
                    .append(", payload=: ").append(bytesToHex( payload ) )
                    .append("]");

            return builder.toString();
        }

        public static String bytesToHex(byte[] chars)
        {
            StringBuilder hex = new StringBuilder();

            if ( chars != null ) {
                for (int i = 0; i < chars.length; i++) {
                    String byteHex = Integer.toHexString(((int) chars[i]) & 0xff);

                    if (byteHex.length() == 1 )
                        byteHex = "0" + byteHex;

                    hex.append( byteHex );
                }
            }

            return hex.toString();
        }

        public static byte[] hexStringToByteArray(String s) {
            int len = s.length();
            byte[] data = new byte[len / 2];
            for (int i = 0; i < len; i += 2) {
                data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                        + Character.digit(s.charAt(i+1), 16));
            }
            return data;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProximityUUID() {
            return proximityUUID;
        }

        public void setProximityUUID(String proximityUUID) {
            this.proximityUUID = proximityUUID;
        }

        public String getMajor() {
            return major;
        }

        public void setMajor( String major) {
            this.major = major;
        }

        public String getMinor() {
            return minor;
        }

        public void setMinor(String minor) {
            this.minor = minor;
        }

        public byte[] getPayload() {
            return payload;
        }

        public void setPayload( byte[] payload) {
            this.payload = payload;
        }

        public int getTxPower() {
            return txPower;
        }

        public void setTxPower(int txPower) {
            this.txPower = txPower;
        }

        public int getProximity() {
            return proximity;
        }

        public void setProximity(int proximity) {
            this.proximity = proximity;
        }

        public double getAccuracy() {
            return accuracy;
        }

        public void setAccuracy(double accuracy) {
            this.accuracy = accuracy;
        }

        public int getRssi() {
            return rssi;
        }

        public void setRssi(int rssi) {
            this.rssi = rssi;
        }

        public GlobalLocation getLocation() {
            return location;
        }

        public void setLocation(GlobalLocation location) {
            this.location = location;
        }

        public void setBeaconTypeCode(int beaconTypeCode) {
            this.beaconTypeCode = beaconTypeCode;
        }

        public void setComplete(boolean complete) {
            this.complete = complete;
        }

        public boolean isComplete() {
            return complete;
        }

        public void setBeaconType(String beaconType) {
            this.beaconType = beaconType;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public long getDiscoveryTime() {
            return discoveryTime;
        }

        public void setDiscoveryTime(long discoveryTime) {
            this.discoveryTime = discoveryTime;
        }

        public String getIdentifier() {
            return identifier;
        }

        public void setIdentifier(String identifier) {
            this.identifier = identifier;
        }

        public CountDownTimer getSendTimer() {
            return sendTimer;
        }

        public void setSendTimer(CountDownTimer sendTimer) {
            this.sendTimer = sendTimer;
        }

        public String getCurrentSSID() {
            return currentSSID;
        }

        public void setCurrentSSID(String currentSSID) {
            this.currentSSID = currentSSID;
        }

        public String getCurrentBSSID() {
            return currentBSSID;
        }

        public void setCurrentBSSID(String currentBSSID) {
            this.currentBSSID = currentBSSID;
        }

        public RevealAddress getStreetAddress() {
            return streetAddress;
        }

        public void setStreetAddress(RevealAddress streetAddress) {
            this.streetAddress = streetAddress;
        }

        @Override
        public long getLastNotificationTime() {
            return lastNotificationTime;
        }

        @Override
        public void setLastNotificationTime(long lastNotificationTime) {
            this.lastNotificationTime = lastNotificationTime;
        }

        @Override
        public EventState getEventState() {
            return eventState;
        }

        @Override
        public void setEventState(EventState eventState) {
            this.eventState = eventState;
        }

        @Override
        public String getNotes() {
            return notes;
        }

        @Override
        public void setNotes(String notes) {
            this.notes = notes;
        }

        public void combineWith(RevealBeacon other ) {
            if ( other.distance < this.distance ) {
                this.distance = other.distance;
                this.proximity = other.proximity;
                this.rssi = other.rssi;
            }

            if ( other.location != null )
                this.location = other.location;

            if ( other.address != null )
                this.address = other.address;

            if ( other.streetAddress != null )
                this.streetAddress = other.streetAddress;

            if ( other.url != null )
                this.url = other.url;

            if ( other.payload != null )
                this.payload = other.payload;
        }
    }

    private static Reveal sharedInstance = null;
    public static synchronized Reveal getInstance() {
        if (sharedInstance == null)
            sharedInstance = new Reveal();
        return sharedInstance;
    }

    // ******************************************************************************************
    // *                            RevealEventCache classes                                    *
    // ******************************************************************************************

    public interface RevealEvent {

        enum EventType {
            UNKNOWN, BEACON, ENTER, DWELL, EXIT, START, WIFI_ENTER, LOCATION
        }

        enum EventState {
            OPEN, CLOSED, ERROR
        }

        /**
         * Get the type of the event
         *
         * @return EventType constant for this object
         */
        EventType getEventType();

        String getIdentifier();

        double getDwellTime();

        /**
         * The time the event occurred
         *
         * @return time in milliseconds
         */
        long getDiscoveryTime();
        void setDiscoveryTime(long ticks);

        long getLastSeen();
        void setLastSeen(long value);

        /**
         * The active network SSID when the event was discovered
         *
         * @return the SSID string
         */
        String getCurrentSSID();
        void setCurrentSSID(String currentSSID);

        /**
         * The active network SSID when the event was discovered
         *
         * @return the BSSID as a string
         */
        String getCurrentBSSID();
        void setCurrentBSSID(String currentBSSID);

        /**
         * Notes to be passed back to the server about discovery process
         *
         * @return arbitrary debug text
         */
        String getNotes();
        void setNotes(String notes);

        /**
         * This represents the last partial event send time.
         */
        long getLastNotificationTime();
        void setLastNotificationTime(long value);

        /**
         * The current state of the beacon or event
         *
         * States:
         *      OPEN = the beacon (or other marker) is still in sight
         *      CLOSED = the beacon (or other state) is no longer in sight
         *      ERROR = there was a problem with this item
         */
        EventState getEventState();
        void setEventState(EventState value);
    }


    public interface RevealEventListener {
        /**
         * This method is called whenever you need to persist a set o beacons
         *
         * @param events the eventCache that need to be handled
         */
        void onCacheReady(ArrayList<RevealEvent> events);
    }

    public static class RevealEventCache  implements JSONAble {
        private ArrayList<RevealEvent> events = new ArrayList<RevealEvent>();
        private int maxCachedEvents = 50;
        private int maxCachedEventsOverrun = maxCachedEvents * 5;
        private long idleTimeout = 1 * 60;
        private RevealEventListener batchReadyListener = null;
        private CountDownTimer idleTimer;

        public void addEvent( RevealEvent event )
        {
            synchronized ( this ) {
                events.add( event );

                while ( events.size() > this.maxCachedEventsOverrun ) {
                    events.remove( 0 );
                }
            }

            Reveal.RevealLogger.v( "RevealEventCache add (" + events.size() + ") added: " + event );

            (new Handler (Looper.getMainLooper ())).post (new Runnable ()
            {
                @Override
                public void run ()
                {
                    if ( RevealEventCache.this.idleTimer != null )
                        RevealEventCache.this.idleTimer.cancel();
                    RevealEventCache.this.idleTimer = null;

                    if ( events.size() >  maxCachedEvents )
                        flushEvents();
                    else {
                        RevealEventCache.this.idleTimer = new CountDownTimer( idleTimeout * 1000, 60000 ) {

                            @Override
                            public void onTick(long millisUntilFinished) {
                            }

                            @Override
                            public void onFinish() {
                                flushEvents();
                                RevealEventCache.this.idleTimer = null;
                            }
                        }.start();
                    }
                }
            });


        }

        public void flushEvents() {
            boolean inBackground = Reveal.getInstance().isInBackGround();
            boolean sendInBackground = Reveal.getInstance().batchBackgroundSend;

            if ( !inBackground || sendInBackground || ( count(RevealEvent.EventType.ENTER ) > 0 ) ) {

                if ( !Reveal.getInstance().isIdle() ) {
                    ArrayList<RevealEvent> eventList = getEventsAndClear();

                    RevealLogger.v("RevealEventCache flushEvents - have " + eventList.size() + " eventCache to send");
                    if (this.batchReadyListener != null) {
                        this.batchReadyListener.onCacheReady(eventList);
                    }
                }
            }
        }

        public int count( RevealEvent.EventType typeTofFind ) {
            int result = 0;
            Iterator<RevealEvent> iter = events.iterator();

            while (iter.hasNext()) {
                RevealEvent obj = iter.next();

                if ( obj.getEventType() == typeTofFind )
                    result++;
            }

            return result;
        }

        /**
         * Serialize to JSON
         *
         * @return A JSONObject representing the object or a null if an error occurs
         */
        @Override
        public JSONObject getJSON() {
            return getJSON( false );
        }

        /**
         * Serialize to JSON
         *
         * @param includeExtras Include items not normally sent to the server
         * @return A JSONObject representing the object or a null if an error occurs
         */
        public JSONObject getJSON( boolean includeExtras ) {
            JSONObject result = new JSONObject();

            try {
                ArrayList<RevealEvent> events = new ArrayList<RevealEvent>( this.events );

                JSONArray jsonEvents = new JSONArray();

                for( RevealEvent revealEvent : events ) {
                    JSONAble event = (JSONAble) revealEvent;
                    jsonEvents.put( event.getJSON( true ) );
                }

                if ( jsonEvents.length() > 0 ) {
                    try {
                        result.put("events", jsonEvents );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            catch ( Exception e ) {
                e.printStackTrace();
            }

            return result;
        }

        /**
         * deserialize an object from JSON.
         *
         * @param json The JSONObject representing the item
         * @return Returns true if successful and false if an error occurred (the exception should
         *         be logged in the console)
         */
        @Override
        public boolean setJSON( JSONObject json ) {
            boolean result = false;

            try {
                if (json.has("events")) {
                    JSONArray array = json.getJSONArray( "events" );

                    for( int index=0 ; index < array.length() ; index++ ) {
                        JSONObject item = (JSONObject) array.get( index );
                        if ( item.has("eventType" )) {
                            RevealEvent.EventType type = RevealEvent.EventType.values()[item.getInt( "eventType" )];

                            switch ( type ) {
                                case BEACON:
                                    RevealBeacon beacon = new RevealBeacon();

                                    beacon.setJSON( item );
                                    addEvent( beacon );
                                    break;

                                case LOCATION:
                                    RevealLocation location = new RevealLocation();

                                    location.setJSON( item );
                                    addEvent( location );
                                    break;
                            }
                        }
                    }
                }

                result = true;
            }
            catch( JSONException e ) {
                e.printStackTrace();
            }

            return result;
        }


        public ArrayList<RevealEvent> getEventsAndClear() {
            ArrayList<RevealEvent> result = null;

            synchronized ( this ) {
                result = this.events;

                this.events = new ArrayList<RevealEvent>();

                RevealLogger.v( "RevealEventCache getEventsAndClear - have " + result.size() + " eventCache to send" );
            }

            return result;
        }

        public int getMaxCachedEvents() {
            return maxCachedEvents;
        }

        public void setMaxCachedEvents(int maxCachedEvents) {
            this.maxCachedEvents = maxCachedEvents;
        }

        public long getIdleTimeout() {
            return idleTimeout;
        }

        public void setIdleTimeout(long idleTimeout) {
            this.idleTimeout = idleTimeout;
        }

        public RevealEventListener getBatchReadyListener() {
            return batchReadyListener;
        }

        public void setBatchReadyListener(RevealEventListener batchReadyListener) {
            this.batchReadyListener = batchReadyListener;
        }

        public int getMaxCachedEventsOverrun() {
            return maxCachedEventsOverrun;
        }

        public void setMaxCachedEventsOverrun(int maxCachedEventsOverrun) {
            this.maxCachedEventsOverrun = maxCachedEventsOverrun;
        }
    }

    public interface RevealDwellEventListener {
        /**
         * This method is called a beacon's dwell time
         *
         * @param event the event that should be handled
         */
        void onEventReady(RevealEvent event);
    }

    public static class RevealDwellManager implements JSONAble, Parcelable {
        protected HashMap<RevealEvent.EventType,Double> eventTypeTimes = new HashMap<RevealEvent.EventType, Double>();
        protected final ConcurrentHashMap<String,RevealEvent> pendingEvents = new ConcurrentHashMap<String, RevealEvent>();
        protected RevealDwellEventListener eventReadyListener;

        protected final int          magic = 9090;
        protected final int          beaconSchemaVersion = 1;

        public void addEvent( RevealEvent event ) {
            if (event != null) {
                String key = event.getIdentifier();

                Reveal.RevealLogger.d("[IDLE-TEST] RevealBeacon addEvent: " + event);

                Date date = new Date();
                event.setLastSeen(date.getTime() / 1000L);

                if (key != null) {
                    if (key.length() > 0) {

                        RevealEvent old = pendingEvents.get(key);

                        if (old != null) {
                            if (old.getEventType() == BEACON) {
                                RevealBeacon beacon = (RevealBeacon) event;
                                RevealBeacon oldBeacon = (RevealBeacon) old;
                                oldBeacon.setLastSeen( event.getLastSeen() );
                                oldBeacon.combineWith(beacon);

                                Reveal.RevealLogger.d("[IDLE-TEST] RevealBeacon addEvent combined: " + event);
                            }
                        } else {
                            pendingEvents.put(key, event);

                            if (event.getEventType() == BEACON) {
                                RevealBeacon beacon = (RevealBeacon) event;

                                beacon.location = new GlobalLocation(Reveal.getInstance().locationService.getCurrentLocation(Reveal.getInstance().applicationContext));
                            }
                        }
                    }

                    processPendingEvents();
                }
            }
        }

        public void addEventType(RevealEvent.EventType type, Double lossDelay) {
            if (eventTypeTimes.containsKey(type)) {
                eventTypeTimes.remove(type);
            }

            eventTypeTimes.put(type, lossDelay);
        }

        public void processInProgress(long maxTimeSinceAccess) {
            if (this.getEventReadyListener() != null) {
                ArrayList<Reveal.RevealEvent> events = getOldEvents(maxTimeSinceAccess);

                for (RevealEvent event : events) {
                    this.getEventReadyListener().onEventReady(event);
                }
            }
        }

        public ArrayList<Reveal.RevealEvent> getOldEvents(long maxTimeSinceAccess) {
            ArrayList<RevealEvent> result = new ArrayList<RevealEvent>();

            Date date = new Date();

            // current time in seconds
            long now = (long) ( date.getTime() / 1000.0 );

            HashMap<String,RevealEvent> events = null;

            for (String key : this.pendingEvents.keySet()) {
                RevealEvent event = this.pendingEvents.get(key);

                if (event != null) {
                    long lastNotificationTime = event.getLastNotificationTime();

                    // comment the next few lines out if you want to get an event notification
                    // on creation
//                      if (lastNotificationTime == 0) {
//                         lastNotificationTime = now;
//                            event.setLastNotificationTime(lastNotificationTime);
//                        }

                    // get difference
                    long diff = now - lastNotificationTime;

                    // add this one if it has been long enough
                    if (diff >= maxTimeSinceAccess) {
                            event.setLastNotificationTime(now);
                            result.add(event);
                    }
                }
            }

            return result;
        }

        public void processPendingEvents() {
            boolean isIdle = false;

            PowerManager pm = (PowerManager) Reveal.getInstance().applicationContext.getSystemService(Context.POWER_SERVICE);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                isIdle = pm.isDeviceIdleMode();
            }

            if ( !isIdle ) {
                // we are in a maintenance window - so lets release all the batched items\

                Reveal.log( "processPendingEvents event count: " + this.pendingEvents.size(), "IDLE-TEST]" );

                // loop through each and see if any have been out of sight long enough to be sent
                for (String key : this.pendingEvents.keySet()) {
                    RevealEvent event = this.pendingEvents.get(key);

                    if (event.getLastSeen() > 0) {
                        Date date = new Date();
                        Double now = date.getTime() / 1000.0;

                        // get the time since seen
                        Double interval = Math.abs((now - event.getLastSeen()));
                        Double threshold = 0.0;

                        // see if this is a device we monitor dwell times on
                        if (this.eventTypeTimes.containsKey(event.getEventType()))
                            threshold = this.eventTypeTimes.get(event.getEventType());

                        // if it has been long enough send it then remove it from the list
                        if (interval >= threshold) {
                            event.setNotes("type " + event.getEventType() + "  interval=" + interval + " > " + threshold + "  discovery=" + event.getDiscoveryTime() + " lastSeen=" + event.getLastSeen() + " now=" + now + " dwell=" + event.getDwellTime());
                            Reveal.log("[] Adding READY event because " + event.getNotes() + "");

                            if ( event.getEventType() == RevealEvent.EventType.BEACON ) {
                                RevealBeacon beacon = (RevealBeacon) event;
                                Reveal.log( "Beacon being released from dwell manager (normal expiration): " + beacon.beaconType + " " + beacon.identifier, "STATE" );
                            }
                            else if ( event.getEventType() == RevealEvent.EventType.ENTER ) {
                                RevealLocation location = (RevealLocation) event;
                                Reveal.log( "Location being released from dwell manager (normal expiration): lon: " + location.getLocation().longitude + " lat: " + location.getLocation().latitude, "STATE" );
                            }
                            else
                                Reveal.log( "Event being released from dwell manager (normal expiration): " + event, "STATE" );

                            if (this.eventReadyListener != null)
                                this.eventReadyListener.onEventReady(event);
                            this.pendingEvents.remove(key);
                        }
                    }
                }
            }
            else {
                Reveal.log( "We are delaying processing events because we are in idle mode count: " + this.pendingEvents.size(), "IDLE-TEST" );
            }

            Reveal.getInstance().storeInPreferences();
        }

        public void releaseAll() {
            for( String key : this.pendingEvents.keySet() ) {
                RevealEvent event = this.pendingEvents.get( key );

                if ( event.getLastSeen() > 0 ) {
                    if ( event.getEventType() == RevealEvent.EventType.BEACON ) {
                        RevealBeacon beacon = (RevealBeacon) event;
                        Reveal.log( "Beacon being released from dwell manager (releaseAll): " + beacon.beaconType + " " + beacon.identifier, "STATE" );
                    }
                    else if ( event.getEventType() == RevealEvent.EventType.ENTER ) {
                        RevealLocation location = (RevealLocation) event;
                        Reveal.log( "Location being released from dwell manager (releaseAll): lon: " + location.getLocation().longitude + " lat: " + location.getLocation().latitude, "STATE" );
                    }
                    else
                        Reveal.log( "Event being released from dwell manager (releaseAll): " + event, "STATE" );

                    if (this.eventReadyListener != null)
                        this.eventReadyListener.onEventReady(event);
                    this.pendingEvents.remove(key);
                }
            }

            Reveal.getInstance().storeInPreferences();
        }

        public ArrayList<RevealEvent> getPendingEvents() {
            return new ArrayList<RevealEvent>(this.pendingEvents.values());
        }

        public RevealDwellEventListener getEventReadyListener() {
            return eventReadyListener;
        }

        public void setEventReadyListener(RevealDwellEventListener eventReadyListener) {
            this.eventReadyListener = eventReadyListener;
        }

        @Override
        public int describeContents() {
            return hashCode();
        }

        public RevealDwellManager() {

        }

        public RevealDwellManager(Parcel in) {
            String json = in.readString();

            if ( json != null ) {
                try {
                    JSONObject obj = new JSONObject(json);

                    setJSON( obj );
                } catch ( JSONException e ) {
                    e.printStackTrace();
                }
            }
        }



        /**
         * Serialize to JSON
         *
         * @return A JSONObject representing the object or a null if an error occurs
         */
        @Override
        public JSONObject getJSON() {
            return getJSON( false );
        }

        /**
         * Serialize to JSON
         *
         * @param includeExtras Include items not normally sent to the server
         * @return A JSONObject representing the object or a null if an error occurs
         */
        public JSONObject getJSON( boolean includeExtras ) {
            JSONObject result = new JSONObject();

            try {
                ArrayList<RevealEvent> events = new ArrayList<RevealEvent>( pendingEvents.values() );

                JSONArray beacons = new JSONArray();
                JSONArray locations = new JSONArray();

                for( RevealEvent event : events ) {
                    RevealEvent.EventType eventType = event.getEventType();

                    switch ( eventType ) {
                        case BEACON:
                            RevealBeacon beacon = (RevealBeacon) event;
                            JSONObject jbeacon = beacon.getJSON( true );
                            beacons.put( jbeacon );
                            break;

                        case ENTER:
                            RevealLocation loc = (RevealLocation) event;
                            JSONObject jloc = loc.getJSON( true );
                            locations.put( jloc );
                            break;

                        case WIFI_ENTER:
                            break;

                        default:
                            Reveal.RevealLogger.w("Unknown event type " + eventType + " encountered");
                            break;
                    }
                }

                if ( beacons.length() > 0 ) {
                    try {
                        result.put("beacons", beacons);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if ( locations.length() > 0 ) {
                    try {
                        result.put("locations", locations);
                        Reveal.log( "Adding " + locations.length() + " locations to batch", "STATE" );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            catch ( Exception e ) {
                e.printStackTrace();
            }

            return result;
        }

        /**
         * deserialize an object from JSON.
         *
         * @param json The JSONObject representing the item
         * @return Returns true if successful and false if an error occurred (the exception should
         *         be logged in the console)
         */
        @Override
        public boolean setJSON( JSONObject json ) {
            boolean result = false;

            try {
                if (json.has("beacons")) {
                    JSONArray array = json.getJSONArray( "beacons" );

                    for( int index=0 ; index < array.length() ; index++ ) {
                        JSONObject item = (JSONObject) array.get( index );
                        RevealBeacon beacon = new RevealBeacon();

                        beacon.setJSON( item );
                        addEvent( beacon );
                    }
                }


                if (json.has("locations")) {
                    JSONArray array = json.getJSONArray( "locations" );

                    for( int index=0 ; index < array.length() ; index++ ) {
                        JSONObject item = (JSONObject) array.get( index );
                        RevealLocation loc = new RevealLocation();

                        loc.setJSON( item );
                        addEvent( loc );
                    }
                }

                result = true;
            }
            catch( JSONException e ) {
                e.printStackTrace();
            }

            return result;
        }

        public void writeToParcel(Parcel out, int flags) {
            JSONObject obj = getJSON( true );
            out.writeString( obj.toString() );
        }

        public static final Creator<RevealDwellManager> CREATOR = new Creator<RevealDwellManager>() {
            public RevealDwellManager createFromParcel(Parcel in) {
                return new RevealDwellManager(in);
            }
            public RevealDwellManager[] newArray(int size) {
                return new RevealDwellManager[size];
            }
        };

        public int getCount() {
            return this.pendingEvents.size();
        }
    }

    // ******************************************************************************************
    // *                            Reveal                                                      *
    // ******************************************************************************************

    public static String getVersion() {
        return BuildConfig.VERSION_NAME;
    }

    private static final String REVEAL_API_BASE_PRODUCTION = "https://sdk.revealmobile.com";
    private static final String REVEAL_API_BASE_SANDBOX = "http://sandboxsdk.revealmobile.com";

    public static final String BLUETOOTH_SCAN_INTERVAL_KEY = "BLUETOOTH_SCAN_INTERVAL_KEY";
    public static final String BLUETOOTH_SCAN_DURATION_KEY = "BLUETOOTH_SCAN_DURATION_KEY";
    public static final String FOUND_BEACONS_PREFERENCE_NAME = "Reveal_Beacon_Preferences";
    public static final String FOUND_BEACONS_PREFERENCE_KEY = "FOUND_BEACONS_KEY";
    public static final String PENDING_BEACONS_PREFERENCE_KEY = "PENDING_BEACONS_KEY";
    public static final String PERSONAS_PREFERENCE_NAME = "Reveal_Personas";
    public static final String PERSONAS_PREFERENCE_KEY = "personas";

    private BeaconService beaconService;
    private LocationService locationService;
    private String apiKey = null;
    private String apiBaseURL = REVEAL_API_BASE_PRODUCTION;
    private Boolean isDebug = false;

    private Boolean isBeaconScanningEnabled = true;
    private Boolean isBackgroundScanningEnabled = true;
    private List<String> debugUUIDs = new ArrayList<String>();
    private List<String> personas = new ArrayList<String>();
    private Boolean locationSharingEnabled = false;
    private RevealEventCache eventCache = new RevealEventCache();
    private boolean batchBackgroundSend = true;

    private ConcurrentHashMap<String, Integer> successStats = new ConcurrentHashMap<String, Integer>();
    private ConcurrentHashMap<String, Integer> failureStats = new ConcurrentHashMap<String, Integer>();
    private Date startTime = new Date();
    private long retryDelayAfterTimeout = 60;
    private DozeModeReceiver dozeReciever = new DozeModeReceiver();
    private OnLocationFoundListener locationFoundListener = null;
    private Timer memoryCheckTimer = null;
    private TimerTask memoryCheckTimerTask = null;

    private static final int MAX_PERMISSION_POLL_CHECK = 30000;

    /**
     * Thw UI logger - this will be called for each message to be sent from any thread. For
     * internal testing only.
     */
    private UserLogListener logger;

    private Context applicationContext = null;

    private RevealDwellManager dwellManager = new RevealDwellManager();

    private DistanceCalculator distanceCalculator = null;

    private DistanceCalculator defaultDistanceCalculator = null;

    private HashMap<String, RevealStatus> statuses = new HashMap<String, RevealStatus>();

    private HashMap<String, Boolean> permissionStates = new HashMap<String, Boolean>();

    private boolean needRegisteration = true;

    private long incompleteBeaconSendTime = 60 * 60;
    private int simulateMemoryWarning = 0;

    public Reveal(){
        super();

        setStatus( STATUS_BLUETOOTH, STATUS_FAIL, "Bluetooth state unknown" );
        setStatus( STATUS_LOCATION, STATUS_FAIL, "Location state unknown" );
        setStatus( STATUS_NETWORK, STATUS_FAIL, "Network state unknown" );
        setStatus( STATUS_MEMORY, STATUS_SUCCEED, "No memory issues" );
        setStatus( STATUS_SCAN, STATUS_SUCCEED, "Not currently scanning" );
    }

    public String getDeviceState( Context context ) {
        String text = "";

        if ( context == null )
            context = applicationContext;

        text = text + "\n  Version: " + Reveal.getVersion();
        text = text + "\n  Version: " + Reveal.getInstance().getAPIBaseURL();

        if ( context != null ) {
            if (Reveal.selfPermissionGranted( context, Manifest.permission.ACCESS_WIFI_STATE)) {
                if (Utils.isWiFi(context)) {
                    WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();

                    if (wifiInfo != null) {
                        text = text + "\n  Wifi: " + wifiInfo.getSSID();
                    }
                }
            }

            text = text + "\n  Permissions: ";

            if (Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_FINE_LOCATION) )
                text = text + "FINE ";

            if (Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_COARSE_LOCATION) )
                text = text + "COARSE ";

            if (Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_WIFI_STATE) )
                text = text + "WIFI ";

            if (Reveal.selfPermissionGranted(context, Manifest.permission.BLUETOOTH) )
                text = text + "BLUETOOTH ";

            if (Reveal.selfPermissionGranted(context, Manifest.permission.BLUETOOTH_ADMIN) )
                text = text + "BT-ADMIN ";

            if (Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_NETWORK_STATE) )
                text = text + "WIFI ";
        }

        text = text + "\n  Active beacons: " + dwellManager.getCount();

        for( RevealStatus status : this.statuses.values() ) {
            text = text + "\n  Status: " + status.getName() + "=" + status.getStatus() + " - " + status.getMessage();
        }

        final Runtime runtime = Runtime.getRuntime();
        final long usedMemInMB=(runtime.totalMemory() - runtime.freeMemory()) / 1048576L;
        final long maxHeapSizeInMB=runtime.maxMemory() / 1048576L;
        final long availHeapSizeInMB = maxHeapSizeInMB - usedMemInMB;

        text = text  + "\n  System Memory used: " + usedMemInMB + "MB\n  heap size: " + maxHeapSizeInMB + "MB\n  Avail: " + availHeapSizeInMB + "MB";

        return "Device state:" + text;
    }

    public RevealStatus getStatus( String whichStatus ) {
        RevealStatus result = null;

        if ( statuses.containsKey( whichStatus ) )
            result = statuses.get( whichStatus );

        return result;
    }

    public void setStatus( String whichStatus, int state, String message ) {
        RevealStatus status = getStatus( whichStatus );

        if ( status == null ) {
            status = new RevealStatus();

            status.setName( whichStatus );
            statuses.put( whichStatus, status );
        }

        status.setStatus( state );
        status.setMessage( message );
    }

    public boolean isIdle() {
        boolean result = false;

        if ( applicationContext != null ) {
            PowerManager pm = (PowerManager) applicationContext.getSystemService(Context.POWER_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (pm != null)
                    result = pm.isDeviceIdleMode();
            }
        }

        return result;
    }

    protected void setupDwellManager() {

        dwellManager.setEventReadyListener(new RevealDwellEventListener() {
            @Override
            public void onEventReady(RevealEvent event) {
                Reveal.getInstance().addEvent( Reveal.getInstance().applicationContext, event );
            }
        });

        eventCache.setBatchReadyListener(new RevealEventListener() {
            @Override
            public void onCacheReady(final ArrayList<RevealEvent> events) {
            Reveal.this.sendBatch( events );
            }
        });

        Reveal.this.loadFromPreferences();

        // we have loaded the saved elements so see if we need to kick start it
        sendBatch( eventCache.events );
    }

    public void sendBatch( ArrayList<RevealEvent> events ) {
        Reveal.RevealLogger.d( "[IDLE-TEST] setBatchReady beaconService=" + Reveal.this.beaconService );

        if (events.size() == 0)
            return;

        // try to find groups of maxCachedEvents size to send
        int firstEventPositionToSend = 0, lastEventPositionToSend = 0;

        do {
            // Find the last position in this group we want to send (don't go over the end of the array)
            lastEventPositionToSend += Reveal.this.eventCache.maxCachedEvents - 1;

            // If we're over the end of the array, just truncate to the end of the array
            if (events.size() < lastEventPositionToSend + 1)
                lastEventPositionToSend = events.size() - 1;

            final List<RevealEvent> eventsToSend = events.subList(firstEventPositionToSend,lastEventPositionToSend);

            // send batch of found group to server making sure to only send if not empty
            if (!eventsToSend.isEmpty()) {
                Reveal.log("Reveal sending batch of " + eventsToSend.size() + " events to the server", "COMM");
                RevealNetworkClient.sendBatch(applicationContext, eventsToSend, new RevealNetworkClientCallback() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        Reveal.log("Batch events successfully sent", "COMM");
                        if (response != null) {
                            JSONArray personasJSONArray = response.optJSONArray("personas");
                            if (personasJSONArray != null) {
                                setPersonasWithJSON(personasJSONArray);
                            }
                        }

                        // This is removed since we aren't using it yet do to Parcel issues
                        Reveal.this.storeInPreferences();
                    }

                    @Override
                    public void onFailure(String response) {
                        Reveal.log("Batch upload error " + response, "ERROR", "COMM");
                        Reveal.log("Rescheduling these events after " + Reveal.getInstance().retryDelayAfterTimeout + " seconds", "COMM");

                        // Check for errors that we resend on
                        if (response.toLowerCase(Locale.getDefault()).contains("timed out")) {
                            // Put these events back in the queue to resend after a delay
                            // so that we don't burn the battery sending when the server is
                            // down
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    // note that the events will now be out of order but
                                    // that shouldn't matter
                                    for (RevealEvent event : eventsToSend) {
                                        Reveal.getInstance().addDwellEvent(event);
                                    }

                                    Reveal.log("Rescheduled " + eventsToSend.size() + " events to be resent", "COMM");
                                }
                            }, Reveal.this.getRetryDelay());
                        }
                    }
                });
            }
            // Update first position to batch size plus first position
            firstEventPositionToSend += Reveal.this.eventCache.maxCachedEvents;
        } while (events.size() > firstEventPositionToSend);

        Reveal.this.storeInPreferences();
    }

    public static final String DWELL_PREFS = "dwellManager";
    public static final String CACHE_PREFS = "cachePrefs";

    public void storeInPreferences() {

        SharedPreferences settings = this.applicationContext.getSharedPreferences( DWELL_PREFS, 0);
        SharedPreferences.Editor editor = settings.edit();
        String json = dwellManager.getJSON( true ).toString();
        editor.putString( DWELL_PREFS, json );

        json = eventCache.getJSON( true ).toString();
        editor.putString( CACHE_PREFS, json );
        //Reveal.log( "storeInPreferences cache: " +  json, "[STATE]");
        editor.apply();
    }

    public void loadFromPreferences() {
        SharedPreferences sharedPreferences = this.applicationContext.getSharedPreferences(DWELL_PREFS, Context.MODE_PRIVATE );
        String dwellJson = sharedPreferences.getString(DWELL_PREFS, "{}");
        String cacheJson = sharedPreferences.getString(CACHE_PREFS, "{}");

        try {
            this.eventCache.setJSON( new JSONObject( cacheJson ) );
            Reveal.log( "Loaded cache events from last session: " + cacheJson, "STATE");

            this.dwellManager.setJSON(new JSONObject(dwellJson));
            Reveal.log( "Loaded dwell events from last session: " + dwellJson, "STATE");
        }
        catch ( JSONException e ) {
            e.printStackTrace();
        }


    }

    public long getRetryDelay() {
        // TODO: change this to use and a staggered delay based on how long we've been waiting
        return retryDelayAfterTimeout * 1000;
    }

    public static final boolean selfPermissionGranted(final Context context, String permission) {
        // For Android < Android M, self permissions are always granted.
        boolean result = true;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (targetSdkVersion >= Build.VERSION_CODES.M) {
                // targetSdkVersion >= Android M, we can
                // use Context#checkSelfPermission
                result = context.checkSelfPermission(permission)
                        == PackageManager.PERMISSION_GRANTED;
            } else {
                // targetSdkVersion < Android M, we have to use PermissionChecker
                result = PermissionChecker.checkSelfPermission(context, permission)
                        == PermissionChecker.PERMISSION_GRANTED;
            }
        }

        return result;
    }

    protected void onPermissionStateChange( Context context, String permission, boolean state ) {
        Reveal.log( "Permission state for " + permission + " changed to " + state, "STATE in context: " + context );

    }

    protected void checkPermissionState( Context context ) {
        if  ( ( checkPermissionState( context, Manifest.permission.ACCESS_FINE_LOCATION )
        || checkPermissionState( context, Manifest.permission.ACCESS_COARSE_LOCATION ) )
        && checkPermissionState( context, Manifest.permission.BLUETOOTH  ) ) {
            if ( needRegisteration )
                this.sendRegistrationIfPermitted( context );
        }

        //
        checkPermissionState( context, Manifest.permission.BLUETOOTH_ADMIN );
    }

    protected boolean checkPermissionState( Context context, String permission ) {
        boolean state = Reveal.selfPermissionGranted(context, permission );

        boolean existingState = false;

        if ( permissionStates.containsKey( permission) ) {
            existingState = permissionStates.get(permission);

            permissionStates.remove( permission );
        }

        permissionStates.put( permission, state );

        if ( existingState != state )
            onPermissionStateChange( context, permission, state );

        return state && ! existingState;
    }

    public void start(final Application application ) {
        needRegisteration = true;
        RevealLogger.d( "STARTUP - Reveal.start() called" );

        Reveal.log( "Reveal.Start\nVERSION=" + Reveal.getInstance().getVersion()
                + "\nREVEAL_API_KEY=" + apiKey
                + "\nSERVER_URL="
                + Reveal.this.getAPIBaseURL(), "INIT" );

        Reveal.log( getDeviceState( applicationContext ), "INIT" );

        if ( application != null ) {
            this.applicationContext = application.getApplicationContext();

            //Setup lifecycle callbacks to handle foreground/background issues
            application.registerActivityLifecycleCallbacks(this);
        }

        try {
            boolean hasBluetooth = Reveal.selfPermissionGranted(application, Manifest.permission.BLUETOOTH);

            IntentFilter filter = new IntentFilter();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                filter.addAction(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED);
            }

            this.applicationContext.registerReceiver( this.dozeReciever, filter);

            defaultDistanceCalculator =  new ModelSpecificDistanceCalculator(this.applicationContext, BeaconManager.getDistanceModelUpdateUrl());
            setDistanceCalculator(defaultDistanceCalculator);

            if (hasBluetooth) {
                setupDwellManager();

                this.startTime = new Date();
                sendRegistrationIfPermitted(application );
                Reveal.getInstance().recordEvent("start");

                // Setup wifi scanning
//                wifiManager = (WifiManager) application.getSystemService(Context.WIFI_SERVICE);
//                application.registerReceiver(mWifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            }
            else {
                Reveal.log("Bluetooth access denied: No bluetooth", "ERROR", "STATE");
                throw new RuntimeException( "You need to add BLUETOOTH permission to use this SDK" );
            }
        }
        catch( SecurityException e ) {
            // http://stackoverflow.com/questions/33245977/java-lang-securityexception-bluetooth-permission-crash-samsung-devices-only
            boolean isKNOXError = false;
            String message = e.getMessage();

            if ( message != null )
                if ( message.toLowerCase( Locale.getDefault()).contains( "need bluetooth permission"))
                    isKNOXError = true;

            if ( isKNOXError ) {
                Reveal.log("Bluetooth access denied (KNOX)", "ERROR", "STATE");
            }
            else {
                Reveal.log("Bluetooth access denied: " + e, "ERROR", "STATE");
                stop();
                // rethrow the error if it isn't the one we expected
                throw e;
            }
        }
        finally {
            // start timer at one second
            scheduleTimer( 3000 );

            Reveal.log( "Reveal SDK started", "STATE");
        }
    }

    private void scheduleTimer( final int intervalMS ) {
        Reveal.log( "Scheduled background timer starting at " + intervalMS + "ms", "STATE");
        new Handler( Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if ( memoryCheckTimer != null ) {
                    memoryCheckTimer.cancel();
                    memoryCheckTimer = null;
                }

                // Check memory based on a timer that gets longer up to the defined maximum in
                // order to reduce the CPU load
                Reveal.this.memoryCheckTimer = new Timer();
                Reveal.this.memoryCheckTimerTask = new TimerTask() {
                    @Override
                    public void run() {
                        int interval = intervalMS;
                        Reveal.this.checkPermissionState( Reveal.this.applicationContext );

                        // increase it a little each time
                        if ( interval < MAX_PERMISSION_POLL_CHECK ) {
                            if ( interval > MAX_PERMISSION_POLL_CHECK)
                                interval = MAX_PERMISSION_POLL_CHECK;

                            Reveal.this.scheduleTimer( (int)(interval * 1.1 ) );
                        }
                        
                        int memoryWarning = Reveal.getInstance().getSimulateMemoryWarning();
                        if ( memoryWarning == 0 ) {
                            // we are not simulating a memory issue so check for a real one
                            ActivityManager.RunningAppProcessInfo state = new ActivityManager.RunningAppProcessInfo();
                            ActivityManager.getMyMemoryState( state );
                            memoryWarning = state.lastTrimLevel;
                        }
                        else {
                            Reveal.log( "Simulated memory warning " + memoryWarning, "WARNING", "STATE");
                        }


                        if ( memoryWarning > 60 ) {
                            Reveal.getInstance().setStatus( Reveal.STATUS_MEMORY, STATUS_IN_PROGRESS, "Memory recovery in progress level: " + memoryWarning);
                        }
                        else {
                            Reveal.getInstance().setStatus( Reveal.STATUS_MEMORY, STATUS_SUCCEED, "No memory issues" );
                        }

                        // we have memory issues in progress so log it
                        if ( memoryWarning > 60 ) {
                            // clear the simulated warning
                            Reveal.getInstance().setSimulateMemoryWarning( 0 );
                            Reveal.log( "Memory warning " + memoryWarning + " sending any unsaved packets to the server", "WARNING", "STATE");
                        }

                        // on every memory cycle we check for any beacons that are old enough to 
                        // need to be sent, because we are now sending only the timer we just want 
                        // to make sure since we may be terminated soon
                        Reveal.getInstance().dwellManager.processInProgress( Reveal.getInstance().getIncompleteBeaconSendTime() );
                    }
                };

                // setup the new timer
                Reveal.this.memoryCheckTimer.scheduleAtFixedRate( Reveal.this.memoryCheckTimerTask, intervalMS, intervalMS);
            }
        });

    }

    public void restart( Context context ) {
        try {
            needRegisteration = true;
            RevealLogger.d( "STARTUP - Reveal.restart() called" );

            if ( !isStarted() ) {
                RevealLogger.w( "restart called before start has successfully completed");

                if ( applicationContext != null ) {
                    start( null );
                }
            }
            else {

                boolean hasBluetooth = Reveal.selfPermissionGranted(applicationContext, Manifest.permission.BLUETOOTH);

                defaultDistanceCalculator =  new ModelSpecificDistanceCalculator(this.applicationContext, BeaconManager.getDistanceModelUpdateUrl());
                setDistanceCalculator(defaultDistanceCalculator);

                if ( locationService != null ) {
                    // restore this line to only start if we don't think we haven't started
                    if (!locationService.isLocationDiscoveryActive())
                        locationService.startLocationMonitoring(context);
                }

                if (hasBluetooth) {
                    RevealLogger.d("Reveal.restart() has bluetooth permission");

                    sendRegistrationIfPermitted(applicationContext );

                    Reveal.getInstance().recordEvent("restart");
                } else {
                    throw new RuntimeException("You need to add BLUETOOTH permission to use this SDK");
                }
            }
        }
        catch( SecurityException e ) {
            // http://stackoverflow.com/questions/33245977/java-lang-securityexception-bluetooth-permission-crash-samsung-devices-only
            boolean isKNOXError = false;
            String message = e.getMessage();

            if ( message != null )
                if ( message.toLowerCase(Locale.getDefault()).contains( "need bluetooth permission"))
                    isKNOXError = true;

            if ( isKNOXError ) {
                Reveal.RevealLogger.d("Bluetooth access denied (KNOX)");
            }
            else {
                stop();
                // rethrow the error if it isn't the one we expected
                throw e;
            }
        }
    }

    public void sendRegistrationIfPermitted( Context context) {
        if ( needRegisteration ) {
            String adId = Reveal.AdUtils.getAdvertisingId(context);
            if (!TextUtils.isEmpty(Reveal.AdUtils.getAdvertisingId(context))) {

                if (selfPermissionGranted(context, Manifest.permission.ACCESS_FINE_LOCATION) || selfPermissionGranted(context, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    this.registerDevice(context);
                }
            }
            else {
                Reveal.getInstance().setStatus( STATUS_LOCATION, STATUS_IN_PROGRESS, "No advertising ID provided" );
            }
        }
    }

    public void stop() {
        if ( applicationContext != null ) {
            // stop location scanning
            if (locationService != null) {
                locationService.stopLocationMonitoring(applicationContext);
            }

            // stop beacon beacon scanning
            if (beaconService != null) {
                beaconService.stopBeaconScanning(applicationContext);
            }

            //Commented out while we aren't doing wifi scanning
//            // stop wifi scanning
//            applicationContext.unregisterReceiver( mWifiScanReceiver );
        }
    }

    static public String readVersionInfoInManifest( Object obj){
        String result = null;

        Package objPackage = obj.getClass().getPackage();

        if ( objPackage != null ) {
            //examine the package object
            String name = objPackage.getName();  //.getSpecificationTitle();
            String version = objPackage.getImplementationVersion(); //.getSpecificationVersion();
            //some jars may use 'Implementation Version' entries in the manifest instead

            result = version + "/" + name;
        }

        return result;
    }

    private void registerDevice(final Context context ) {
        needRegisteration = false;

        // Fail if no context or apiKey has been set.
        if (context == null)
            throw new RuntimeException("Application Context passed into Reveal must not be null");
        if (this.apiKey == null)
            throw new RuntimeException("The Reveal SDK Requires an API Key to start");

        // Setup Advertising ID and cache locally for future use (we call it now and save it for
        // the future but we always replace in case it has changed
        AdUtils.setupAdvertisingId(context);

        //Setup and start location service, using our default service if none has been provided
        if ( locationService == null ) {
            Reveal.RevealLogger.d("No LocationService injected, using default Reveal implementation");
            locationService = new RevealLocationService();
        }
        else {
            Reveal.RevealLogger.d("Using existing LocationService: " + locationService );
        }

        locationService.startLocationMonitoring( context );

        //If we want to start scanning for wifi network changes, uncomment this.
        //startWiFiScan(context);

        try {
            if (beaconService == null) {
                Reveal.RevealLogger.d("No BeaconService injected, using default Reveal implementation (registerDevice)");
                setBeaconService(new RevealBeaconService(context));
            } else
                Reveal.RevealLogger.d("Using existing beaconService: " + beaconService);


            // if we have a service attempt to refresh it
            if ( Reveal.this.beaconService != null )
                Reveal.this.beaconService.setupIfNeeded();
        }
        catch ( RuntimeException e ) {
            Reveal.log( "registerDevice exception when setting beacon service:" + e, "ERROR", "STATE" );
        }

        // Delay start of registration call until we get a valid location or timeout
        locationService.waitForValidLocation(new LocationService.OnValidLocationCallback() {
            @Override
            public void onLocationFound() {

                // Send registration to server
                RevealNetworkClient.registerDevice(context, new RevealNetworkClientCallback() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        Reveal.log("Registration successful, starting up Reveal", "STATE");
                        Reveal.RevealLogger.d("waitForValidLocation OnSuccess beaconService: " + beaconService );
                        //Handle response to registration
                        if ( response != null ) {
                            JSONArray personasJSONArray = response.optJSONArray("personas");
                            if (personasJSONArray != null) {
                                setPersonasWithJSON(personasJSONArray);
                            }
                        }

                        // Check to see if bluetooth is available before doing any beacon scanning checks
                        if (Reveal.Utils.isBluetoothLeSupported(context)) {
                            // After Registration success, start bluetooth scanning
                            Boolean discoveryEnabled = null;

                            Reveal.RevealLogger.d( "registerService waitingForLocation beaconService=" + Reveal.this.beaconService );

                            // we should always get a response, but apparently sometimes we don't
                            if ( response != null ) {
                                discoveryEnabled = response.optBoolean("discovery_enabled");

                                Integer incompleteBeaconSendTime = response.optInt("incompleteBeaconSendTime");
                                if (incompleteBeaconSendTime > 0)
                                    Reveal.getInstance().setIncompleteBeaconSendTime(incompleteBeaconSendTime);
                            }

                            if (discoveryEnabled != null && discoveryEnabled) {
                                if (isBeaconScanningEnabled) {
                                    Reveal.RevealLogger.d("Beacon scanning is enabled and available, starting up beacon scanning");
                                    BeaconScanningProperties scanningProperties = new BeaconScanningProperties();

                                    // Handle secure cast beacons from server
                                    JSONArray securecastManufacturerCodes = response.optJSONArray("securecast_manufacturer_codes");
                                    if (securecastManufacturerCodes != null) {
                                        Reveal.RevealLogger.d("Received "+securecastManufacturerCodes.length()+" codes from server to scan for");
                                        // we got some so add them
                                        for (int i = 0; i < securecastManufacturerCodes.length(); i++) {
                                            try {
                                                String hexString = securecastManufacturerCodes.optString(i);
                                                Reveal.RevealLogger.d("Scanning for securecast beacons with manufacturer code "+hexString);
                                                int code = Integer.valueOf(hexString, 16);
                                                scanningProperties.additionalSecureCastManufacturerCodes.add(code);
                                            } catch (NumberFormatException e) {
                                                Reveal.RevealLogger.e(e);
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                    scanningProperties.setScanInterval(response.optInt("scan_interval"));

                                    scanningProperties.setScanDuration(response.optInt("scan_length"));

                                    scanningProperties.setCacheTTL(response.optInt("cache_ttl") * 60.0);

                                    scanningProperties.setEddystoneTimeout(response.optInt("eddystone_completion_timeout"));

                                    if (scanningProperties.getEddystoneTimeout() < (scanningProperties.getScanInterval()))
                                        scanningProperties.setEddystoneTimeout(scanningProperties.getScanInterval());

                                    if (response.has( "location_fix_timeout" )) {
                                        scanningProperties.setLocationTimeOut(response.optDouble("location_fix_timeout") * 60.0);

                                        if ( Reveal.this.getLocationService() != null)
                                            Reveal.this.getLocationService().setLocationValidTime(response.optDouble("location_fix_timeout"));
                                    }

                                    Integer batchSize = response.optInt( "batch_size" );
                                    Reveal.getInstance().eventCache.setMaxCachedEvents(batchSize);
                                    Reveal.getInstance().eventCache.setMaxCachedEventsOverrun( batchSize * 5 );

                                    Reveal.getInstance().eventCache.setIdleTimeout(response.optInt("batch_timeout"));

                                    Reveal.getInstance().setBatchBackgroundSend(response.optBoolean("batch_background_send"));

                                    Double dwellTimeout = 140.0;

                                    if ( response.has( "beacon_exit_time"))
                                        dwellTimeout = response.optDouble( "beacon_exit_time" );

//                                    if ( dwellTimeout > 300.0 ) {
//                                        dwellTimeout = 300.0;
//                                        Reveal.log( "Forced dwell time max to 5 minutes for testing", "STATE");
//                                    }

                                    Reveal.this.dwellManager.addEventType(BEACON, dwellTimeout );
                                    Reveal.this.dwellManager.addEventType(RevealEvent.EventType.ENTER, dwellTimeout );
                                    Reveal.this.dwellManager.addEventType(RevealEvent.EventType.EXIT, dwellTimeout );
                                    Reveal.this.dwellManager.addEventType(RevealEvent.EventType.LOCATION, 0.0 );

                                    if (debugUUIDs != null && !debugUUIDs.isEmpty()){
                                        scanningProperties.setDebugUUIDs(debugUUIDs);
                                    }

                                    scanningProperties.setBackgroundEnabled(isBackgroundScanningEnabled);

                                    startBeaconScanning(context, scanningProperties);
                                    Reveal.RevealLogger.d("waitForValidLocation after scanning properties beaconService: " + beaconService );
                                } else
                                    Reveal.log("Beacon scanning has been disabled manually", "WARNING", "STATE");
                            }
                            //If we are disabling scanning remotely, then stop any current scanning
                            else {
                                Reveal.log("Beacon scanning has been disabled from the server", "WARNING", "STATE");
                                stopBeaconScanning(context);
                            }
                        } else {
                            Reveal.log("Bluetooth LE is not supported on this device, so no beacon scanning will be done", "WARNING", "STATE");
                        }
                    }

                    @Override
                    public void onFailure(String response) {
                        Reveal.log("Error from server on device registration call: " + response, "ERROR", "STATE");
                    }
                });

            }
        });
    }

    @TargetApi(23)
    private void startBeaconScanning(Context context, BeaconScanningProperties scanningProperties ) {
        Reveal.RevealLogger.d("startBeaconScanning beaconService: " + beaconService );
        try {
            if (beaconService == null) {
                Reveal.RevealLogger.d("No BeaconService injected, using default Reveal implementation (startBeaconScanning)");
                setBeaconService(new RevealBeaconService(context));

                LocationService ls = this.getLocationService();
                if (ls != null && scanningProperties != null)
                    ls.setLocationValidTime(scanningProperties.getLocationTimeOut());
            }

            beaconService.setupIfNeeded();

            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                context.registerReceiver(dozeReciever, new IntentFilter(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED));
            }

            Reveal.log("Start beacon scan", "STATE");
            beaconService.startBeaconScanning(context, scanningProperties);
        }
        catch ( RuntimeException e ) {
            Reveal.log( "registerDevice exception:" + e, "ERROR", "STATE" );
        }
    }

//    private void startWiFiScan(Context context) {
//        RevealLogger.d( "startWiFiScan beaconService=" + Reveal.this.beaconService );
//
//        // Also scan for WiFi
//        if (context != null
//                && ( wifiManager != null )
//                && Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_WIFI_STATE)
//                && Reveal.selfPermissionGranted(context, Manifest.permission.CHANGE_WIFI_STATE)
//                && Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_COARSE_LOCATION)
//                    || Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_FINE_LOCATION) ) {
//            wifiManager.startScan();
//            Reveal.RevealLogger.d("startWiFiScan end beaconService: " + beaconService );
//        }
//    }

    private void stopBeaconScanning(Context context) {
        Reveal.RevealLogger.d( "stopBeaconScanning beaconService=" + Reveal.this.beaconService );

        if (this.beaconService != null) {
            this.beaconService.stopBeaconScanning( context );
        }
    }

    /**
     * Send the beacon to the server. This actually will add the beacon to the event queue to be
     * sent at an appropriate time.
     *
     * @param context the Android context
     * @param beacon the beacon to send
     */
    public void sendDiscoveryOfBeacon(Context context, RevealBeacon beacon) {
        Reveal.RevealLogger.d( "sendDiscoveryOfBeacon beaconService=" + Reveal.this.beaconService );
        Reveal.RevealLogger.v( "Encountered beacon " + beacon.getBeaconType() + " : " + beacon.getIdentifier() );


        Reveal.getInstance().recordEvent( "All beacons");
        Reveal.getInstance().recordEvent( beacon.beaconType + " beacon");

        dwellManager.addEvent( beacon );
        Reveal.RevealLogger.d( "sendDiscoveryOfBeacon end beaconService=" + Reveal.this.beaconService );
    }

    public boolean isStarted() {
        boolean result = false;

        if ( ( this.applicationContext != null ) && ( this.startTime != null ) )
            result = true;

        return result;
    }

    public void addDwellEvent( RevealEvent eventObject ) {
        dwellManager.addEvent( eventObject );

        if ( eventObject.getEventType() == ENTER ) {
            RevealLocation locationEvent = (RevealLocation) eventObject;

            OnLocationFoundListener listener = Reveal.this.locationFoundListener;
            if ( listener != null ) {
                listener.onLocationDiscovered( locationEvent.location );
            }
        }
    }

    /**
     * Add an event to the queue, after first adding the SSID information if available
     *
     * @param context The application context
     * @param eventObject The event you wish to add
     */
    public void addEvent( Context context, RevealEvent eventObject ) {
        Reveal.RevealLogger.d( "addEvent beaconService=" + Reveal.this.beaconService );

        if ( context != null ) {
            if ( Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_WIFI_STATE) ) {
                if (Utils.isWiFi(context)) {
                    WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();

                    if (wifiInfo != null) {
                        if (eventObject.getCurrentSSID() == null) {
                            String ssid = wifiInfo.getSSID();

                            if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
                                ssid = ssid.substring(1, ssid.length() - 1);
                            }

                            eventObject.setCurrentSSID(ssid);
                        }

                        if (eventObject.getCurrentBSSID() == null)
                            eventObject.setCurrentBSSID(wifiInfo.getBSSID());
                    }
                }
            }
        }

        eventCache.addEvent( eventObject );
    }

    /**
     * Record a successful event for the debug Status Screen. Maintains a count of the successes as well
     * as the failures.
     *
     * @param eventName thing being tracked (any string that represents a meaningful statistic)
     */
    public void recordEvent( String eventName ) {
        recordEvent( eventName, true );
    }

    /**
     * Record an event for the debug Status Screen. Maintains a count of the successes as well
     * as the failures.
     *
     * @param eventName thing being tracked (any string that represents a meaningful statistic)
     * @param success whether the event was a success or failure
     */
    public void recordEvent( String eventName, boolean success ) {
        recordEvent( eventName, success, 1 );
    }

    /**
     * Record an event for the debug Status Screen. Maintains a count of the successes as well
     * as the failures.
     *
     * @param eventName thing being tracked (any string that represents a meaningful statistic)
     * @param success whether the event was a success or failure
     * @param count the number to add to the prospective count
     */
    public void recordEvent( String eventName, boolean success, Integer count ) {
        if ( Reveal.getInstance().getIsDebug() ) {
            ConcurrentHashMap<String, Integer> choice = null;
            Integer total = 0;
            String name = eventName;

            if (this.isInBackGround())
                name = name + " (background)";

            if (success)
                choice = successStats;
            else
                choice = failureStats;

            if (choice.containsKey(name)) {
                total = choice.get(name);
                choice.remove(name);
            }

            if (total != null) {
                total = total + count;

                choice.put(name, total);
            }
        }
    }

    /**
     * Send a message to the UI for testing. This method should be treated as deprecated and is
     * provided for internal testing only. The message will be passed via the UserLogListener to
     * the UI, as well as logged via Reveal.RevealLogger
     *
     * @param message The message to display
     */
    public static void log( Object message ) {
        Reveal.log( message, "DEBUG" );
    }

    /**
     * Send a message to the UI for testing. This method should be treated as deprecated and is
     * provided for internal testing only. The message will be passed via the UserLogListener to
     * the UI, as well as logged via Reveal.RevealLogger
     *
     * @param message The message to display
     * @param group An arbitrary group name for filtering
     */
    public static void log( Object message, String group ) {
        Reveal.log( message, "DEBUG", group );
    }

    /**
     * Send a message to the UI for testing. This method should be treated as deprecated and is
     * provided for internal testing only. The message will be passed via the UserLogListener to
     * the UI, as well as logged via Reveal.RevealLogger
     *
     * @param message The message to display
     * @param type The type off message (debug, verbose, error, warning)
     * @param group An arbitrary group name for filtering
     */
    public static void log( Object message, String type, String group ) {
        String text = message.toString();

        if ( Reveal.getInstance().logger != null ) {

            Reveal.getInstance().logger.logMessage( text, type, group );
        }

        text = "[" + group + "] " + text;

        if ( type.compareToIgnoreCase( "verbose" ) == 0 )
            Reveal.RevealLogger.v( text );
        else if ( type.compareToIgnoreCase( "error" ) == 0 )
            Reveal.RevealLogger.e( text );
        else if ( type.compareToIgnoreCase( "warning" ) == 0 )
            Reveal.RevealLogger.w( text );
        else
            Reveal.RevealLogger.d( text );
    }

    public static void logMemoryUsage( String label ) {
        final Runtime runtime = Runtime.getRuntime();
        final long usedMemInMB=(runtime.totalMemory() - runtime.freeMemory()) / 1048576L;
        final long maxHeapSizeInMB=runtime.maxMemory() / 1048576L;
        final long availHeapSizeInMB = maxHeapSizeInMB - usedMemInMB;

        Reveal.log( label + " System Memory - used: " + usedMemInMB + "MB, heap size: " + maxHeapSizeInMB + "MB Avail: " + availHeapSizeInMB + "MB", "STATE" );
    }

    public static final long CHECK_DELAY = 2000;
    private Handler handler = new Handler();
    private Runnable check;
    private Activity current;
    private boolean foreground;

    @Override
    public void onActivityResumed(Activity activity) {
        RevealLogger.d("Resuming Activity");
    }

    @Override
    public void onActivityPaused(Activity activity) {
        // if we're changing configurations we aren't going background so
        // no need to schedule the check
        if (!activity.isChangingConfigurations()) {
            // don't prevent activity being gc'd
            final WeakReference<Activity> ref = new WeakReference<Activity>(activity);
            handler.postDelayed(check = new Runnable() {
                @Override
                public void run() {
                    onActivityCeased(ref.get());
                }
            }, CHECK_DELAY);
        }
    }

    @Override
    public void onActivityStarted(Activity activity) {
        current = activity;
        // remove any scheduled checks since we're starting another activity
        // we're definitely not going background
        if (check != null) {
            handler.removeCallbacks(check);
        }

        // check if we're becoming foreground and notify listeners
        if ( !foreground ) {
            if (activity != null && !activity.isChangingConfigurations()) {
                foreground = true;
                Reveal.log( "Application is now in the foreground", "STATE");
            }

            if ( foreground && Reveal.getInstance().isStarted() )
                Reveal.getInstance().restart( applicationContext );
        }
    }

    @Override
    public void onActivityStopped(Activity activity) {
        if (check != null) {
            handler.removeCallbacks(check);
        }
        onActivityCeased(activity);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {}

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}

    @Override
    public void onActivityDestroyed(Activity activity) {

    }


    private void onActivityCeased(Activity activity){
        if (foreground) {
            if ((activity == current) && (activity != null && !activity.isChangingConfigurations())){
                foreground = false;

                Reveal.log( "Application is now in the background", "STATE");
            }
        }
    }

    private void setPersonasWithJSON(JSONArray personasJSONArray) {
        Reveal.RevealLogger.d("New Personas: "+personasJSONArray.toString());
        List<String> personas = new ArrayList<String>();
        for (int i=0; i < personasJSONArray.length(); i++) {
            personas.add(personasJSONArray.optString(i));
        }
        this.personas = personas;
    }

    public void setAPIKey(String apiKey){
        this.apiKey = apiKey;
    }

    public void setServiceType(ServiceType serviceType) {
        if (serviceType == ServiceType.SANDBOX) {
            this.apiBaseURL = REVEAL_API_BASE_SANDBOX;
        } else this.apiBaseURL = REVEAL_API_BASE_PRODUCTION;
    }

    public void setAPIEndpointBase(String apiEndpointBase) {
        RevealLogger.w("Setting endpoint base to "+apiEndpointBase+" - only for specific installations");
        this.apiBaseURL = apiEndpointBase;
    }

    // We need this for the test app, but really would rather not have it public
    public String getAPIBaseURL() {
        return apiBaseURL;
    }

    public void setDebug(Boolean isDebug) {
        this.isDebug = isDebug;
    }

    public void setDebugUUIDs(List<String> debugUUIDs) {
        this.debugUUIDs = debugUUIDs;
    }

    public List<String> getPersonas(){
        return personas;
    }

    public String getAPIKey() {
        return apiKey;
    }

    public Boolean getIsDebug() {
        return isDebug;
    }

    public BeaconService getBeaconService() {
        return beaconService;
    }

    public void setBeaconService(BeaconService beaconService) {
        this.beaconService = beaconService;
    }

    public LocationService getLocationService() {
        return locationService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }


    public Boolean getLocationSharingEnabled() {
        return locationSharingEnabled;
    }

    public void setLocationSharingEnabled(Boolean locationSharingEnabled) {
        this.locationSharingEnabled = locationSharingEnabled;
    }

    public Boolean getIsBackgroundScanningEnabled() {
        return isBackgroundScanningEnabled;
    }

    public void setIsBackgroundScanningEnabled(Boolean backgroundScanningEnabled) {
        isBackgroundScanningEnabled = backgroundScanningEnabled;
    }

    // Used for debugging purposes only
    public List<String> getBeaconDescriptions() {
        ArrayList<String> result = new ArrayList<String>();

        if ( beaconService != null ) {
            List<RevealBeacon> beacons = beaconService.getBeacons();
            if (beacons != null) {
                for (RevealBeacon beacon : beacons ) {

                    result.add(beacon.toString());
                }
            }
        }

        Reveal.RevealLogger.v("Found " + result.size() + " beacons");

        return result;
    }


    public boolean isBatchBackgroundSend() {
        return batchBackgroundSend;
    }

    public void setBatchBackgroundSend(boolean batchBackgroundSend) {
        this.batchBackgroundSend = batchBackgroundSend;
    }

    public boolean isInBackGround() {
        return !foreground;
    }

    public UserLogListener getLogger() {
        return logger;
    }

    public void setLogger(UserLogListener logger) {
        this.logger = logger;
    }

    public ConcurrentHashMap<String, Integer> getSuccessStats() {
        return successStats;
    }

    public void setSuccessStats(ConcurrentHashMap<String, Integer> successStats) {
        this.successStats = successStats;
    }

    public ConcurrentHashMap<String, Integer> getFailureStats() {
        return failureStats;
    }

    public void setFailureStats(ConcurrentHashMap<String, Integer> failureStats) {
        this.failureStats = failureStats;
    }


    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public RevealDwellManager getDwellManager() {
        return dwellManager;
    }

    public DistanceCalculator getDistanceCalculator() {
        return distanceCalculator;
    }

    public void setDistanceCalculator(DistanceCalculator distanceCalculator) {
        this.distanceCalculator = distanceCalculator;
    }

    public OnLocationFoundListener getLocationFoundListener() {
        return locationFoundListener;
    }

    public void setLocationFoundListener(OnLocationFoundListener locationFoundListener) {
        this.locationFoundListener = locationFoundListener;
    }

    public long getIncompleteBeaconSendTime() {
        return incompleteBeaconSendTime;
    }

    public void setIncompleteBeaconSendTime(long incompleteBeaconSendTime) {
        this.incompleteBeaconSendTime = incompleteBeaconSendTime;
    }

    public int getSimulateMemoryWarning() {
        return simulateMemoryWarning;
    }

    public void setSimulateMemoryWarning(int simulateMemoryWarning) {
        this.simulateMemoryWarning = simulateMemoryWarning;
    }

    public Context getApplicationContext() {
        return applicationContext;
    }
}
