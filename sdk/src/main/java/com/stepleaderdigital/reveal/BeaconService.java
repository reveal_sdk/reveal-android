package com.stepleaderdigital.reveal;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;

import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;

import static android.app.PendingIntent.FLAG_ONE_SHOT;
import static android.app.PendingIntent.getBroadcast;

/**
 * Created by bobby on 5/9/17.
 */

public class BeaconService extends Service  {


    /**
     * Command to the service to display a message
     */
    public static final int MSG_START_MONITORING = 2;
    public static final int MSG_STOP_MONITORING = 3;
    public static final int MSG_SET_SCAN_PERIODS = 6;

    private CycledLeScanner cycledScanner;
    private BeaconManager beaconManager;
    private boolean backgroundFlag = false;
    private boolean disableAndroidL = false;
    private ExecutorService executor;
    private final Handler handler = new Handler();

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class BeaconBinder extends Binder {
        public BeaconService getService() {
            Reveal.RevealLogger.d( "getService of BeaconBinder called");
            // Return this instance of LocalService so clients can call public methods
            return BeaconService.this;
        }
    }



    private class ScanData {
        public ScanData(BluetoothDevice device, int rssi, byte[] scanRecord) {
            this.device = device;
            this.rssi = rssi;
            this.scanRecord = scanRecord;
        }

        int rssi;
        BluetoothDevice device;
        byte[] scanRecord;
    }


    protected final CycledLeScanCallback cycledLeScanCallback = new CycledLeScanCallback() {
        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            //Reveal.RevealLogger.d( "Got scan data for " + device.getAddress() + " : " + scanRecord );

            BeaconManager.BeaconLeScanCallback nonBeaconLeScanCallback = beaconManager.getBeaconCallback();

            try {
                ScanProcessor processor = new ScanProcessor(nonBeaconLeScanCallback);
                processor.executeOnExecutor(executor, new ScanData(device, rssi, scanRecord));
            } catch (RejectedExecutionException e) {
                Reveal.RevealLogger.w( "Ignoring scan result because we cannot keep up.");
            }
        }

        @Override
        public void onCycleEnd() {
            Reveal.RevealLogger.d( "Scan ended" );

        }
    };

    static class IncomingHandler extends Handler {
        private final WeakReference<BeaconService> mService;

        IncomingHandler(BeaconService service) {
            mService = new WeakReference<BeaconService>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            BeaconService service = mService.get();
            StartRMData startRMData = (StartRMData) msg.obj;

            if (service != null) {
                Reveal.RevealLogger.v( "Handle MSG_... " + msg.what + " from bindee" );

                switch (msg.what) {
                    case MSG_START_MONITORING:
                        Reveal.RevealLogger.i(  "MSG_START_MONITORING called");
                        service.start();
                        service.setScanPeriods(startRMData.getScanPeriod(), startRMData.getBetweenScanPeriod(), startRMData.getBackgroundFlag());
                        break;

                    case MSG_SET_SCAN_PERIODS:
                        Reveal.RevealLogger.i(  "MSG_SET_SCAN_PERIODS called");
                        service.setScanPeriods(startRMData.getScanPeriod(), startRMData.getBetweenScanPeriod(), startRMData.getBackgroundFlag());
                        break;

                    case MSG_STOP_MONITORING:
                        Reveal.RevealLogger.i(  "MSG_STOP_MONITORING called");
                        service.stop();
                        break;

                    default:
                        super.handleMessage(msg);
                }
            }
        }
    }


    public void setScanPeriods(long scanPeriod, long betweenScanPeriod, boolean backgroundFlag) {
        cycledScanner.setScanPeriods(scanPeriod, betweenScanPeriod, backgroundFlag);
    }

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger messenger = new Messenger(new IncomingHandler(this));
    BluetoothCrashResolver bluetoothCrashResolver;

    public void start() {
        Reveal.RevealLogger.i( "beaconService beacon version: " + BuildConfig.VERSION_NAME);
        bluetoothCrashResolver = new BluetoothCrashResolver(this);
        bluetoothCrashResolver.start();

        cycledScanner = CycledLeScanner.createScanner( this.getApplicationContext(),
                BeaconManager.DEFAULT_FOREGROUND_SCAN_PERIOD,
                BeaconManager.DEFAULT_FOREGROUND_BETWEEN_SCAN_PERIOD,
                backgroundFlag,
                cycledLeScanCallback,
                bluetoothCrashResolver,
                disableAndroidL );

        if ( cycledScanner != null )
            cycledScanner.start();
    }

    public void stop() {

        if ( cycledScanner != null )
            cycledScanner.stop();
    }

    @Override
    public void onCreate() {
        Reveal.RevealLogger.i( "beaconService version %s is starting up", BuildConfig.VERSION_NAME);


        // Create a private executor so we don't compete with threads used by AsyncTask
        // This uses fewer threads than the default executor so it won't hog CPU
        executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);

        bluetoothCrashResolver = new BluetoothCrashResolver(this);
        bluetoothCrashResolver.start();

        beaconManager = BeaconManager.getInstanceForApplication(getApplicationContext());
    }

    /**
     * When binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        Reveal.RevealLogger.i( "Binding service");
        return messenger.getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Reveal.RevealLogger.i( "Unbinding beacon service");
        stop();
        return false;
    }

    @Override
    public void onDestroy() {
        Reveal.RevealLogger.w( "destroying beacon service");

        if (android.os.Build.VERSION.SDK_INT < 18) {
            Reveal.RevealLogger.e( "Destruction prior API 18 is not permitted.");

            return;
        }

        if ( bluetoothCrashResolver != null )
            bluetoothCrashResolver.stop();

        if ( cycledScanner != null ) {
            cycledScanner.stop();
            cycledScanner.destroy();
        }

        if ( handler != null )
            handler.removeCallbacksAndMessages(null);

        Reveal.RevealLogger.i(  "onDestroy called. Stopping scans");
    }



    private class ScanProcessor extends AsyncTask<ScanData, Void, Void> {
        private BeaconManager.BeaconLeScanCallback beaconLeScanCallback;

        public ScanProcessor( BeaconManager.BeaconLeScanCallback beaconLeScanCallback) {
            this.beaconLeScanCallback = beaconLeScanCallback;
        }

        @Override
        protected Void doInBackground(ScanData... params) {
            ScanData scanData = params[0];

                if (beaconLeScanCallback != null) {
                    beaconLeScanCallback.onBeaconLeScan(scanData.device, scanData.rssi, scanData.scanRecord);
                }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

}
