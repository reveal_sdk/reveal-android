package com.stepleaderdigital.reveal;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Created by bobby on 4/26/17.
 */

public class BeaconScanner  {
    protected boolean useLowPowerOnNewerAndroidVersion;

    protected BluetoothAdapter adapter;
    protected BluetoothCrashResolver bluetoothCrashResolver;
    protected BroadcastReceiver reciever;
    protected Context applicationContext;
    protected BeaconScannerCallback callback;

    public interface BeaconScannerCallback {
        void onBeaconFound(BluetoothDevice device, int rssi, byte[] scanRecord);
    }

    public void setup( Context appContext ) {
        applicationContext = appContext;
        Reveal.RevealLogger.d("beaconService version " + BuildConfig.VERSION_NAME + " is starting up" );

        if ( adapter != null ) {
            Reveal.RevealLogger.d( "we have a good adapter" );

            if (! adapter.isEnabled()) {
//                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//                applicationContext.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }

    public void start() {
        adapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothCrashResolver = new BluetoothCrashResolver( applicationContext );

        reciever = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                Reveal.RevealLogger.d( "BroadcastReceiver.onReceive() action=" + action);

                //Finding devices
                if (BluetoothDevice.ACTION_FOUND.equals(action))
                {
                    // Get the BluetoothDevice object from the Intent
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    // Add the name and address to an array adapter to show in a ListView
                    Reveal.RevealLogger.d(device.getName() + "\n" + device.getAddress());
                    int  rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE);
                    byte[] bytes = new byte[10];

                    if ( callback != null ) {
                        callback.onBeaconFound( device, rssi, bytes );
                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_FOUND );
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);

        applicationContext.registerReceiver( reciever, filter);
        bluetoothCrashResolver.start();
        adapter.startDiscovery();
    }

    public BluetoothAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(BluetoothAdapter adapter) {
        this.adapter = adapter;
    }

    public boolean isUseLowPowerOnNewerAndroidVersion() {
        return useLowPowerOnNewerAndroidVersion;
    }

    public void setUseLowPowerOnNewerAndroidVersion(boolean useLowPowerOnNewerAndroidVersion) {
        this.useLowPowerOnNewerAndroidVersion = useLowPowerOnNewerAndroidVersion;
    }

    public BeaconScannerCallback getCallback() {
        return callback;
    }

    public void setCallback(BeaconScannerCallback callback) {
        this.callback = callback;
    }
}
