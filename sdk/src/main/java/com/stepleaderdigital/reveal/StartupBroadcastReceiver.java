package com.stepleaderdigital.reveal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by bobby on 5/9/17.
 */

public class StartupBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v( "Reveal", "*\n*\n*\n*\n*\n*\n*\n* StartupBroadcastReceiver onReceive called in startup broadcast receiver\n*\n*\n*\n*\n*\n*\n*");
        if (android.os.Build.VERSION.SDK_INT < 18) {
            Reveal.RevealLogger.w( "Not starting up beacon service because we do not have API version 18 (Android 4.3).  We have: " + android.os.Build.VERSION.SDK_INT);
            return;
        }

        String action = intent.getAction();
        if ( action != null ) {
            // TODO: Bobby - Check to see if anyone is listening and only wake up if someone is listening
//        BeaconManager beaconManager = BeaconManager.getInstanceForApplication(context.getApplicationContext());
//        if (beaconManager.isAnyConsumerBound()) {
            if (intent.getBooleanExtra("wakeup", false)) {
                Reveal.RevealLogger.d("StartupBroadcastReceiver got wake up intent");
            } else {
                Reveal.RevealLogger.d("StartupBroadcastReceiver Already started.  Ignoring intent: " + intent + " of type: " + intent.getStringExtra("wakeup"));
            }
//        }
        }
    }
}
