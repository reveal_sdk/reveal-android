package com.stepleaderdigital.reveal;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by bobby on 5/11/17.
 */

public class DistanceConfigFetcher {
    protected String mResponse;
    protected Exception mException;
    private int mResponseCode = -1;
    private String mUrlString;
    private String mUserAgentString;

    public DistanceConfigFetcher(String urlString, String userAgentString) {
        this.mUrlString = urlString;
        this.mUserAgentString = userAgentString;
    }


    public int getResponseCode() {
        return mResponseCode;
    }

    public String getResponseString() {
        return mResponse;
    }

    public Exception getException() {
        return mException;
    }

    public void request() {
        mResponse = null;
        String currentUrlString = mUrlString;
        int requestCount = 0;
        StringBuilder responseBuilder = new StringBuilder();
        URL url;
        HttpURLConnection conn = null;
        do {
            if (requestCount != 0) {
                Reveal.RevealLogger.d("Following redirect from %s to %s",
                        mUrlString, conn.getHeaderField("Location"));
                currentUrlString = conn.getHeaderField("Location");
            }
            requestCount++;
            mResponseCode = -1;
            url = null;
            try {
                url = new URL(currentUrlString);
            } catch (Exception e) {
                Reveal.RevealLogger.e("Can't construct URL from: %s", mUrlString);
                mException = e;

            }
            if (url == null) {
                Reveal.RevealLogger.d("URL is null.  Cannot make request");
            } else {
                try {
                    conn = (HttpURLConnection) url.openConnection();
                    conn.addRequestProperty("User-Agent", mUserAgentString);
                    mResponseCode = conn.getResponseCode();
                    Reveal.RevealLogger.d( "response code is %s", conn.getResponseCode());
                } catch (SecurityException e1) {
                    Reveal.RevealLogger.w(e1, "Can't reach sever.  Have you added android.permission.INTERNET to your manifest?");
                    mException = e1;
                } catch (FileNotFoundException e2) {
                    Reveal.RevealLogger.w(e2, "No data exists at \"+urlString");
                    mException = e2;
                } catch (java.io.IOException e3) {
                    Reveal.RevealLogger.w(e3, "Can't reach server");
                    mException = e3;
                }
            }
        }
        while (requestCount < 10 &&
                (mResponseCode == HttpURLConnection.HTTP_MOVED_TEMP
                        || mResponseCode == HttpURLConnection.HTTP_MOVED_PERM
                        || mResponseCode == HttpURLConnection.HTTP_SEE_OTHER));

        if (mException == null) {
            try {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                conn.getInputStream())
                );
                String inputLine;
                while ((inputLine = in.readLine()) != null)
                    responseBuilder.append(inputLine);
                in.close();
                mResponse = responseBuilder.toString();
            } catch (Exception e) {
                mException = e;
                Reveal.RevealLogger.w(e, "error reading beacon data");
            }
        }

    }
}
