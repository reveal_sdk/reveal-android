package com.stepleaderdigital.reveal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PowerManager;

import java.util.Arrays;

/**
 * Created by bobby on 7/6/17.
 */

public class DozeModeReceiver extends BroadcastReceiver {
    // https://codelabs.developers.google.com/codelabs/android-doze-standby/index.html?index=..%2F..%2Findex#2
    // https://developer.android.com/training/monitoring-device-state/doze-standby.html#assessing_your_app

    @Override
    public void onReceive(Context context, Intent intent) {
        Reveal.getInstance().recordEvent("Idle state changes");
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            String action = intent.getAction();
            if (action != null) {
                String state;

                PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                if ( !pm.isDeviceIdleMode() ) {
                    // we are in a maintenance window or awaking from doze - so lets release all
                    // the batched items, the intent will provide more details, but we process all
                    // pending events either way so no need to differentiate
                    Reveal.getInstance().getDwellManager().processPendingEvents();
                    Reveal.LocationService locationService = Reveal.getInstance().getLocationService();

                    if ( locationService != null ) {
                        // restore this line to only start if we don't think we haven't started
                        //if (locationService.isLocationDiscoveryActive() == false)
                            locationService.startLocationMonitoring(context);
                    }

                    state = "Not idle";
                }
                else {
                    state = "Idle";
                }

                if ( pm.isPowerSaveMode() )
                    state = state + " (Power Save)";

                Reveal.log( "Idle state change: " + state + " intent=" + intentToString( intent ), "STATE" );
            }
        }
    }

    public static String intentToString(Intent intent) {
        if (intent == null) {return null;}
        String out = intent.toString();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            extras.size();
            out += "\n" + bundleToString(extras);
        }
        if (intent.getAction() != null)
            out+="\nAction = " + intent.getAction();
        if (intent.getType() != null)
            out+="\nType = " + intent.getType();
        if (intent.getData() != null)
            out+="\nData = " + intent.getData();
        if (intent.getPackage() != null)
            out+="\nPackage = " + intent.getPackage();
        if (intent.getDataString() != null)
            out+="\nDataString = " + intent.getDataString();
        return out;
    }

    public static String bundleToString(Bundle bundle) {
        StringBuilder out = new StringBuilder("Bundle[");

        if (bundle == null) {
            out.append("null");
        } else {
            boolean first = true;
            for (String key : bundle.keySet()) {
                if (!first) {
                    out.append(", ");
                }

                out.append(key).append('=');

                Object value = bundle.get(key);

                if (value instanceof int[]) {
                    out.append(Arrays.toString((int[]) value));
                } else if (value instanceof byte[]) {
                    out.append(Arrays.toString((byte[]) value));
                } else if (value instanceof boolean[]) {
                    out.append(Arrays.toString((boolean[]) value));
                } else if (value instanceof short[]) {
                    out.append(Arrays.toString((short[]) value));
                } else if (value instanceof long[]) {
                    out.append(Arrays.toString((long[]) value));
                } else if (value instanceof float[]) {
                    out.append(Arrays.toString((float[]) value));
                } else if (value instanceof double[]) {
                    out.append(Arrays.toString((double[]) value));
                } else if (value instanceof String[]) {
                    out.append(Arrays.toString((String[]) value));
                } else if (value instanceof CharSequence[]) {
                    out.append(Arrays.toString((CharSequence[]) value));
                } else if (value instanceof Parcelable[]) {
                    out.append(Arrays.toString((Parcelable[]) value));
                } else if (value instanceof Bundle) {
                    out.append(bundleToString((Bundle) value));
                } else {
                    out.append(value);
                }

                first = false;
            }
        }

        out.append("]");
        return out.toString();
    }
}
