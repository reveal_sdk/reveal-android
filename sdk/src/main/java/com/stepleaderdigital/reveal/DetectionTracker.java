package com.stepleaderdigital.reveal;

import android.os.SystemClock;

/**
 * Created by bobby on 5/10/17.
 */

public class DetectionTracker {
    private static final DetectionTracker INSTANCE = new DetectionTracker();

    private long mLastDetectionTime = 0l;
    private DetectionTracker() {

    }
    public static DetectionTracker getInstance() {
        return INSTANCE;
    }
    public long getLastDetectionTime() {
        return mLastDetectionTime;
    }
    public void recordDetection() {
        mLastDetectionTime = SystemClock.elapsedRealtime();
    }
}
