package com.stepleaderdigital.reveal;

import android.Manifest;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static com.stepleaderdigital.reveal.Reveal.Utils.isBluetoothEnabled;

/**
 * Created by seandoherty on 5/27/15.
 */
public class RevealBeaconService implements Reveal.BeaconService, BeaconConsumer { //, BootstrapNotifier, MonitorNotifier {
    public static final int     TYPE_EDDYSTONE       = 16;
    public static final int     TYPE_EDDYSTONE2      = 0;

    /**
     * Estimote beacon type code
     */
    public static final int     TYPE_ESTIMOTE        = 533;

    public static final String  TYPE_NAME_SECURECAST  = "Securecast";
    public static final String  TYPE_NAME_EDDYSTONE   = "Eddystone";
    public static final String  TYPE_NAME_ESTIMOTE    = "iBeacon";
    public static final String  TYPE_NAME_GIMBAL      = "Gimbal";
    public static final String  TYPE_NAME_GATT        = "GATT";

    public static final int      PROXIMITY_UNKNOWN   = 0;
    public static final int      PROXIMITY_IMMEDIATE = 1;
    public static final int      PROXIMITY_NEAR      = 2;
    public static final int      PROXIMITY_FAR       = 3;

    public enum CacheState {
        New, Closer, Existing
    }

    private BackgroundPowerSaver backgroundPowerSaver;

    public class RevealScanGroup {
        private String serviceId;
        private int startVendor;
        private int endVendor;
        private int vendorIdStart = 0;
        private int vendorIdEnd = 1;
        private String configurationString;
        private String name;

        public RevealScanGroup( String type, String name, int vendor, String configString ) {
            this.type = type;
            this.name = name;
            this.startVendor = vendor;
            this.endVendor = vendor;
            this.configurationString = configString;
        }

        public ArrayList<String> getBeaconStrings() {
            ArrayList<String> result = new ArrayList<String>();

            for( int vendor=this.startVendor ; vendor <= this.endVendor ; vendor++ ) {
                StringBuilder builder = new StringBuilder();
                String vendorString = Long.toHexString( vendor );
                int digits = ( this.vendorIdEnd - this.vendorIdStart + 1 ) * 2;

                while ( vendorString.length() < digits )
                    vendorString = "0" + vendorString;

                if ( this.serviceId != null )
                    builder.append( "s:0-1=" )
                            .append(this.serviceId)
                            .append(",");

                builder.append( "m:" )
                        .append( this.vendorIdStart )
                        .append( "-" )
                        .append( this.vendorIdEnd )
                        .append( "=" )
                        .append( vendorString )
                        .append(",")
                        .append(this.configurationString);

                result.add( builder.toString() );
            }

            return result;
        }


        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        private String type;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getServiceId() {
            return serviceId;
        }

        public void setServiceId(String serviceId) {
            this.serviceId = serviceId;
        }

        public int getStartVendor() {
            return startVendor;
        }

        public void setStartVendor(int startVendor) {
            this.startVendor = startVendor;
        }

        public int getEndVendor() {
            return endVendor;
        }

        public void setEndVendor(int endVendor) {
            this.endVendor = endVendor;
        }

        public int getVendorIdStart() {
            return vendorIdStart;
        }

        public void setVendorIdStart(int vendorIdStart) {
            this.vendorIdStart = vendorIdStart;
        }

        public int getVendorIdEnd() {
            return vendorIdEnd;
        }

        public void setVendorIdEnd(int vendorIdEnd) {
            this.vendorIdEnd = vendorIdEnd;
        }

        public String getConfigurationString() {
            return configurationString;
        }

        public void setConfigurationString(String configurationString) {
            this.configurationString = configurationString;
        }

    }

    public interface CCObjectCacheListener<K, T> {
        void onItemAdded(CCObjectCacheEntry<K, T> entry);
        void onItemRemoved(CCObjectCacheEntry<K, T> entry);
        void onItemAccessed(CCObjectCacheEntry<K, T> entry);
    }
    
    /**
     * Created by bobby on 1/29/16.
     *
     * This is a port of the objective c class by the same name
     */
    public class CCObjectCache<K, T> {
        private Boolean resetOnEveryAccess = false;
        private Boolean resetOnEveryAdd = false;
        private double cacheTime = 1.0 * 60.0 * 60.0;
        protected ConcurrentHashMap<K, CCObjectCacheEntry<K, T>> items = new ConcurrentHashMap<K, CCObjectCacheEntry<K, T>>();
        private CCObjectCacheListener<K, T> objectCacheListener = null;

        public double getCacheTime() {
            return cacheTime;
        }

        public void setCacheTime(double cacheTime) {
            this.cacheTime = cacheTime;
        }

        public Boolean getResetOnEveryAdd() {
            return resetOnEveryAdd;
        }

        public void setResetOnEveryAdd(Boolean resetOnEveryAdd) {
            this.resetOnEveryAdd = resetOnEveryAdd;
        }

        public Boolean getResetOnEveryAccess() {
            return resetOnEveryAccess;
        }

        public void setResetOnEveryAccess(Boolean resetOnEveryAccess) {
            this.resetOnEveryAccess = resetOnEveryAccess;
        }

        public T get( K key ) {
            purgeOld();

            T result = null;
            CCObjectCacheEntry<K, T> entry = items.get( key );
            Date now = new Date();

            if ( entry != null )  {
                if ( resetOnEveryAccess ) {
                    Calendar cal = GregorianCalendar.getInstance();
                    cal.setTime(now);
                    cal.add(Calendar.SECOND, (int) cacheTime);
                    entry.setDateOut(cal.getTime());
                }

                result = entry.getObject();

                if ( objectCacheListener != null )
                    objectCacheListener.onItemAccessed( entry );
            }

            return result;
        }

        public Boolean put( T obj, K key ) {
            purgeOld();

            Boolean result = false;
            CCObjectCacheEntry<K, T> entry = items.get( key );
            Date now = new Date();

            if ( entry != null )  {
                Reveal.RevealLogger.v( "Cache already contains " + key.toString() );
                if ( resetOnEveryAdd ) {
                    Calendar cal = GregorianCalendar.getInstance();
                    cal.setTime(now);
                    cal.add(Calendar.SECOND, (int) cacheTime);
                    entry.setDateOut(cal.getTime());
                }
            }
            else {
                entry = new CCObjectCacheEntry<K, T>();

                entry.setKey(key);
                entry.setObject(obj);
                entry.setDateIn(now);

                Calendar cal = GregorianCalendar.getInstance();
                cal.setTime(now);
                cal.add(Calendar.SECOND, (int) cacheTime);
                entry.setDateOut(cal.getTime());

                items.put(key, entry);
                Reveal.RevealLogger.v( "Cache aadded " + key.toString() );

                if ( objectCacheListener != null )
                    objectCacheListener.onItemAdded( entry );

                result = true;
            }

            return result;
        }

        // remove old cached entries - this wil get called on each access - it could be
        // called on a timer to allow entries to go away faster, but then we would steal
        // time in the background. Which may not be desirable.
        public void purgeOld() {
            Date now = new Date();
            synchronized ( items ) {
                for (CCObjectCacheEntry<K, T> item : allValues()) {
                    if (item.getDateOut().compareTo(now) < 0) {
                        //Reveal.RevealLogger.v("deleting " + item.getKey() + " expired " + item.getDateOut());

                        if (objectCacheListener != null)
                            objectCacheListener.onItemAdded(item);

                        items.remove(item.getKey());

                    }
                }
            }
        }

        public List<CCObjectCacheEntry<K, T>> allValues() {
            ArrayList<CCObjectCacheEntry<K, T>> result = new ArrayList<CCObjectCacheEntry<K, T>>();

            synchronized ( items ) {
                Iterator<CCObjectCacheEntry<K, T>> iter = items.values().iterator();

                while (iter.hasNext()) {
                    CCObjectCacheEntry<K, T> obj = iter.next();
                    result.add(obj);
                }
            }

            return result;
        }

        public List<T> values() {
            ArrayList<T> result = new ArrayList<T>();

            synchronized ( items ) {
                for (CCObjectCacheEntry<K, T> obj : items.values()) {
                    result.add(obj.getObject());
                }
            }

            return result;
        }


        public CCObjectCacheListener<K, T> getObjectCacheListener() {
            return objectCacheListener;
        }

        public void setObjectCacheListener(CCObjectCacheListener<K, T> objectCacheListener) {
            this.objectCacheListener = objectCacheListener;
        }
    }

    public class CCObjectCacheEntry<K, T> {
        private K key;
        private T object;
        private Date dateIn;
        private Date dateOut;

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public T getObject() {
            return object;
        }

        public void setObject(T object) {
            this.object = object;
        }

        public Date getDateIn() {
            return dateIn;
        }

        public void setDateIn(Date dateIn) {
            this.dateIn = dateIn;
        }

        public Date getDateOut() {
            return dateOut;
        }

        public void setDateOut(Date dateOut) {
            this.dateOut = dateOut;
        }
    }

    private Context applicationContext;
    private BeaconManager beaconManager;

    //TODO: remove this set and rely on the cache
    private Set<String> foundBeaconAddressSet = new HashSet<String>();
    private Boolean isBackgroundScanningEnabled = true;
    private CCObjectCache<String, Reveal.RevealBeacon> cachedBeacons = new CCObjectCache<String, Reveal.RevealBeacon>();
    private ArrayList<RevealScanGroup> bluetoothDeviceScanGroups = new ArrayList<RevealScanGroup>();
    // Temporary storage for partial Eddystone Beacons
    private HashMap<String, Reveal.RevealBeacon> partialEddystonePackets = new HashMap<String, Reveal.RevealBeacon>();
    private Reveal.OnBeaconFoundListener beaconFoundListener = null;
    private Reveal.BeaconScanningProperties beaconScanningProperties;
    private boolean setupComplete = false;

    public RevealBeaconService(Context applicationContext ) {
        this.applicationContext = applicationContext;

        setupIfNeeded();
    }

    public void setupIfNeeded() {
        if ( !setupComplete ) {
            setup();
        }
    }

    private void setup() {
        boolean fineAccess = Reveal.selfPermissionGranted(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION);
        boolean courseAccess = Reveal.selfPermissionGranted(applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION);

        Reveal.log( "RevealBeaconService fine=" + fineAccess + ", course=" + courseAccess, "STATE" );

        if ( fineAccess || courseAccess ) {
            if ( isBluetoothEnabled(applicationContext) )
                Reveal.getInstance().setStatus( Reveal.STATUS_BLUETOOTH, 1, "Bluetooth permission granted" );
            else
                Reveal.getInstance().setStatus( Reveal.STATUS_BLUETOOTH, 0, "Bluetooth disabled" );
            this.beaconManager = BeaconManager.getInstanceForApplication(applicationContext);
            //this.beaconManager.setDebug( true );
            this.beaconManager.bind( this );

            if (!this.beaconManager.isAnyConsumerBound()) {
                this.deleteScanGroups(RevealBeaconService.TYPE_NAME_SECURECAST);
                }

            this.loadFoundBeacons();

            this.setupComplete = true;
            this.cachedBeacons.setObjectCacheListener(new CCObjectCacheListener<String, Reveal.RevealBeacon>() {

                @Override
                public void onItemAdded(CCObjectCacheEntry<String, Reveal.RevealBeacon> entry) {

                }

                @Override
                public void onItemRemoved(CCObjectCacheEntry<String, Reveal.RevealBeacon> entry) {
                    if ( RevealBeaconService.this.beaconFoundListener != null )
                        RevealBeaconService.this.beaconFoundListener.onBeaconLeave( entry.key );
                }

                @Override
                public void onItemAccessed(CCObjectCacheEntry<String, Reveal.RevealBeacon> entry) {

                }
            });
        }
        else {
            Reveal.log("Beacon scanning disabled due to location permissions", "WARNING", "STATE");
            Reveal.getInstance().setStatus( Reveal.STATUS_BLUETOOTH, 1, "Bluetooth disabled due to permissions" );
        }
    }

    public void loadBeaconParsers( List<Integer> codes ) {

        List<String> layouts = getBeaconStringList();

        Reveal.RevealLogger.d("Scanning for beacons:\n" + layouts);

        BeaconManager manager = this.beaconManager;

        if (manager != null) {
            BeaconManager.BeaconLeScanCallback oldCallback = manager.getBeaconCallback();

            Reveal.RevealLogger.v("Non-beacon hook: " + oldCallback);

            // patch into alt beacon to retrieve all bluetooth devices we encounter
            manager.setBeaconCallback( new BeaconManager.BeaconLeScanCallback() {
                @Override
                public void onBeaconLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] bytes) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        if (bluetoothDevice != null ) {
                            // only bother with BLE devices or combined devices
                            Reveal.UserLogListener listener = Reveal.getInstance().getLogger();
                            String hex = null;
                            ArrayList<Reveal.PDU> pdus = null;

                            if (bytes != null) {
                                hex = Reveal.RevealBeacon.bytesToHex(bytes);

                                pdus = Reveal.PDU.PDUList(bytes);
                            } else {
                                hex = "No payload data";
                                pdus = new ArrayList<Reveal.PDU>();
                            }

                            Reveal.BluetoothItem info = new Reveal.BluetoothItem();

                            info.setDevice(bluetoothDevice);
                            info.setAddress(bluetoothDevice.getAddress());
                            info.setPdus(pdus);
                            info.setRssi(rssi);
                            info.setUuids(new ArrayList<String>());

                            Reveal.PDUiBeacon iBeacon = new Reveal.PDUiBeacon(info);
                            Reveal.PDUiBeacon iBeaconData = iBeacon;
                            String name = bluetoothDevice.getName();
                            Reveal.RevealBeacon beacon = null;
                            byte[] payload;
                            Integer bits = 0;

                            switch (iBeacon.getServiceUUID()) {
                                case 0xfeaa:
                                    // handle eddystone here
                                    ArrayList<Byte> serviceData = iBeacon.getServiceData();

                                    beacon = new Reveal.RevealBeacon();
                                    beacon.setComplete(false);
                                    int[] typeArray = {Reveal.PDU.SERVICE_DATA};
                                    ArrayList<Reveal.PDU> servicePDUs = info.getPDUTypes(typeArray);

                                    for (Reveal.PDU item : servicePDUs) {


                                        // determine the beacon type and handle custom elements for that type
                                        Reveal.RevealBeacon otherEddystoneBeacon = null;

                                        int type = item.int8at(2);
                                        beacon.setBeaconType("Eddystone");

                                        switch (type) {
                                            case TYPE_EDDYSTONE:
                                                // because the eddy stone comes in two parts we need to save the first
                                                // one we get and combine the two
                                                otherEddystoneBeacon = partialEddystonePackets.get(beacon.getAddress());
                                                if (otherEddystoneBeacon != null) {
                                                    beacon.setProximityUUID(otherEddystoneBeacon.getProximityUUID());
                                                    beacon.setMajor(otherEddystoneBeacon.getMajor());
                                                } else {
                                                    partialEddystonePackets.put(beacon.getAddress(), beacon);
                                                }

                                                beacon.setBeaconType(TYPE_NAME_EDDYSTONE);

                                                byte[] compressedURL = item.bytesAt(4);
                                                String uncompressed = UrlBeaconUrlCompressor.uncompress(compressedURL);
                                                //Reveal.RevealLogger.d( "Eddystone DEBUG " + type + " Data:" + item.getHexAt(4, item.getLength()) + " URL?: " + uncompressed + " at " + info.getAddress() );
                                                beacon.setUrl(uncompressed);
                                                if (beacon.getMajor() != null)
                                                    if (beacon.getMajor().length() > 0) {
                                                        beacon.setComplete(true);
                                                        partialEddystonePackets.remove(beacon.getAddress());
                                                    }
                                                break;

                                            case TYPE_EDDYSTONE2:
                                                otherEddystoneBeacon = partialEddystonePackets.get(beacon.getAddress());
                                                if (otherEddystoneBeacon != null) {
                                                    beacon.setUrl(otherEddystoneBeacon.getUrl());
                                                } else {
                                                    partialEddystonePackets.put(beacon.getAddress(), beacon);
                                                }
                                                beacon.setProximityUUID(item.getHexAt(4, 10));
                                                beacon.setMajor("0x" + item.getHexAt(14, 6));

                                                beacon.setBeaconType(TYPE_NAME_EDDYSTONE);
                                                String url = beacon.getUrl();
                                                if ((url != null) && (url.length() > 0)) {
                                                    beacon.setComplete(true);
                                                    partialEddystonePackets.remove(beacon.getAddress());
                                                }
                                                break;
                                        }
                                    }
                                    break;

                                case 0xfeed:
                                    // tile
                                    beacon = new Reveal.RevealBeacon();
                                    beacon.setBeaconType("Tile");

                                    // get payload
                                    payload = Arrays.copyOfRange(bytes, 13, bytes.length);
                                    bits = 0;

                                    // see if any data present
                                    for (byte b : payload)
                                        bits = bits | b;

                                    // if all zero's then send nothing to match iOS
                                    if (bits != 0)
                                        beacon.setPayload(payload);
                                    beacon.setComplete(true);
                                    break;

                                case 0x0f3e:
                                    // TrackR
                                    beacon = new Reveal.RevealBeacon();
                                    beacon.setBeaconType("TrackR");

                                    // get payload
                                    payload = Arrays.copyOfRange(bytes, 13, bytes.length);

                                    // see if any data present
                                    for (byte b : payload)
                                        bits = bits | b;

                                    // if all zero's then send nothing to match iOS
                                    if (bits != 0)
                                        beacon.setPayload(payload);
                                    beacon.setComplete(true);
                                    break;

                                default:
                                    switch (iBeacon.getManufacturerCode()) {
                                        // we can add specific handler s for non ibeacon but similar devices here pebblebee?

                                        case Reveal.PDUiBeacon.Apple:
                                            ArrayList<Byte> data = iBeacon.getServiceData();
                                            if ( ( data != null ) && iBeacon.isValid() ) {
                                                Reveal.RevealLogger.v("PDUiBeacon found " + iBeacon.getUuid() + " - " + iBeacon.getMajor() + "/" + iBeacon.getMinor());

                                                // NOTE: This feels a little hacky, however if we
                                                //       accept anything we get way too much data,
                                                //       so we are only accepting sizes we have
                                                //       seen for now
//                                                if ( (data.size() == 13) || (data.size() == 25) ) {

                                                    beacon = new Reveal.RevealBeacon(iBeacon);
                                                    beacon.setBeaconType(iBeacon.getManufacturer());

                                                    if (iBeacon.getUuid() != null) {
                                                        beacon.setIdentifier(iBeacon.getUuid()
                                                                + "-" + iBeacon.getMajor()
                                                                + "-" + iBeacon.getMinor());
                                                    }
//                                                }
                                            }
                                            break;

                                        default:
                                            // if we don't recognise it then assume it is an iBeacon if we can decode it as such
                                            if (iBeacon.isValid()) {
                                                Reveal.RevealLogger.v("PDUiBeacon found " + iBeacon.getUuid() + " - " + iBeacon.getMajor() + "/" + iBeacon.getMinor());

                                                beacon = new Reveal.RevealBeacon(iBeacon);
                                                beacon.setBeaconType(iBeacon.getManufacturer());

                                                if (iBeacon.getUuid() != null)
                                                    beacon.setIdentifier(iBeacon.getUuid());
                                            } else {
                                                // ignoring unknown type
                                                // TODO: do we want to report the PDU's and let Reveal
                                                //       sort these out on the server side?
                                                iBeacon = null;

                                                // uncomment this code to show possible beacons that we are ignoring
//                                            if ( bluetoothDevice.getType() == 2 || bluetoothDevice.getType() == 3 ) {
//                                                Reveal.RevealLogger.v("Found unknown BLE device:\nName:" + bluetoothDevice.getName()
//                                                        + "\naddress: " + bluetoothDevice.getAddress()
//                                                        + "\ntype: " + bluetoothDevice.getType()
//                                                        + "\nRSSI:" + rssi + "\nbytes: " + hex);
//                                            }
                                            }
                                            break;
                                    }
                                    break;
                            }

                            if (beacon != null) {

                                beacon.setName(name);
                                beacon.setAddress(bluetoothDevice.getAddress());
                                beacon.setRssi(rssi);

                                // if no type set then  use the vendor from the PDU
                                if (beacon.getBeaconTypeCode() == 0)
                                    beacon.setBeaconTypeCode(iBeacon.getManufacturerCode());

                                // Create an id for the device, if a name is defined make it a prefix as a
                                // convienance
                                if (beacon.getIdentifier() == null) {
                                    if (name != null)
                                        beacon.setIdentifier(name + "-" + bluetoothDevice.getAddress());
                                    else
                                        beacon.setIdentifier(bluetoothDevice.getAddress());
                                }

                                beacon.setComplete(true);

                                int power = beacon.getTxPower();

                                // if no power provided so use a baseline guess for distance calculations
                                if (power == 0)
                                    power = -47;

                                if (Reveal.getInstance().getDistanceCalculator() != null) {
                                    beacon.setDistance(Reveal.getInstance().getDistanceCalculator().calculateDistance(power, rssi));
                                } else {
                                    Reveal.RevealLogger.d("Distance calculator not set.  Distance will bet set to -1");
                                    beacon.setDistance(-1.0);
                                }

                                beacon.setAccuracy(Reveal.RevealBeacon.calculateAccuracy(power, rssi));

                                // set proximity integer
                                if (beacon.getAccuracy() < 0.0)
                                    beacon.setProximity(PROXIMITY_UNKNOWN);
                                else if (beacon.getAccuracy() < 2.0)
                                    beacon.setProximity(PROXIMITY_IMMEDIATE);
                                else if (beacon.getAccuracy() < 15.0)
                                    beacon.setProximity(PROXIMITY_NEAR);
                                else
                                    beacon.setProximity(PROXIMITY_FAR);

                                // Add the beacon to the list
                                processBeacon(beacon);

                            }


                            if ((bluetoothDevice.getType() == 2) || (bluetoothDevice.getType() == 3)) {


                                // enable the following debug to log all BLE devices encountered (searching for new beacon types)
//                                Reveal.RevealLogger.v( "Found BLE device:\nName:" + bluetoothDevice.getName()
//                                                     + "\naddress: " + bluetoothDevice.getAddress()
//                                                     + "\ntype: " + bluetoothDevice.getType()
//                                                     + "\nRSSI:" + rssi + "\nbytes: " + hex );

                                // NOTE: this is a hardcoded constant derived from
                                //       http://www.argenox.com/a-ble-advertising-primer/ we may need to fully
                                //       decode the PDU in the future for other beacons, but for now this check
                                //       should be adequate
                                //
                                //       This also assumes that the datalength is fixed and 0 filled which
                                //       appears to be the case for tile beacons.
                                //
                                //       Also: http://j2abro.blogspot.com/2014/06/understanding-bluetooth-advertising.html


                                // check for pebblebee beacon
                                if (hex.startsWith("02010609030318")) {
//                                    Reveal.RevealLogger.v("Found pebblebee beacon:\nName:" + bluetoothDevice.getName()
//                                            + "\naddress: " + bluetoothDevice.getAddress()
//                                            + "\ntype: " + bluetoothDevice.getType()
//                                            + "\nRSSI:" + rssi + "\nbytes: " + hex);
                                    beacon = new Reveal.RevealBeacon();

                                    // get payload
                                    payload = Arrays.copyOfRange(bytes, 13, bytes.length);
                                    bits = 0;

                                    // see if any data present
                                    for (byte b : payload)
                                        bits = bits | b;

                                    // if all zero's then send nothing to match iOS
                                    if (bits != 0)
                                        beacon.setPayload(payload);

                                    beacon.setName(name);
                                    beacon.setBeaconType("PebbleBee");
                                    beacon.setAddress(bluetoothDevice.getAddress());
                                    beacon.setRssi(rssi);

                                    // Create an id for the device, if a name is defined make it a prefix as a
                                    // convienance
                                    if (name != null)
                                        beacon.setIdentifier(name + "-" + bluetoothDevice.getAddress());
                                    else
                                        beacon.setIdentifier(bluetoothDevice.getAddress());
                                    beacon.setComplete(true);

                                    // Add the beacon to the list
                                    processBeacon(beacon);
                                }
                            }

                            if (listener != null) {
                                listener.logDevice(info, iBeaconData);
                            }
                        }
                    }
                }
            });
        }
        else {
            Reveal.log("Beacon scanning disabled due to location permissions", "ERROR", "STATE" );
        }
    }

    public void startBeaconScanning(Context context, Reveal.BeaconScanningProperties properties ) {
        boolean fineAccess = Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_FINE_LOCATION);
        boolean courseAccess = Reveal.selfPermissionGranted(context, Manifest.permission.ACCESS_COARSE_LOCATION);

        Reveal.log("startBeaconScanning fine=" + fineAccess + ", course=" + courseAccess, "STATE");

        if ((fineAccess || courseAccess)) {
            if ( isBluetoothEnabled(context) )
                Reveal.getInstance().setStatus( Reveal.STATUS_BLUETOOTH, Reveal.STATUS_SUCCEED, "Bluetooth permission granted" );
            else
                Reveal.getInstance().setStatus(Reveal.STATUS_BLUETOOTH, Reveal.STATUS_FAIL, "Bluetooth disabled");

            //Remove any existing found beacons
            clearBeaconsFound(context);
            this.beaconScanningProperties = properties;

            this.cachedBeacons.cacheTime = properties.getCacheTTL();

            BeaconManager manager = this.beaconManager;

            this.loadBeaconParsers(this.beaconScanningProperties.getAdditionalSecureCastManufacturerCodes());

            if ( beaconManager != null ) {
                manager.setForegroundScanPeriod(properties.getScanDuration() * 1001);
                if (properties.getBackgroundEnabled())
                    manager.setBackgroundScanPeriod(properties.getScanDuration() * 1001);
                else
                    manager.setBackgroundScanPeriod(0);

                manager.setForegroundBetweenScanPeriod(properties.getScanInterval() * 1001);
                if (properties.getBackgroundEnabled())
                    manager.setForegroundBetweenScanPeriod(properties.getScanInterval() * 1001);
                else
                    manager.setForegroundBetweenScanPeriod(0);

                try {
                    beaconManager.startScanning();
                } catch (RemoteException e) {
                    e.printStackTrace();
                    Reveal.getInstance().setStatus( Reveal.STATUS_BLUETOOTH, 0, e.toString() );
                }
            }
            else {
                Reveal.log("beaconManager not set in startBeaconScanning", "WARNING", "STATE");
                Reveal.getInstance().setStatus( Reveal.STATUS_BLUETOOTH, 0, "beaconManager not set in startBeaconScanning" );
            }
        }
        else {
            Reveal.log("No location provided for beacon scanner", "WARNING", "STATE");
            Reveal.getInstance().setStatus( Reveal.STATUS_BLUETOOTH, 0, "No location provided for beacon scanner" );
        }
    }

    public void stopBeaconScanning( Context context ) {
        handleTimedOutEddyStonePartials();

        if (isBackgroundScanningEnabled && (this.beaconManager != null )) {
            try {
                this.beaconManager.stopScanning();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        synchronized ( this ) {
            this.foundBeaconAddressSet = new HashSet<String>();
        }
        this.storeFoundBeacons();

        if ( this.beaconManager != null )
            this.beaconManager.setBeaconCallback( null );
    }

    public List<Reveal.RevealBeacon> getBeacons()
    {
        return cachedBeacons.values();
    }

    private Reveal.RevealBeacon processBeacon( Reveal.RevealBeacon revealBeacon ) {

        DetectionTracker.getInstance().recordDetection();
        // reject incomplete beacon
        Boolean complete = revealBeacon.isComplete();
        final Reveal.RevealBeacon result = revealBeacon;

        Reveal.getInstance().setStatus( Reveal.STATUS_BLUETOOTH, 1, "Beacons found");

        if ( complete ) {
            Reveal.RevealLogger.v( "Have complete beacon: " + revealBeacon.toString() );
            switch( updateCacheWithBeacon( revealBeacon ) )
            {
                case New:
                case Closer:
                    if ( revealBeacon.getDistance() < 2.0 ) {
                        Reveal.RevealLogger.v("processBeacon, we are within a couple of meters so sending " + revealBeacon.getIdentifier() + " @ " + revealBeacon.getDistance() + " meters" );
                        processCompleteBeacon(revealBeacon);
                    }
                    else
                    {
                        Context context = this.applicationContext;
                        Handler mainHandler = new Handler(context.getMainLooper());

                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run()
                            {
                                Reveal.RevealLogger.v( "processBeacon, we are waiting to get closer to " + result.getIdentifier() + " @ " + result.getDistance() + " meters" );
                                CountDownTimer timer = result.getSendTimer();
                                if ( timer != null ) {
                                    timer.cancel();
                                    result.setSendTimer( null );
                                }

                                timer = new CountDownTimer( 30000, 1000 )
                                {
                                    public void onTick(long millisUntilFinished) {
                                        // We don't actually need to do anything on ticks, except turn on the following debug for testing
                                        //Reveal.RevealLogger.v( "processBeacon tick " + millisUntilFinished + " - " + result.getIdentifier() + " @ " + result.getDistance() + " meters" );
                                    }

                                    public void onFinish() {
                                        Reveal.RevealLogger.v( "processBeacon, we have waited long enough " + result.getIdentifier() + " @ " + result.getDistance() + " meters" );
                                        processCompleteBeacon( result );
                                    }
                                }.start();

                                result.setSendTimer( timer );
                            }
                        };
                        mainHandler.post(myRunnable);
                    }
                    break;

                case Existing:
                    Reveal.RevealLogger.v("Beacon already sent");
                    break;
            }
        }
        else
            Reveal.RevealLogger.v( "Not sending incomplete beacon: " + revealBeacon.toString() );

        return result;
    }

    private CacheState updateCacheWithBeacon( Reveal.RevealBeacon revealBeacon )
    {
        CacheState result = CacheState.New;
        Reveal.RevealBeacon old = this.cachedBeacons.get( revealBeacon.getIdentifier() );

        if (old != null ) {
            if ( revealBeacon.getDistance() < old.getDistance() ) {
                result = CacheState.Closer;
                old.setDistance( revealBeacon.getDistance());
                old.setProximity( revealBeacon.getProximity() );

                this.cachedBeacons.put( revealBeacon, revealBeacon.getIdentifier() );
                Reveal.log("Moved closer to beacon: " + revealBeacon.getBeaconType() + " " + revealBeacon.getIdentifier(), "STATE");
            }
            else {
                result = CacheState.Existing;
            }
        }
        else {
            Reveal.log("New beacon encountered: " + revealBeacon.getBeaconType() + " " + revealBeacon.getIdentifier(), "STATE");
            this.processCompleteBeacon( revealBeacon );
        }

        return result;
    }

    private Reveal.RevealBeacon processCompleteBeacon( Reveal.RevealBeacon originalBeacon ) {
            final Reveal.RevealBeacon revealBeacon = originalBeacon;

            if (RevealBeaconService.this.cachedBeacons.put(revealBeacon, revealBeacon.getIdentifier()) ) {
                storeFoundBeacons();
                synchronized ( this ) {
                    foundBeaconAddressSet.add(revealBeacon.getAddress());
                }

                final Reveal.LocationService locationService = Reveal.getInstance().getLocationService();

                if ( locationService != null ) {
                    Reveal.RevealLogger.v( "Waiting for a location to be retrieved for " + revealBeacon );
                    locationService.waitForValidLocation(new Reveal.LocationService.OnValidLocationCallback() {
                        @Override
                        public void onLocationFound() {
                           Reveal.GlobalLocation location = new Reveal.GlobalLocation( locationService.getCurrentLocation( getApplicationContext() ) );


                            revealBeacon.setLocation( location );
                            Reveal.RevealLogger.v( "onLocationFound retrieved for " + revealBeacon);
                            //If we don't have real location yet, then retrieve the last known location
                            if (revealBeacon.getLocation() == null) {
                                revealBeacon.setLocation( new Reveal.GlobalLocation( Reveal.Utils.getLastKnownLocation(applicationContext) ));

                                Reveal.RevealLogger.v( "Using last known location location since because we never received one" );
                            }
                            else
                                Reveal.RevealLogger.v( "Location received" );

                            Reveal.getInstance().sendDiscoveryOfBeacon(applicationContext, revealBeacon);

                            if ( beaconFoundListener != null ) {
                                Map<String, Object> properties =revealBeacon.getProperties();
                                properties.put("beacon", revealBeacon);

                                //Reveal.RevealLogger.d( "[NAME]='foundBeaconOfType' [TYPE]='propertyCompareType' [ID]='" + revealBeacon.getIdentifier() + "'\n" + RevealBeaconService.propertiesFromMap( properties, "" ));
                                RevealBeaconService.this.beaconFoundListener.onBeaconDiscovered( properties );
                            }
                        }
                    });
                }
                else {
                    Reveal.RevealLogger.v("No location service provided sending Beacon without location");
                    Reveal.getInstance().sendDiscoveryOfBeacon(applicationContext, revealBeacon);
                }

            } else
                Reveal.RevealLogger.v("processCompleteBeacon Beacon already sent: " + revealBeacon );

        return revealBeacon;
    }

    private void handleTimedOutEddyStonePartials() {
        ArrayList<String> removed = new ArrayList<String>();

        for( Map.Entry entry : partialEddystonePackets.entrySet() )
        {
            Reveal.RevealBeacon beacon = (Reveal.RevealBeacon) entry.getValue();
            if ( ( beacon != null) && ( beaconScanningProperties != null ) ) {
                Reveal.RevealLogger.v( "testing " + beacon.getIdentifier() + " AGE: " + beacon.age() + " complete: " + beacon.isComplete() + " @ " + beacon.getDiscoveryTime() );
                if ( !beacon.isComplete() && beacon.age() > beaconScanningProperties.getEddystoneTimeout() ) {
                    beacon.setComplete( true );
                    processBeacon( beacon );
                    removed.add( beacon.getAddress() );
                }
            }
        }

        for( String key : removed ) {
            partialEddystonePackets.remove( key );
        }
    }

    @Override
    public void onBeaconServiceConnect() {
        Reveal.log( "Beacon service connected", "STATE" );
    }

    @Override
    public boolean bindService(Intent intent, ServiceConnection connection, int mode) {
        return this.applicationContext.bindService(intent, connection, mode);
    }

    @Override
    public void unbindService(ServiceConnection connection) {
        this.applicationContext.unbindService( connection );
    }

    @Override
    public Context getApplicationContext() {
        return applicationContext;
    }


    //We store and load founds beacons from shared preferences so that the values are maintained across Service instances
    private void storeFoundBeacons() {
        SharedPreferences settings = this.applicationContext.getSharedPreferences(Reveal.FOUND_BEACONS_PREFERENCE_NAME, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = settings.edit();

        // create a copy to return to UI so that will not modify while being iterated
        Set<String> beacons = new HashSet<String>();

        synchronized ( this ) {
            beacons.addAll(this.foundBeaconAddressSet);
        }

        String value = "";
        for (String item : beacons ) {
            if ( value.length() > 0 )
                value += ",";
            value += item;
        }

        editor.putString(Reveal.FOUND_BEACONS_PREFERENCE_KEY, value);
        editor.apply();
    }

    private void loadFoundBeacons() {
        SharedPreferences sharedPreferences = this.applicationContext.getSharedPreferences(Reveal.FOUND_BEACONS_PREFERENCE_NAME, Context.MODE_PRIVATE );
        String listAsString = sharedPreferences.getString(Reveal.FOUND_BEACONS_PREFERENCE_KEY, "");

        List<String> list = Arrays.asList(listAsString.split(","));
        synchronized ( this ) {
            this.foundBeaconAddressSet = new HashSet<String>(list);
        }
    }

    private void clearBeaconsFound(Context context) {
        SharedPreferences settings = context.getSharedPreferences(Reveal.FOUND_BEACONS_PREFERENCE_NAME, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = settings.edit();
        editor.putString( Reveal.FOUND_BEACONS_PREFERENCE_KEY, "" );
        editor.apply();
    }

    //Used for debugging
    private void debugReadBeacons()
    {
        SharedPreferences sharedPreferences = this.applicationContext.getSharedPreferences(Reveal.FOUND_BEACONS_PREFERENCE_NAME, Context.MODE_PRIVATE );
        Map<String,?> keys = sharedPreferences.getAll();

        Iterator it = keys.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Reveal.RevealLogger.v("   MAP ENTRY: " + pair.getKey() + " = " + pair.getValue());
        }
    }

    // MARK: - Group methods -

    public void addScanGroup( RevealScanGroup group ) {
        this.bluetoothDeviceScanGroups.add( group );
    }

    public List<String> getBeaconStringList() {
        ArrayList<String> result = new ArrayList<String>();

        for( RevealScanGroup group : this.bluetoothDeviceScanGroups) {
            for( String item : group.getBeaconStrings() ) {
                result.add( item );
            }
        }

        return result;
    }

    public void deleteScanGroups( String type ) {
        if ( type != null ) {
            ArrayList<RevealScanGroup> originals = new ArrayList<RevealScanGroup>();
            originals.addAll(this.bluetoothDeviceScanGroups);

            for (RevealScanGroup group : originals) {
                if (type.equals(group.type))
                    this.bluetoothDeviceScanGroups.remove(group);
            }
        }
        else
            this.bluetoothDeviceScanGroups = new ArrayList<RevealScanGroup>();
    }

    public RevealScanGroup getBeaconGroup( int vendor, String serviceId ) {
        RevealScanGroup result = null;

        for( RevealScanGroup group : this.bluetoothDeviceScanGroups) {
            // check for serviceId requirement and match
            if ( (serviceId != null ) && ( group.getServiceId() != null ) ) {
                if (serviceId.equals( group.getServiceId()) ) {
                    result = group;
                    break;
                }
            }
            // check for group only requirement and match
            else if ( ( vendor >= group.getStartVendor() ) && ( vendor <= group.getEndVendor() )  ) {
                if ( ( serviceId == null ) && (  group.getServiceId() == null ) ) {
                    result = group;
                    break;
                }
            }
        }

        return result;
    }


    public void setBeaconScanningProperties(Reveal.BeaconScanningProperties beaconScanningProperties) {
        this.beaconScanningProperties = beaconScanningProperties;
    }

    public Reveal.OnBeaconFoundListener getBeaconFoundListener() {
        return beaconFoundListener;
    }

    public void setBeaconFoundListener(Reveal.OnBeaconFoundListener beaconFoundListener) {
        this.beaconFoundListener = beaconFoundListener;
    }

}
