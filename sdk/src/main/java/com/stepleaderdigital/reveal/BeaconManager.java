package com.stepleaderdigital.reveal;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by bobby on 5/10/17.
 */

public class BeaconManager {

    protected Context context;
    // TODO: This code was copied from AltBeacon we should remove this reference
    protected static volatile BeaconManager client = null;

    /**
     * Private lock object for singleton initialization protecting against denial-of-service attack.
     */
    private static final Object SINGLETON_LOCK = new Object();

    /**
     * The default duration in milliseconds of the Bluetooth scan cycle
     */
    public static final long DEFAULT_FOREGROUND_SCAN_PERIOD = 1100;
    /**
     * The default duration in milliseconds spent not scanning between each Bluetooth scan cycle
     */
    public static final long DEFAULT_FOREGROUND_BETWEEN_SCAN_PERIOD = 20000;
    /**
     * The default duration in milliseconds of the Bluetooth scan cycle when no ranging/monitoring clients are in the foreground
     */
    public static final long DEFAULT_BACKGROUND_SCAN_PERIOD = 60000;
    /**
     * The default duration in milliseconds spent not scanning between each Bluetooth scan cycle when no ranging/monitoring clients are in the foreground
     */
    public static final long DEFAULT_BACKGROUND_BETWEEN_SCAN_PERIOD = 5 * 60 * 1000;
    /**
     * The default duration in milliseconds of region exit time
     */
    public static final long DEFAULT_EXIT_PERIOD = 10000L;

    private static boolean manifestCheckingDisabled = false;
    private boolean backgroundMode = false;
    private boolean backgroundModeUninitialized = true;
    private final ConcurrentMap<BeaconConsumer, ConsumerInfo> consumers = new ConcurrentHashMap<BeaconConsumer,ConsumerInfo>();
    private Messenger serviceMessenger = null;
    private BeaconLeScanCallback beaconCallback;

    private long foregroundScanPeriod = DEFAULT_FOREGROUND_SCAN_PERIOD;
    private long foregroundBetweenScanPeriod = DEFAULT_FOREGROUND_BETWEEN_SCAN_PERIOD;
    private long backgroundScanPeriod = DEFAULT_BACKGROUND_SCAN_PERIOD;
    private long backgroundBetweenScanPeriod = DEFAULT_BACKGROUND_BETWEEN_SCAN_PERIOD;

    protected static String distanceModelUpdateUrl = "http://static.revealmobile.com/android-distance.json";

    public interface BeaconLeScanCallback {
        /**
         * NOTE: This method is NOT called on the main UI thread.
         *
         * @param device Identifies the remote device
         * @param rssi The RSSI value for the remote device as reported by the
         *             Bluetooth hardware. 0 if no RSSI value is available.
         * @param scanRecord The content of the advertisement record offered by
         *                   the remote device.
         */
        void onBeaconLeScan(BluetoothDevice device, int rssi, byte[] scanRecord);
    }

    public class ServiceNotDeclaredException extends RuntimeException {
        public ServiceNotDeclaredException() {
            super("The BeaconService is not properly declared in AndroidManifest.xml.  If using Eclipse," +
                    " please verify that your project.properties has manifestmerger.enabled=true");
        }
    }

    private class ConsumerInfo {
        public boolean isConnected = false;
        public BeaconServiceConnection beaconServiceConnection;

        public ConsumerInfo() {
            this.isConnected = false;
            this.beaconServiceConnection= new BeaconServiceConnection();
        }
    }

    private class BeaconServiceConnection implements ServiceConnection {
        private BeaconServiceConnection() {
        }

        // Called when the connection with the service is established
        public void onServiceConnected(ComponentName className, IBinder service) {
            Reveal.RevealLogger.d( "we have a connection to the service now");
            serviceMessenger = new Messenger(service);
            synchronized(consumers) {
                Iterator<Map.Entry<BeaconConsumer, ConsumerInfo>> iter = consumers.entrySet().iterator();
                while (iter.hasNext()) {
                    Map.Entry<BeaconConsumer, ConsumerInfo> entry = iter.next();

                    if (!entry.getValue().isConnected) {
                        entry.getKey().onBeaconServiceConnect();
                        entry.getValue().isConnected = true;
                    }
                }
            }
        }

        // Called when the connection with the service disconnects
        public void onServiceDisconnected(ComponentName className) {
            Reveal.RevealLogger.e("onServiceDisconnected");
            serviceMessenger = null;
        }
    }

    /**
     * An accessor for the singleton instance of this class.  A context must be provided, but if you need to use it from a non-Activity
     * or non-Service class, you can attach it to another singleton or a subclass of the Android Application class.
     */
    public static BeaconManager getInstanceForApplication(Context context) {
        /*
         * Follow double check pattern from Effective Java v2 Item 71.
         *
         * Bloch recommends using the local variable for this for performance reasons:
         *
         * > What this variable does is ensure that `field` is read only once in the common case
         * > where it's already initialized. While not strictly necessary, this may improve
         * > performance and is more elegant by the standards applied to low-level concurrent
         * > programming. On my machine, [this] is about 25 percent faster than the obvious
         * > version without a local variable.
         *
         * Joshua Bloch. Effective Java, Second Edition. Addison-Wesley, 2008. pages 283-284
         */
        BeaconManager instance = client;
        if (instance == null) {
            synchronized (SINGLETON_LOCK) {
                instance = client;
                if (instance == null) {
                    client = instance = new BeaconManager(context);
                }
            }
        }
        return instance;
    }

    protected BeaconManager(Context context) {
        this.context = context.getApplicationContext();
        if (!manifestCheckingDisabled) {
            verifyServiceDeclaration();
        }
    }

    private String callbackPackageName() {
        String packageName = context.getPackageName();
        Reveal.RevealLogger.d("callback packageName: %s", packageName);
        return packageName;
    }

    @TargetApi(18)
    public void startScanning() throws RemoteException {
        if (!isBleAvailable()) {
            Reveal.RevealLogger.w( "No BLE so we can't scan");
            return;
        }

        if (serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        }

        Reveal.RevealLogger.d("Starting scanning" );
        Message msg = Message.obtain(null, BeaconService.MSG_START_MONITORING, 0, 0);
        msg.obj = new StartRMData( callbackPackageName(), this.getScanPeriod(), this.getBetweenScanPeriod(), this.backgroundMode);
        serviceMessenger.send(msg);
    }

    @TargetApi(18)
    public void setScanPeriods() throws RemoteException {
        if (serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        }

        Reveal.RevealLogger.d("Starting scanning" );
        Message msg = Message.obtain(null, BeaconService.MSG_SET_SCAN_PERIODS, 0, 0);
        msg.obj = new StartRMData( callbackPackageName(), this.getScanPeriod(), this.getBetweenScanPeriod(), this.backgroundMode);
        serviceMessenger.send(msg);
    }

    @TargetApi(18)
    public void stopScanning() throws RemoteException {
        if (!isBleAvailable()) {
            Reveal.RevealLogger.w( "No BLE so we can't scan or stop one");
            return;
        }

        if (serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        }

        try {
            Reveal.RevealLogger.d("Stopping scanning");
            Message msg = Message.obtain(null, BeaconService.MSG_STOP_MONITORING, 0, 0);
            msg.obj = new StartRMData(callbackPackageName(), this.getScanPeriod(), this.getBetweenScanPeriod(), this.backgroundMode);
            serviceMessenger.send(msg);
        }
        catch( RuntimeException exp ) {
            Reveal.RevealLogger.w( "A runtime exception occurred and is being suppressed to prevent a crash, please report the following exception to Reveal support" );
            Reveal.RevealLogger.w( exp );
        }
    }




    /**
     * Tells the <code>BeaconService</code> to start looking for beacons that match the passed
     * <code>Region</code> object.  Note that the Region's unique identifier must be retained to
     * later call the stopMonitoringBeaconsInRegion method.
     *
     * @param region
     * @see BeaconManager#setMonitorNotifier(MonitorNotifier)
     * @see BeaconManager#stopMonitoringBeaconsInRegion(Region region)
     * @see MonitorNotifier
     * @see Region
     */

    /**
     * Updates an already running scan with scanPeriod/betweenScanPeriod according to Background/Foreground state.
     * Change will take effect on the start of the next scan cycle.
     *
     * @throws RemoteException - If the BeaconManager is not bound to the service.
     */
    @TargetApi(18)
    public void updateScanPeriods() throws RemoteException {
        if (!isBleAvailable()) {
            Reveal.RevealLogger.w( "Method invocation will be ignored.");
            return;
        }
        if (serviceMessenger == null) {
            throw new RemoteException("The BeaconManager is not bound to the service.  Call beaconManager.bind(BeaconConsumer consumer) and wait for a callback to onBeaconServiceConnect()");
        }
        Message msg = Message.obtain(null, BeaconService.MSG_SET_SCAN_PERIODS, 0, 0);
        Reveal.RevealLogger.d( "updating background flag to " + backgroundMode);
        Reveal.RevealLogger.d( "updating scan period to " + this.getScanPeriod() + ", " + this.getBetweenScanPeriod());
        msg.obj = new StartRMData(this.getScanPeriod(), this.getBetweenScanPeriod(), this.backgroundMode);
        serviceMessenger.send(msg);
    }

    /**
     * Binds an Android <code>Activity</code> or <code>Service</code> to the <code>BeaconService</code>.  The
     * <code>Activity</code> or <code>Service</code> must implement the <code>beaconConsumer</code> interface so
     * that it can get a callback when the service is ready to use.
     *
     * @param consumer the <code>Activity</code> or <code>Service</code> that will receive the callback when the service is ready.
     */
    public void bind(BeaconConsumer consumer) {
        if (!isBleAvailable()) {
            Reveal.RevealLogger.w( "Method invocation will be ignored.");
            return;
        }
        if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Reveal.RevealLogger.w( "This device does not support bluetooth LE.  Will not start beacon scanning.");
            return;
        }
        synchronized (consumers) {
            ConsumerInfo newConsumerInfo = new ConsumerInfo();
            ConsumerInfo alreadyBoundConsumerInfo = consumers.putIfAbsent(consumer, newConsumerInfo);
            if (alreadyBoundConsumerInfo != null) {
                Reveal.RevealLogger.d("This consumer is already bound");
            }
            else {
                Reveal.RevealLogger.d( "This consumer is not bound.  binding: " + consumer);
                Intent intent = new Intent(consumer.getApplicationContext(), BeaconService.class);
                consumer.bindService(intent, newConsumerInfo.beaconServiceConnection, Context.BIND_AUTO_CREATE);
                Reveal.RevealLogger.d("consumer count is now: " + consumers.size());
            }
        }
    }

    /**
     * Unbinds an Android <code>Activity</code> or <code>Service</code> to the <code>BeaconService</code>.  This should
     * typically be called in the onDestroy() method.
     *
     * @param consumer the <code>Activity</code> or <code>Service</code> that no longer needs to use the service.
     */
    public void unbind(BeaconConsumer consumer) {
        if (!isBleAvailable()) {
            Reveal.RevealLogger.w("Method invocation will be ignored.");
            return;
        }
        synchronized (consumers) {
            if (consumers.containsKey(consumer)) {
                Reveal.RevealLogger.d( "Unbinding");
                consumer.unbindService(consumers.get(consumer).beaconServiceConnection);
                consumers.remove(consumer);
                if (consumers.size() == 0) {
                    // If this is the last consumer to disconnect, the service will exit
                    // release the serviceMessenger.
                    serviceMessenger = null;
                    // Reset the mBackgroundMode to false, which is the default value
                    // This way when we restart ranging or monitoring it will always be in
                    // foreground mode
                    backgroundMode = false;
                }
            }
            else {
                Reveal.RevealLogger.d( "This consumer is not bound to: " + consumer);
                Reveal.RevealLogger.d( "Bound consumers: ");
                Set<Map.Entry<BeaconConsumer, ConsumerInfo>> consumers = this.consumers.entrySet();
                for (Map.Entry<BeaconConsumer, ConsumerInfo> consumerEntry : consumers) {
                    Reveal.RevealLogger.d(String.valueOf(consumerEntry.getValue()));
                }
            }
        }
    }

    /**
     * Tells you if the passed beacon consumer is bound to the service
     *
     * @param consumer
     * @return
     */
    public boolean isBound(BeaconConsumer consumer) {
        synchronized(consumers) {
            return consumer != null && consumers.get(consumer) != null && (serviceMessenger != null);
        }
    }

    /**
     * Tells you if the any beacon consumer is bound to the service
     *
     * @return
     */
    public boolean isAnyConsumerBound() {
        synchronized(consumers) {
            return consumers.size() > 0 && (serviceMessenger != null);
        }
    }

    /**
     * This method notifies the beacon service that the application is either moving to background
     * mode or foreground mode.  When in background mode, BluetoothLE scans to look for beacons are
     * executed less frequently in order to save battery life. The specific scan rates for
     * background and foreground operation are set by the defaults below, but may be customized.
     * When ranging in the background, the time between updates will be much less frequent than in
     * the foreground.  Updates will come every time interval equal to the sum total of the
     * BackgroundScanPeriod and the BackgroundBetweenScanPeriod.
     *
     * @param backgroundMode true indicates the app is in the background
     *
     * @ see #DEFAULT_FOREGROUND_SCAN_PERIOD
     * @ see #DEFAULT_FOREGROUND_BETWEEN_SCAN_PERIOD;
     * @ see #DEFAULT_BACKGROUND_SCAN_PERIOD;
     * @ see #DEFAULT_BACKGROUND_BETWEEN_SCAN_PERIOD;
     * @ see #setForegroundScanPeriod(long p)
     * @ see #setForegroundBetweenScanPeriod(long p)
     * @ see #setBackgroundScanPeriod(long p)
     * @ see #setBackgroundBetweenScanPeriod(long p)
     */
    public void setBackgroundMode(boolean backgroundMode) {
        if (!isBleAvailable()) {
            Reveal.RevealLogger.w( "Method invocation will be ignored.");
            return;
        }

        backgroundModeUninitialized = false;
        if (this.backgroundMode != backgroundMode) {
            this.backgroundMode = backgroundMode;
            try {
                this.updateScanPeriods();
            } catch (RemoteException e) {
                Reveal.RevealLogger.e("Cannot contact service to set scan periods");
            }
        }
    }

    /**
     * @return indicator of whether any calls have yet been made to set the
     * background mode
     */
    public boolean isBackgroundModeUninitialized() {
        return backgroundModeUninitialized;
    }


    private boolean isBleAvailable() {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            Reveal.RevealLogger.w( "Bluetooth LE not supported prior to API 18.");
            return false;
        } else if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Reveal.RevealLogger.w( "This device does not support bluetooth LE.");
            return false;
        }
        return true;
    }

    private long getScanPeriod() {
        if (backgroundMode) {
            return backgroundScanPeriod;
        } else {
            return foregroundScanPeriod;
        }
    }

    private long getBetweenScanPeriod() {
        if (backgroundMode) {
            return backgroundBetweenScanPeriod;
        } else {
            return foregroundBetweenScanPeriod;
        }
    }

    /**
     * Sets the duration in milliseconds of each Bluetooth LE scan cycle to look for beacons.
     * This function is used to setup the period before calling {@link #bind} or when switching
     * between background/foreground. To have it effect on an already running scan (when the next
     * cycle starts), call {@link #updateScanPeriods}
     *
     * @param p
     */
    public void setForegroundScanPeriod(long p) {
        foregroundScanPeriod = p;
    }

    /**
     * Sets the duration in milliseconds between each Bluetooth LE scan cycle to look for beacons.
     * This function is used to setup the period before calling {@link #bind} or when switching
     * between background/foreground. To have it effect on an already running scan (when the next
     * cycle starts), call {@link #updateScanPeriods}
     *
     * @param p
     */
    public void setForegroundBetweenScanPeriod(long p) {
        foregroundBetweenScanPeriod = p;
    }

    /**
     * Sets the duration in milliseconds of each Bluetooth LE scan cycle to look for beacons.
     * This function is used to setup the period before calling {@link #bind} or when switching
     * between background/foreground. To have it effect on an already running scan (when the next
     * cycle starts), call {@link #updateScanPeriods}
     *
     * @param p
     */
    public void setBackgroundScanPeriod(long p) {
        backgroundScanPeriod = p;
    }

    /**
     * Sets the duration in milliseconds spent not scanning between each Bluetooth LE scan cycle when no ranging/monitoring clients are in the foreground
     *
     * @param p
     */
    public void setBackgroundBetweenScanPeriod(long p) {
        backgroundBetweenScanPeriod = p;
    }

    private void verifyServiceDeclaration() {
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent( context, BeaconService.class);
        List resolveInfo =
                packageManager.queryIntentServices(intent,
                        PackageManager.MATCH_DEFAULT_ONLY);
        if (resolveInfo.size() == 0) {
            throw new ServiceNotDeclaredException();
        }
    }

    public static String getDistanceModelUpdateUrl() {
        return distanceModelUpdateUrl;
    }

    /**
     * Allows disabling check of manifest for proper configuration of service.  Useful for unit
     * testing
     *
     * @param disabled
     */
    public static void setsManifestCheckingDisabled(boolean disabled) {
        manifestCheckingDisabled = disabled;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public boolean isBackgroundMode() {
        return backgroundMode;
    }

    public ConcurrentMap<BeaconConsumer, ConsumerInfo> getConsumers() {
        return consumers;
    }

    public Messenger getServiceMessenger() {
        return serviceMessenger;
    }

    public void setServiceMessenger(Messenger serviceMessenger) {
        this.serviceMessenger = serviceMessenger;
    }

    public BeaconLeScanCallback getBeaconCallback() {
        return beaconCallback;
    }

    public void setBeaconCallback(BeaconLeScanCallback beaconCallback) {
        this.beaconCallback = beaconCallback;
    }

    public long getForegroundScanPeriod() {
        return foregroundScanPeriod;
    }

    public long getForegroundBetweenScanPeriod() {
        return foregroundBetweenScanPeriod;
    }

    public long getBackgroundScanPeriod() {
        return backgroundScanPeriod;
    }

    public long getBackgroundBetweenScanPeriod() {
        return backgroundBetweenScanPeriod;
    }
}
