package com.stepleaderdigital.reveal;

import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by bobby on 5/11/17.
 */

public class ModelSpecificDistanceUpdater extends AsyncTask<Void, Void, Void> {

    private static final String TAG = "ModelSpecificDistanceUpdater";
    private Context mContext;
    private DistanceConfigFetcher mDistanceConfigFetcher;
    private CompletionHandler mCompletionHandler;

    @Override
    protected Void doInBackground(Void... params) {
        mDistanceConfigFetcher.request();
        if (mCompletionHandler != null) {
            mCompletionHandler.onComplete(mDistanceConfigFetcher.getResponseString(), mDistanceConfigFetcher.getException(), mDistanceConfigFetcher.getResponseCode());
        }
        return null;
    }

    public ModelSpecificDistanceUpdater(Context context, String urlString, CompletionHandler completionHandler) {
        mContext = context;
        mDistanceConfigFetcher = new DistanceConfigFetcher(urlString, getUserAgentString());
        mCompletionHandler = completionHandler;
    }

    private String getUserAgentString() {
        return "Android Beacon Library;"+getVersion()+";"+getPackage()+";"+getInstallId()+";"+getModel();
    }
    private String getPackage() {
        return mContext.getPackageName();
    }
    private String getModel() {
        return AndroidModel.forThisDevice().toString();
    }
    private String getInstallId() {
        return Reveal.Utils.getDeviceId(mContext); // Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
    private String getVersion() {
        return BuildConfig.VERSION_NAME;
    }

    interface CompletionHandler {
        void onComplete(String body, Exception exception, int code);
    }

}
