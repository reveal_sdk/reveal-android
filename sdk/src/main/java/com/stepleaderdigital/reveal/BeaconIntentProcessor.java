package com.stepleaderdigital.reveal;

import android.app.IntentService;
import android.content.Intent;

/**
 * Created by bobby on 5/10/17.
 */

public class BeaconIntentProcessor extends IntentService {
    public BeaconIntentProcessor() {
        super("BeaconIntentProcessor");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Reveal.RevealLogger.d( "Received a beacon intent: " + intent );
    }
}
