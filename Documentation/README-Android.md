# Reveal Mobile Android SDK
#### (Version 1.3.23 published 3/31/17) 

Reveal Mobile is a native mobile SDK that allows developers to provide tailored personas to their ad network based on a user's location, beacon interactions and device details.

## Usage
The Reveal Mobile setup should happen only once when the application launches.  You need to subclass android.app.Application (adding the appropriate AndroidManifest.xml information to use your subclass) and adding your code in the onCreate() method.

### Simple Setup
```java
public class BaseApplication extends Application {
...
@Override
public void onCreate() {
    super.onCreate();
    
    // Setup the Reveal Mobile SDK pointed at production with no debug features
    Reveal reveal = Reveal.getInstance();
    reveal.setAPIKey("<YOUR API KEY>");

    //Start Reveal Mobile service. This will start start discovering beacons.  The Reveal Mobile SDK requires you to pass in the Application variable to receive boot loading broadcasts
    reveal.start(this);
    ...
}
```

### Restarting

The SDK now manages restarting for you, so you no longer need to do anything special. You can safely call start or restart, but this is no longer a requirement.
 
### Testing Setup

If you need to troubleshoot issues related to the reveal API, you can set the debug flag to get log entries to help you run down the issue.

```java
public class BaseApplication extends Application {
...
@Override
public void onCreate() {
    super.onCreate();
    
    // Get a reference to the SDK object
    Reveal reveal = Reveal.getInstance();
    reveal.setAPIKey("<YOUR API KEY>");
        
    // Turn on optional debug logging
    reveal.setDebug(true);

    // Start the SDK.  The SDK will contact the server for further config info
    // and start monitoring for beacons.  The Reveal SDK requires you to pass in the Application variable to receive boot loading broadcasts
    reveal.start(this);
    ...
}
```

### Accessing the Personas
In order for your app to deliver ads using Reveal Mobile audience data, you must pass the personas to your ad server.


```java
// Get personas to send to ad network.  Personas array is an array of NSString* objects.
List<String> personas = Reveal.getInstance().getPersonas();
```
Accessing the personas is a lightweight call to the SDK that does not involve a network request. You should call it to get the most current available list of personas every time you build an ad view.

### DFP Integration
If using DoubleClick (DFP), ensure the personas pass to the "reveal" custom targeting key in DFP as a custom parameter. Reveal Mobile will work with your ad trafficking team to create the custom targeting key in DFP. Below is sample code you can insert into your ad call to DFP. *Please, make sure you revise this code sample to fit your existing ad call! [Here's Google's documentation](https://developers.google.com/mobile-ads-sdk/docs/dfp/android/targeting#custom_targeting) to help out.*
```java
import com.google.android.gms.ads.mediation.admob.AdMobExtras;

AdView adview = new AdView(activity);
adview.setAdSize(size);
adview.setAdUnitId(unit);

adview.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, size.getHeightInPixels(activity)));

AdRequest.Builder ab = new AdRequest.Builder();

// add reveal
String revealApiKey = activity.getString(R.string.revealApiKey);
if (revealApiKey != null && revealApiKey.length() > 0) {
    List<String> personas = Reveal.getInstance().getPersonas();

    Bundle b = new Bundle();
    b.putString ("reveal", StringUtils.join(personas, ","));

    ab.addNetworkExtras(new AdMobExtras(b));
}


// add location
double lat;
double lon;
float accuracy;

// populate lat, lon, accuracy with location information from the system

Location loc = new Location ("");
loc.setLatitude(lat);
loc.setLongitude(lon);
loc.setAccuracy(accuracy);

ab.setLocation (loc);

AdRequest adrequest = ab.build();
adview.loadAd(adrequest);
```

### Detecting beacons:

```java


        Reveal.getInstance().getBeaconService().setBeaconFoundListener(new Reveal.OnBeaconFoundListener() {
            @Override
            public void onBeaconDiscovered(Map<String, Object> beaconProperties) {
                Reveal.RevealLogger.d( "MainActivity got a beacon:\n" + beaconProperties );
            }

            @Override
            public void onBeaconLeave(String beaconIdentifier) {

            }
        });
```
### Capturing app openings
Reveal Mobile sends information about the user location each time the application becomes active on your main activity.  This is done using the following code in the onCreate() function of the activity that is the activity you want to track.
```java
// Send current information to the Reveal servers.  
Reveal.getInstance().restart(getApplicationContext());
```
### Independent Beacon Scanning
Should your usage of the SDK require independent control over beacon detection, the appropriate flag is:
```java
setIsBeaconScanningEnabled
```
If your usage of the SDK requires the ability to control beacon detection while the app is in the background, the appropriate flag is:
```java
setIsBackgroundScanningEnabled
```

## Project setup
Reveal Mobile requires the following updates to your AndroidManifest.xml file:
### Permissions/Features
```xml
<uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED"/>
    <uses-permission-sdk-23 android:name="android.permission.ACCESS_COARSE_LOCATION"/>
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    
<!-- Android suggests putting this in the manifest even though false is the default value -->
<uses-feature android:name="android.hardware.bluetooth_le" android:required="false"/>
```
### Components
If you are not using the suggested integration method of using the aar dependency through maven central, then the following components should be added to your AndroidManifest.xml.  If you are using the manual integration, then you will need to add these entries for beacon scanning to occur.
```xml
<receiver android:name="com.stepleaderdigital.reveal.StartupBroadcastReceiver">
            <intent-filter>
                <action android:name="android.intent.action.BOOT_COMPLETED"/>
                <action android:name="android.intent.action.ACTION_POWER_CONNECTED"/>
                <action android:name="android.intent.action.ACTION_POWER_DISCONNECTED"/>
            </intent-filter>
        </receiver>

        <service android:enabled="true"
            android:exported="false"
            android:isolatedProcess="false"
            android:label="beacon"
            android:name="com.stepleaderdigital.reveal.BeaconService"
            />

        <service android:name="com.stepleaderdigital.reveal.BeaconIntentProcessor"
            android:enabled="true"
            android:exported="false"
            />
```

## Requirements

Reveal Mobile requires at least Android 4.3 to run.  Beacon scanning requires a Bluetooth LE capable device to operate.

Reveal Mobile requires an account with our system and a valid API key.  You may request a production key from the Reveal Mobile team at developersupport@revealmobile.com.

## Installation
### Maven Central Integration
    dependencies {
      compile 'com.stepleaderdigital.reveal:reveal:1.4.1@aar'
    }
    
### Manual Integration
Reveal Mobile is available as an Android .jar file. Place [this .jar file](http://www.revealmobile.com/s/reveal-127.jar) and then follow the instructions below.

Integration using gradle requires the following to be added to your build.gradle file:

    dependencies {
      compile fileTree(dir: 'libs', include: ['*.jar'])
    }

It also requires that you use the 'flatDir' option in your repositories section.

    repositories {
      mavenCentral()
      flatDir {
        dirs 'libs'
      }
    }
    
## Design notes

### Startup
The sample app determines if you are being launched in the foreground like an app, or in the background as a service. This check is performed in the MyApplication.onCreate method. It checks to see if there is an activity for this app being displayed, if there isn't then it starts the service assuming it is being run in a headless state.

If an activity is detected (the splash screen) then the startup activity is launched allowing the user to select the settings for testing purposes. If no selection is made within 30 seconds, it automatically selects the same settings you selected the last time (or the defaults if this is the first run).

### Beacon persistence 

Beacon events are saved automatically and reloaded at startup so that you will no longer get repeat beacon discoveries if you kill the app and restart it within an hour.

## Author

Reveal Mobile, support@revealmobile.com, 
Sean Doherty, sean.doherty@crosscomm.net,
Bobby Skinner, bobby.skinner@crosscomm.com

## License

Reveal Mobile is available under the MIT license. See the LICENSE file for more info.

## Version Log
- 1.3.23, 4.31.17 - 
- 1.2.7, 1.21.16 - Resolved location conflict with Google Play Services and beacon bumps
- 1.2.5 & 1.2.6 - Internal versions for testing
- 1.2.4, 12.30.15 - Removed unnecessary permissions
- 1.2.3, 12.17.15 - Fixed bug with background beacon scanning
- 1.2.2, 12.15.15 - Added persona caching, improved server communication, Eddystone support
- 1.2.1, 11.12.15 - Improved stability of SDK when ranging for beacon transmission values 
- 1.1.10, 10.02.15 - Backwards compatability for older Android API version support
- 1.1.9, 09.05.15 - Updated advertiser ID routine
- 1.1.8, 08.25.15 - Additional flags included to support independent scanning of beacons and apps
- 1.1.7, 08.03.15 - Added controls to support scanning for beacons and apps independently
- 1.1.3, 06.15.15 - Improved support for Titanium developers to recognize different classes of app startups
- 1.1.2, 06.02.15 - Support for AltBeacon standard
- 1.1.1, 02.11.15 - Improved beacon scanning and battery life
