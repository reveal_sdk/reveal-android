# Reveal Android SDK Release Notes

## Release 1.4.20

* Code simplifications and fixes to concurrent modification exceptions and removal of code that would cause bluetooth access issues on certain samsung phones.

## **Previous Releases:**

## Release 1.4.19

* Updating Batch sending routine on timeout

##### Known issues:

* Currently you must specify android.permission.INTERNET in your android manifest, this should be inherited as part of the SDK manifest.
* Currently the SDK requests: android.permission.ACCESS_FINE_LOCATION, android.permission.ACCESS_NETWORK_STATE and android.permission.CHANGE_WIFI_STATE in the SDK but they should really be defined in the customer's settings.

## Release 1.4.18

* iBeacon identifier includes major/minor

## Release 1.4.17

* Fixed early dwell time delivery

## Release 1.4.15

* Detect new estimote beacons

## Release 1.4.14

* 158 Handle long dwelling beacon events.
* 163 send an open beacon event on the initial encounter of a beacon

## Release 1.4.13

* 162 Implement software filter for google play locations

## Release 1.4.11

* Parcelable crash fix

## Release 1.4.10

* Added memory debugs
* added AdId to startup screen

## Release 1.4.8

* Don't send until location permissions

## Release 1.4.7

* 150 Make the sdk endpoint configurable in the test app so we don't have to build a sandbox / prod / accuweather version
* 151 Some Apple devices showing up as beacons

## Release 1.4.6

* 148 Roadtrippers Crash Report

## Release 1.4.5

* Remote logging capability

## Release 1.4.4

* 144 Collect Device Make and Model
* 145 Remove Device ID from both SDK's, keep IDFA only

## Release 1.4.3

* 133 Handle Android Doze mode

## Release 1.4.2

* Bug fixes and merge with main branch

## Release 1.4.1

* 125 change IDFA code to use new one instead of the stored one, unless you can't get a new one then use the old.

## Release 1.4.0
* 112 Remove AltBeacon dependency and use our own scanning mechanism.

## Release 1.3.33
* Restart before calling start will no longer cause a crash

## Release 1.3.31

* Addressed issue where each activitiy restarted the SDK

## Release 1.3.29
* Preview no alt beacon
* 119 Configuration option for selecting API endpoint

## Release 1.3.30
* Updated to play services 10.2.6
* Only call registerDevice on activity events transitioning from background to foreground

## Release 1.3.29

* 119 Configuration option for selecting API endpoint
* Updated to play services 10.2.6

## Release 1.3.26

* 122 Leave beacon not reported

## Release 1.3.25

* 116 Knox security exception

## Release 1.3.24

* Fixed several customer reported crashes

## Release 1.3.23

* 103 Build in hooks in SDK to handle Foredroid code for foreground/background events.

This verson no longer requires the user to setup anything on returning to the forground. The start
and restart methods now require an Application object instead of a Context object. Since it turns
out that our documentation already told them to pass the application most will not notice this
change.

The isInBackground property is now read only.

The sample project has the Foreground class removed since we no longer require the user to deal
with background state. While they may choose to continue to use it for their own puropses, they

## Release 1.3.22

* Fixed a crash related to Parcel

## Release 1.3.21

* 90 Capture beacon exits

##### Schema changes:

The schema was updated to match the iOS see example below

```json
{
   "device_id":"f24b7f2fa8e559587a1536066cc3bcb",
   "os":"android",
   "bluetooth_enabled":true,
   "supports_ble":true,
   "con_type":"wifi",
   "sdk_version":"1.3.19",
   "version":"7.0",
   "locale":"en_US",
   "app_id":"com.stepleaderdigital.reveal.android.sample",
   "app_version":"1.3.19",
   "time_zone":"America\/New_York",
   "idfa":"02d158cc-57d9-4e90-a87e-69fcec00f73c",
   "locationSharingEnabled":true,
   "beacons":[
      {
         "txpower":0,
         "beacon_uuid":"54300003",
         "beacon_mac":"06:0F:10:C1:FD:A8",
         "beacon_proximity":"far",
         "beacon_accuracy":-1,
         "beacon_type":"SecureCast",
         "beacon_rssi":0,
         "beacon_vendor":181,
         "dwellTime":28,
         "lastSeenTime":1490200545,
         "discoveryTime":1490200517,
         "type":"SecureCast",
         "rssi":0,
         "proximity":"far",
         "accuracy":-1,
         "identity":"54300003",
         "location":{
            "lat":35.9956557,
            "lon":-78.9020807,
            "time":36554,
            "altitude":0,
            "accuracy":21.552000045776367,
            "provider":"fused",
            "speed":0,
            "age":-36.55399990081787
         },
         "address":{
            "street":"211 West Main Street",
            "city":"Durham",
            "state":"North Carolina",
            "zip":"27701",
            "country":"United States"
         },
         "beacon_payload":"zUcvwkmsdqLimHbwWvGrUQ==",
         "beacon_vendor_key":1412431875,
         "key":1412431875
      },
      {
         "name":"EST",
         "txpower":0,
         "beacon_uuid":"edd1ebeac04e5defa017",
         "beacon_major":"0xc3a3832928de",
         "beacon_mac":"C3:A3:83:29:28:DE",
         "beacon_proximity":"near",
         "beacon_accuracy":-1,
         "beacon_type":"Eddystone",
         "beacon_rssi":0,
         "beacon_vendor":0,
         "dwellTime":97,
         "lastSeenTime":1490200544,
         "discoveryTime":1490200447,
         "type":"Eddystone",
         "rssi":0,
         "proximity":"near",
         "accuracy":-1,
         "identity":"edd1ebeac04e5defa017",
         "major":"0xc3a3832928de",
         "location":{
            "lat":35.9956557,
            "lon":-78.9020807,
            "time":36776,
            "altitude":0,
            "accuracy":21.552000045776367,
            "provider":"fused",
            "speed":0,
            "age":-36.775999784469604
         },
         "address":{
            "street":"211 West Main Street",
            "city":"Durham",
            "state":"North Carolina",
            "zip":"27701",
            "country":"United States"
         }
      },
      {
         "txpower":0,
         "beacon_uuid":"299d0003",
         "beacon_mac":"06:2F:19:29:29:C8",
         "beacon_proximity":"far",
         "beacon_accuracy":-1,
         "beacon_type":"SecureCast",
         "beacon_rssi":0,
         "beacon_vendor":181,
         "dwellTime":29,
         "lastSeenTime":1490200546,
         "discoveryTime":1490200517,
         "type":"SecureCast",
         "rssi":0,
         "proximity":"far",
         "accuracy":-1,
         "identity":"299d0003",
         "location":{
            "lat":35.9956557,
            "lon":-78.9020807,
            "time":37068,
            "altitude":0,
            "accuracy":21.552000045776367,
            "provider":"fused",
            "speed":0,
            "age":-37.067999839782715
         },
         "address":{
            "street":"211 West Main Street",
            "city":"Durham",
            "state":"North Carolina",
            "zip":"27701",
            "country":"United States"
         },
         "beacon_payload":"RX00FfwaxP4xw4tIEtn2vg==",
         "beacon_vendor_key":698155011,
         "key":698155011
      }
   ],
   "currentSSID":"CrossComm",
   "currentBSSID":"82:2a:a8:4b:5c:52"
}
```

## Release 1.3.19

* Upgraded to google play services 10 (location only)
* Upgraded to altbeacon 2.8.1

###### Customizing the configuration:

##### In the application manifest:
The following optional permissions should be considered to fully utilize the SDK:
```xml
	<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
	// you may specify coarse location if you prefer

	<uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
    <uses-permission android:name="android.permission.INTERNET" />
	<uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
	<uses-feature android:name="android.hardware.bluetooth_le" android:required="false"/>
```

###### Inherited gradle and manifest settings

You should not need to update your gradle or manifest files with these settings since they will be merged with the ones built into the SDK. However, these are provided so that you can see the compatibility requirements:

gradle script:
```ruby

android {
    compileSdkVersion 25
    buildToolsVersion "25.0.0"

    defaultConfig {
        minSdkVersion 14
        targetSdkVersion 25
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_6
        targetCompatibility JavaVersion.VERSION_1_6
    }
}

dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    compile 'com.android.support:appcompat-v7:25.0.0'
    compile 'com.google.android.gms:play-services-location:10.2.0'
    compile 'org.altbeacon:android-beacon-library:2.8.1'

}
```

The following permissions should be requested:
```xml
    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
```


## Release 1.3.18

This release was not included in ongoing releases

## Release 1.3.17

* Upgraded beacon notification to match iOS Dwell time release

##### Completed:

* Added additional logging
* Fixed a pre-Android M target issue

Implement BeaconFoundListener use onBeaconDiscovered to be notified of new beacons. The data for the beacon will be available in the beacon map. 

**NOTE: The current version returns the internal beacon object in the "beacon" key, this is for use by the test application in development and will eventually be removed.**

```java
Reveal.BeaconService service = Reveal.getInstance().getBeaconService();

        service.setBeaconFoundListener(new Reveal.OnBeaconFoundListener() {
            @Override
            public void onBeaconDiscovered(Map<String, Object> beacon) {
                Reveal.RevealLogger.d( "FOUND BEACON: " + beacon );
            }

            @Override
            public void onBeaconLeave(String beaconIdentifier) {
                Reveal.RevealLogger.d( "LOST BEACON: " + beaconIdentifier );
            }
        });
```

##### Known issues

## Release 1.3.16

##### Completed:

* Added additional logging
* Fixed a pre-M target issue


## Release 1.3.15

##### Completed:

* Added additional logging

## Release 1.3.14

##### Completed:

* 105 Crash when scanning wifi with location disabled

## Release 1.3.13

##### Completed:

* 102 Implement addition of WiFi data to packets on all events
  
  The batch event has been modified to returrn the current SSD and BSSID fields for the active network
  ```json
  {
  	...
  	"beacons":
  	[
  		{
  			"currentSSID":"CrossComm",
  			"currentBSSID":"82:2a:a8:4b:5c:52",
  			,"beacon_type":"PebbleBee",
  			...
  		}
  	]
  	...
  }
  ```


## Release 1.3.12

##### Completed:

* 100 Null pointer exception crashes
* 99 Beacon property inconsistencies (part II)
* 77 Investigate WIFI data we can access

     *preliminary data returned in /batch API call under the key "networks"*
     
	 ***NOTE: This may change moving forward, and should be viewed as a R&D only feature for now***
     
```json
{
	...
	"networks":
	[
	    {
		    "address":
		    {
			    "street":"123 Main Street",
			    "city":"Cary",
			    "state":"North Carolina",
			    "zip":"27513",
			    "country":"United States"
		    },
		    "SSID":"myNetwork",
		    "BSSID":"ac:b3:13:c5:0b:25",
		    "capabilities":"[WPA2-PSK-CCMP][WPS][ESS]",
		    "level":-45,
		    "frequency":5240
	    }
	]
	...
}
```

The network sending will only be monitored if you add the following permissions to your manifest:

```XML
    <android:uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
    <android:uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
```

*Hopefully future versions will not require CHANGE WIFI STATE*

## Release 1.3.11

##### Completed:

* 96 Crash - empty response from server
* 99 Beacon property inconsistencies
* Statistics added to sample application

## Release 1.3.9

##### Completed:

* 86 Collect batch events

## Release 1.3.7

##### Completed:

* 94 sdk_version is empty on android sdk API calls

## Release 1.3.6

##### Completed:

* 92 Crash when starting beaconScanning

## Release 1.3.5

##### Completed:

* 75 Crash related to location

## Release 1.3.4

##### Completed:

* 64 Support for Tile Beacons

## Release 1.3.3

##### Completed:

* 74 Crash on loadFoundBeacons 
* 76 Crash on double decode

## Release 1.3.2

##### Completed:

* Fixed environment variable issue 

## Release 1.3.1

##### Completed:

* 58 Create beacon listeners accessible to SDK.
* 65 Make location timeouts configurable from server
* 72 Estimote beacons not detected if configured for eddystone mode

## Release 1.3.0

##### Completed:

* 60 Respect cache_ttl on Android

## Release 1.2.20

##### Completed:

* 47 Add the address lookup to Android /info requests
* 57 Look at the age attribute on Android to bring in sync with iOS
* 59 Restore setIsBackgroundScanningEnabled

## Release 1.2.19

* 46 add sdk_version attribute to the Android info request
* 53 High battery use


## Release 1.2.18

##### Completed:

* Updated Google play services 8.4.0
* Updated android 6 permissions

## Release 1.2.17

##### Completed:
* 35 Switch to Android studio 2.0
* fixed beacon age issue

## Release 1.2.16

##### Completed:
* 34 We need to incorporate securecast_manufacturer_codes to select which securecast beacons to search for

## Release 1.2.10

##### Completed:
* 33 Swirl beacon should use the combined key and instance fields as reveal_beacon_key

## Release 1.2.9

##### Completed:
* 28 Send the SDK version on every event
* 25 Verify and match beacon information between iOS and Android

## Release 1.2.8

##### Completed:
* 16 Detect Swirl beacons
* 17 Improve the UUID caching mechanism to include timestamp
* 18 Clean up and normalize data being sent on info and bump events

## Release 1.2.7

##### Completed:

* 1 remove ranging code
* 2 Add connection type to response
* 4 Eddystone integration
* 5 Store beacons to prefs
* 6 Delay sending of beacon until location either success or failure
* 8 convert major/minor to strings


