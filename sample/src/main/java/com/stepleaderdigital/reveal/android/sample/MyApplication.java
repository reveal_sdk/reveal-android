package com.stepleaderdigital.reveal.android.sample;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.stepleaderdigital.reveal.Reveal;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import io.fabric.sdk.android.Fabric;

/**
 * Created by seandoherty on 5/27/15.
 */
public class MyApplication extends Application implements  Reveal.UserLogListener, Reveal.OnLocationFoundListener, Application.ActivityLifecycleCallbacks {

    private boolean alreadyRunning = false;

    private final CopyOnWriteArrayList<LogEntry> logItems = new CopyOnWriteArrayList<LogEntry>();

    private final ConcurrentHashMap<String, Reveal.BluetoothItem> devices = new ConcurrentHashMap<>();


    private final CopyOnWriteArrayList<Reveal.GlobalLocation> locations = new CopyOnWriteArrayList<Reveal.GlobalLocation>();

    private int activityCount = 0;

    private MainActivity currentActivity = null;

    private final int maxTrackedLocations = 15;

    public static final String PREFS_NAME = "revealStartupServer";
    public static final String SERVER_TYPE_KEY = "service_type";

    private final int MY_PERMISSIONS_ACCESS_FINE_LOCATION = 0;
    private final int MY_PERMISSIONS_CHANGE_WIFI_STATE = 1;
    private final int MY_PERMISSIONS_ACCESS_WIFI_STATE = 2;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        registerActivityLifecycleCallbacks(this);

        RevealLogAgent.getInstance().addGroup( "COMM" );
        RevealLogAgent.getInstance().addGroup( "STATE" );
        RevealLogAgent.getInstance().addGroup( "INIT" );
        RevealLogAgent.getInstance().addGroup( "DEVICE" );
        RevealLogAgent.getInstance().addGroup( "ERROR" );
        RevealLogAgent.getInstance().addGroup( "WARNING" );
        RevealLogAgent.getInstance().setEnabled( true );


        String adId = Reveal.AdUtils.getAdvertisingId( this );
        if (!TextUtils.isEmpty(adId)) {
            RevealLogAgent.getInstance().setAdId( adId );
        }

        Reveal.log( "Starting session", "BEACON" );

        Reveal.logMemoryUsage( "onCreate");

        //Create Reveal configuration.
        Reveal reveal = Reveal.getInstance();
        reveal.setLogger( this );
        String apiKey = BuildConfig.REVEAL_API_KEY;
        String serviceTypeString = BuildConfig.REVEAL_SERVER_TYPE; // System.getProperty( "REVEAL_SERVER_TYPE" );

        // Get the last type we selected and start up the same way
        SharedPreferences sharedPreferences = getSharedPreferences( PREFS_NAME, Context.MODE_PRIVATE );
        serviceTypeString = sharedPreferences.getString( SERVER_TYPE_KEY, serviceTypeString);

        Reveal.ServiceType serviceType = Reveal.ServiceType.PRODUCTION;

        if (serviceTypeString != null) {
            serviceTypeString = serviceTypeString.toLowerCase();

            if (serviceTypeString.equals("sandbox"))
                serviceType = Reveal.ServiceType.SANDBOX;
            else if (serviceTypeString.equals("rvlservicetypesandbox")) // this provided for compatibility with iOS environment variables
                serviceType = Reveal.ServiceType.SANDBOX;
            else if (serviceTypeString.equals("accuweather")) {
                Reveal.getInstance().setAPIEndpointBase("https://accuweather-sdk.revealmobile.com/");
            }
        }

        reveal.setDebug(true);
        // NOTE: the gradle script returns the string null if the key is not provided so this
        //       looks a little strange but is correct.
        if (apiKey.equals( "null" ) || apiKey.equals("PutYourAPIKeyHere!")) {
            if ( serviceType == Reveal.ServiceType.SANDBOX )
                apiKey = "53921489d442b031018220bf";
            else
                apiKey = "53921489d442b031018220bf";
        }

        reveal.setAPIKey( apiKey );
        reveal.setServiceType( serviceType );
        reveal.setIsBackgroundScanningEnabled( true );

        reveal.setLocationFoundListener( this );


        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
        String deviceName = myDevice.getName();
        Reveal.log( "Started Application\nVERSION=" + Reveal.getVersion()
                + "\nNAME=" + deviceName
                + "\nREVEAL_API_KEY=" + apiKey
                + "\nREVEAL_SERVER_TYPE="
                + serviceTypeString, "INIT" );

        // Check for running as a service here. If we were started as a foreground app
        // there will be an activity launched, however if we were launched as a service
        // there wont be. In the former case we want to launch the startup screen so you
        // can select options, otherwise just start up with the standard options
        if ( !isMainActivityRunning( "com.stepleaderdigital.reveal.android.sample" ) ) {
            Reveal.log( "Application is running as a service so starting reveal now", "STATE");
            startReveal();
        }
        else {
            Reveal.log( "Application is not running as a service so starting so launching the startup screen", "STATE");

        }
    }

    public void askForLocationPermission( int delayMS ) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new Handler( Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (currentActivity != null) {
                            currentActivity.askForLocationPermission();
                        }
                    }
                });
            }
        }, delayMS);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        RevealLogAgent.getInstance().logMessage( "Activity " + activity.getLocalClassName() + " resumed", "INFO", "STATE");

        Reveal.log( "onActivityResumed " + Reveal.getInstance().getDeviceState(getApplicationContext() ), "STATE" );
    }

    @Override
    public void onActivityPaused(Activity activity) {
        RevealLogAgent.getInstance().logMessage( "Activity " + activity.getLocalClassName() + " paused", "INFO", "STATE");

        Reveal.log( "onActivityPaused " + Reveal.getInstance().getDeviceState( getApplicationContext() ), "STATE" );
    }

    @Override
    public void onActivityStarted(Activity activity) {
        RevealLogAgent.getInstance().logMessage( "Activity " + activity.getLocalClassName() + " started", "INFO", "STATE");
        activityCount++;
        Reveal.logMemoryUsage( "onActivityStarted");

        Reveal.log( "onActivityStarted " + Reveal.getInstance().getDeviceState( getApplicationContext()  ), "STATE" );
    }

    @Override
    public void onActivityStopped(Activity activity) {
        RevealLogAgent.getInstance().logMessage( "Activity " + activity.getLocalClassName() + " stopped", "INFO", "STATE");
        if ( --activityCount < 1 ) {
            RevealLogAgent.getInstance().logMessage( "All activities stopped, so the application is possibly being removed from memory", "INFO", "STATE");
            activityCount = 0; // we should never go below 0, but lets be safe
        }

        Reveal.log( "onActivityStopped " + Reveal.getInstance().getDeviceState( getApplicationContext() ), "STATE" );
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Reveal.log( "onActivityCreated " + Reveal.getInstance().getDeviceState( getApplicationContext() ), "STATE" );

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Reveal.log( "onActivitySaveInstanceState " + Reveal.getInstance().getDeviceState( getApplicationContext() ), "STATE" );

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

        Reveal.logMemoryUsage( "onActivityDestroyed");
        Reveal.log( "onActivityDestroyed " + Reveal.getInstance().getDeviceState( getApplicationContext() ), "STATE" );

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Reveal.log( "Low memory reported", "ERROR", "STATE" );
        Reveal.logMemoryUsage( "LOW MEMORY");

        Reveal.getInstance().setStatus( Reveal.STATUS_MEMORY, 2, "Low Memory reported ");
    }

    @Override
    public void onTrimMemory( int level ) {
        super.onTrimMemory(level);
        Reveal.log( "Memory trim requested", "WARNING", "STATE" );
        Reveal.logMemoryUsage( "TRIM MEMORY level: " + level );

        Reveal.getInstance().setStatus( Reveal.STATUS_MEMORY, 0, "Memory trim in process ");

        // levels are 5, 10, 20, 40 80
        if ( level > 60 ) {
            cleanup();
        }
    }

    public void cleanup() {
        // Remove all of the elements we maintain for testing, since memory is low
        this.devices.clear();
        this.logItems.clear();
        this.locations.clear();


        Reveal.log( "Cleared logs to release memory", "WARNING", "STATE" );
    }

    public void startReveal() {
        // Uncomment this line to test proper handling of erroneous restart calls
        //reveal.restart( getApplicationContext() );
        Reveal.getInstance().start( this );
    }

    public void addBeacon( Map<String, Object> beacon )
    {
        Reveal.BluetoothItem info = null;
        String address = beacon.get( "bluetoothAddress" ).toString();

        if ( this.devices.containsKey( address ) ) {
            info = this.devices.get( address );
            this.devices.remove(address );
        }
        else {
            info = new Reveal.BluetoothItem();

            info.setAddress( address );
        }

        HashMap<String, Object> hash = new HashMap<String, Object>( beacon );

        info.setUserData( hash );

        this.devices.put( address, info );

        Reveal.log( "Added device " + address + "\n" + info, "BEACON" );
    }

    public boolean isMainActivityRunning(String packageName) {
        boolean result = false;

        ActivityManager activityManager = (ActivityManager) getSystemService (Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasksInfo = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (int i = 0; i < tasksInfo.size(); i++) {
            String name = tasksInfo.get(i).baseActivity.getPackageName();
            if ( name.equals(packageName) ) {
                Reveal.log( "Activity " + name + " is running" );
                result = true;
            }
        }

        return result;
    }

    public ArrayList<Reveal.BluetoothItem> discoveredBeacons() {
        ArrayList<Reveal.BluetoothItem> result = new ArrayList<Reveal.BluetoothItem>();

        for( Reveal.BluetoothItem item : this.devices.values() ) {
            if ( item.getUserData() != null )
                result.add( item );
        }

        return result;
    }

    @Override
    public void logMessage( String message, String type, String group ) {
        synchronized ( this.logItems ) {
            RevealLogAgent.getInstance().logMessage( message, type, group );

            LogEntry entry = new LogEntry();

            entry.setTimeStamp(new Date());
            entry.setMessage(message);
            entry.setGroup( group );
            entry.setType(type);

            this.logItems.add(entry);

            while ( this.logItems.size() > 250 ) {
                this.logItems.remove( 0 );
            }
        }
    }

    @Override
    public void logDevice(Reveal.BluetoothItem info, Reveal.PDUiBeacon beacon ) {

        if ( info != null) {
            synchronized (this.devices) {
                HashMap<String, Object> hash = null;

                if (this.devices.containsKey(info.getAddress())) {
                    Reveal.BluetoothItem old = this.devices.get(info.getAddress());
                    hash = old.getUserData();
                    this.devices.remove(info.getAddress());
                }
                else {
                    String text = "Device information: " + info.getAddress() + " (" + info.getName() + ")";
                    Map<String,Object> data = info.getBeacon();

                    if ( data != null ) {
                        text = text + "\nBeacon Info:";
                        for( String key : data.keySet() ) {
                            text = text + "\n  " + key + ": " + data.get(key);
                        }

                         ArrayList<Reveal.PDU> pdus = info.getPdus();
                        if ( pdus != null ) {
                            text = text + "\n  PDUs:";
                            for( Reveal.PDU pdu : pdus ) {
                                text = text + "\n    " + pdu;
                            }
                        }
                    }

                    if ( beacon != null ) {
                        text = text + "\n  Beacon: ";


                        text = text + "\n    UUID: " + beacon.getUuid();
                        text = text + "\n    Major: " + beacon.getMajor();
                        text = text + "\n    Minor: " + beacon.getMinor();
                        text = text + "\n    Flags: " + beacon.getFlags();
                        text = text + "\n    MFG: " + beacon.getManufacturerCode();

                        ArrayList<Byte> payload = beacon.getServiceData();

                        if ( payload != null )
                        text = text + "\n    serviceData (" + payload.size() + "): " + payload;
                    }

                    RevealLogAgent.getInstance().logMessage( text, "DEBUG", "DEVICE" );
                }

                if ( beacon != null ) {
                    info.setUserData(hash);
                    this.devices.put(info.getAddress(), info);
                }

            }
        }
    }

    @Override
    public void onLocationDiscovered(Reveal.GlobalLocation location ) {
        locations.add( location );

        while ( locations.size() > maxTrackedLocations ) {
            locations.remove( 0 );
        }
    }

    public CopyOnWriteArrayList<LogEntry> getLogItems() {
        return logItems;
    }

    public ConcurrentHashMap<String, Reveal.BluetoothItem> getDevices() {
        return devices;
    }

    public CopyOnWriteArrayList<Reveal.GlobalLocation> getLocations() {
        return locations;
    }

    public MainActivity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(MainActivity currentActivity) {
        this.currentActivity = currentActivity;
    }
}
