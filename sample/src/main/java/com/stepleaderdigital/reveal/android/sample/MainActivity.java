package com.stepleaderdigital.reveal.android.sample;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.crashlytics.android.Crashlytics;
import com.stepleaderdigital.reveal.Reveal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

public class MainActivity extends StatusAwareActivity implements Reveal.OnBeaconFoundListener {

    private final int MY_PERMISSIONS_ACCESS_FINE_LOCATION = 0;
    private final int MY_PERMISSIONS_CHANGE_WIFI_STATE = 1;
    private final int MY_PERMISSIONS_ACCESS_WIFI_STATE = 2;

    protected  ListView listView;
    protected Timer timer = new Timer();
    protected TimerTask updateTable;
    ArrayAdapter<String> adapter;
    CopyOnWriteArrayList<String> listItems=new CopyOnWriteArrayList<String>();
    Reveal reveal = Reveal.getInstance();

    protected StatusIndicator bluetoothStatus;
    protected StatusIndicator networkStatus;
    protected StatusIndicator locationStatus;
    protected StatusIndicator memoryStatus;

//    private class OrderAdapter extends ArrayAdapter<Order> {
//
//        private ArrayList<Order> items;
//
//        public OrderAdapter(Context context, int textViewResourceId, ArrayList<Order> items) {
//            super(context, textViewResourceId, items);
//            this.items = items;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            View v = convertView;
//            if (v == null) {
//                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                v = vi.inflate(R.layout.beacon_row, null);
//            }
//            Order o = items.get(position);
//            if (o != null) {
//                TextView tt = (TextView) v.findViewById(R.id.toptext);
//                TextView bt = (TextView) v.findViewById(R.id.bottomtext);
//                if (tt != null) {
//                    tt.setText("Name: "+o.getOrderName());                            }
//                if(bt != null){
//                    bt.setText("Status: "+ o.getOrderStatus());
//                }
//            }
//            return v;
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        MyApplication app = (MyApplication) getApplication();
        app.setCurrentActivity( this );

        setContentView(R.layout.activity_main);

        setupView();

        reveal = Reveal.getInstance();

        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.beaconTable);

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        String adid = Reveal.Utils.getAdvertisingId( this );
        if ( adid == null )
            adid = "Unknown user";
        Crashlytics.setUserIdentifier( adid );

        updateTable = new UpdateTableTask();
        timer.scheduleAtFixedRate(updateTable, 0, 500 );
    }

    public void setupBeaconFoundCallBack() {

        Reveal.BeaconService beaconService = Reveal.getInstance().getBeaconService();

        if ( beaconService != null ) {
            beaconService.setBeaconFoundListener( this );
        }

    }

    public void forceCrash() {
        throw new RuntimeException("This is a crash");
    }


    @Override
    public void onBeaconDiscovered(Map<String, Object> beaconProperties) {
        Reveal.RevealLogger.v("MainActivity got a beacon:\n" + beaconProperties);
        MyApplication app = (MyApplication) MainActivity.this.getApplication();

        app.addBeacon( beaconProperties );
    }

    @Override
    public void onBeaconLeave(String beaconIdentifier) {
        Reveal.RevealLogger.d( "MainActivity lost " + beaconIdentifier );

        Reveal.log( "Added device " + beaconIdentifier, "BEACON" );
    }

    public void askForLocationPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this,  Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_ACCESS_FINE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.

            }

        }
        else
            havePermisionForLocation();

        if ( ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED ) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_WIFI_STATE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,  new String[]{ Manifest.permission.ACCESS_WIFI_STATE}, MY_PERMISSIONS_ACCESS_WIFI_STATE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        if ( ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_STATE) != PackageManager.PERMISSION_GRANTED ) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CHANGE_WIFI_STATE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,  new String[]{ Manifest.permission.CHANGE_WIFI_STATE}, MY_PERMISSIONS_CHANGE_WIFI_STATE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    public void havePermisionForLocation() {

        MyApplication app = (MyApplication) MainActivity.this.getApplication();

        //app.startReveal();

        setupBeaconFoundCallBack();

        // The following code generates an error to see if the log tester detects it
//        Reveal.RevealLogger.e( "This is a fake error to test the tools" );
//
//        try {
//            int a = 1;
//
//            a--;
//
//            int b = 14 / a;
//        } catch ( Exception e ) {
//            Reveal.RevealLogger.w( "Test warning followed by a stack trace for the exception: " + e );
//            e.printStackTrace();
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Reveal.log( "onRequestPermissionsResult( " + requestCode + ", " + permissions.toString() + ", " + grantResults.toString() + " )", "STATE" );

        switch (requestCode) {
            case MY_PERMISSIONS_ACCESS_FINE_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    havePermisionForLocation();

                }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        List<String> personas = Reveal.getInstance().getPersonas();
        Reveal.RevealLogger.v(personas);

        Intent intent;

        switch ( id ) {
            case R.id.action_logs:
                intent = new Intent(this, LogActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_devices:
                intent = new Intent(this, DeviceListActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_release_all:
                Reveal.getInstance().getDwellManager().releaseAll();
                return true;

            case R.id.action_statistics:
                intent = new Intent(this, StatisticsActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_map:
                intent = new Intent(this, MapActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class UpdateTableTask extends TimerTask {

        public void run() {
            MyApplication app = (MyApplication) MainActivity.this.getApplication();
            ArrayList<Reveal.BluetoothItem> beacons = app.discoveredBeacons();

            final ArrayList<String> list=new ArrayList<>();

            list.clear();

            for(Reveal.BluetoothItem item : beacons ) {
                Map<String, Object> userData = item.getUserData();

                if ( userData != null ) {
                    String text = "";
                    Reveal.RevealBeacon beacon = (Reveal.RevealBeacon) userData.get( "beacon" );

                    if ( beacon != null ) {
                        text = text + beacon;

                        if ( beacon.getDistance() > 0.0 )
                            text = text + String.format( " - %.1fft", beacon.getDistance());

                        if ( beacon.getDwellTime() > 0.0 )
                            text = text + String.format( " - %.1fs", beacon.getDwellTime());

                        Reveal.GlobalLocation currentLocation = beacon.getLocation();

                        if ( currentLocation != null ) {
                            text = text + String.format( "\nLAT: %.8f, LON: %.8f", currentLocation.getLatitude(), currentLocation.getLongitude() );
                        }

                        if (item.getPdus() != null)
                            text = text + "\n" + item;

                        list.add(text);
                    }
                }
            }

            runOnUiThread(new Runnable() {
                public void run() {
                    adapter.clear();
                    adapter.addAll( list );
                    updateStatuses();

                    setupBeaconFoundCallBack();
                }

            });

            }
        }
}
