package com.stepleaderdigital.reveal.android.sample;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.stepleaderdigital.reveal.Reveal;

/**
 * Created by bobby on 9/1/17.
 */

public class StatusAwareActivity extends AppCompatActivity {

    protected StatusIndicator bluetoothStatus;
    protected StatusIndicator networkStatus;
    protected StatusIndicator locationStatus;
    protected StatusIndicator memoryStatus;
    protected StatusIndicator scanStatus;

    protected void setupView() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if ( toolbar != null )
            setSupportActionBar(toolbar);

        // Show status bar
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            // Show Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
            decorView.setSystemUiVisibility(uiOptions);
        }

        bluetoothStatus = new StatusIndicator( this, Reveal.STATUS_BLUETOOTH, R.id.fabBluetooth);
        bluetoothStatus.add( Reveal.STATUS_FAIL, "#ff8080", R.drawable.ic_bluetooth_disabled_black_24dp );
        bluetoothStatus.add( Reveal.STATUS_SUCCEED, "#80ff80", R.drawable.ic_bluetooth_black_24dp );
        bluetoothStatus.add( Reveal.STATUS_IN_PROGRESS, "#ffff80", R.drawable.ic_bluetooth_black_24dp );

        locationStatus = new StatusIndicator( this, Reveal.STATUS_LOCATION, R.id.fabLocation);
        locationStatus.add( Reveal.STATUS_FAIL, "#ff8080", R.drawable.ic_location_off_black_24dp);
        locationStatus.add( Reveal.STATUS_SUCCEED, "#80ff80", R.drawable.ic_location_on_black_48dp );
        locationStatus.add( Reveal.STATUS_IN_PROGRESS, "#ffff80", R.drawable.ic_location_on_black_48dp );

        networkStatus = new StatusIndicator( this, Reveal.STATUS_NETWORK, R.id.fabWeb);
        networkStatus.add( Reveal.STATUS_FAIL, "#ff8080", R.drawable.ic_cloud_off_black_24dp);
        networkStatus.add( Reveal.STATUS_SUCCEED, "#80ff80", R.drawable.ic_cloud_queue_black_24dp );
        networkStatus.add( Reveal.STATUS_IN_PROGRESS, "#ffff80", R.drawable.ic_cloud_queue_black_24dp );

        memoryStatus = new StatusIndicator( this, Reveal.STATUS_MEMORY, R.id.fabMemory);
        memoryStatus.add( Reveal.STATUS_FAIL, "#ff8080", R.drawable.ic_memory_black_24dp);
        memoryStatus.add( Reveal.STATUS_SUCCEED, "#80ff80", R.drawable.ic_memory_black_24dp );
        memoryStatus.add( Reveal.STATUS_IN_PROGRESS, "#ffff80", R.drawable.ic_memory_black_24dp );

        scanStatus = new StatusIndicator( this, Reveal.STATUS_SCAN, R.id.fabScan);
        scanStatus.add( Reveal.STATUS_FAIL, "#ff8080", R.drawable.ic_perm_scan_wifi_black_24dp);
        scanStatus.add( Reveal.STATUS_SUCCEED, "#80ff80", R.drawable.ic_perm_scan_wifi_black_24dp );
        scanStatus.add( Reveal.STATUS_IN_PROGRESS, "#ffff80", R.drawable.ic_perm_scan_wifi_black_24dp );

        updateStatuses();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Intent intent;

        switch ( id ) {

            case R.id.action_beacons:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_logs:
                intent = new Intent(this, LogActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_devices:
                intent = new Intent(this, DeviceListActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_release_all:
                Reveal.getInstance().getDwellManager().releaseAll();
                return true;

            case R.id.action_statistics:
                intent = new Intent(this, StatisticsActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_map:
                intent = new Intent(this, MapActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateStatuses() {

        if ( bluetoothStatus != null )
            bluetoothStatus.update();

        if ( locationStatus != null )
            locationStatus.update();

        if ( networkStatus != null )
            networkStatus.update();

        if ( memoryStatus != null )
            memoryStatus.update();

        if ( scanStatus != null )
            scanStatus.update();
    }

}
