package com.stepleaderdigital.reveal.android.sample;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

public class LogActivity extends StatusAwareActivity {
    protected ListView listView;
    ArrayAdapter<String> adapter;
    CopyOnWriteArrayList<String> listItems=new CopyOnWriteArrayList<String>();
    protected Timer timer = new Timer();
    protected TimerTask updateTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_log);

        setupView();
    }

    @Override
    protected void setupView() {
        super.setupView();

        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.log_list);

        this.adapter = new ArrayAdapter<String>(this,
        android.R.layout.simple_list_item_1,
        listItems);

        // Assign adapter to ListView
        listView.setAdapter(this.adapter);

        MyApplication app = (MyApplication) this.getApplication();

        updateTable = new UpdateTableTask();
        timer.scheduleAtFixedRate(updateTable, 0, 5000 );
    }

    public void update( CopyOnWriteArrayList<LogEntry> logList ) {
        final ArrayList<String> entries = new ArrayList<String>();
        ArrayList<LogEntry> list = new ArrayList<>();

        list.addAll( logList );

        for( int i= list.size()-1 ; i>=0 ; i-- ) {
            entries.add( list.get( i ).toString() );
        }



        runOnUiThread(new Runnable() {
            public void run() {
                LogActivity.this.adapter.clear();
                LogActivity.this.adapter.addAll( entries );
                //adapter.notifyDataSetChanged();
            }

        });
    }

    class UpdateTableTask extends TimerTask {

        public void run() {
            MyApplication app = (MyApplication) LogActivity.this.getApplication();
            LogActivity.this.update( app.getLogItems() );
        }
    }


}
