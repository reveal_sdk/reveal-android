package com.stepleaderdigital.reveal.android.sample;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.stepleaderdigital.reveal.Reveal;

import java.util.Timer;
import java.util.TimerTask;

public class StartupActivity extends AppCompatActivity {
    private final int autoStartDelay = 30;
    protected Timer timer = new Timer();
    protected TimerTask updateStartButton;
    protected int count = autoStartDelay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Reveal.log( "startup screen loaded", "STATE");
        setContentView(R.layout.activity_startup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        count = autoStartDelay;

        String adId = Reveal.AdUtils.getAdvertisingId( this );

        if ( adId == null )
            adId = "unknown";

        TextView versionLabel = (TextView) findViewById(R.id.versionTextView);
        versionLabel.setText( "SDK: " + Reveal.getInstance().getVersion() );

        TextView adidLabel = (TextView) findViewById(R.id.adidTextView);
        adidLabel.setText( "ADID: " + adId );

        final RadioButton sandbox = (RadioButton) findViewById(R.id.radioSandbox );
        final RadioButton production = (RadioButton) findViewById(R.id.radioProduction );
        final RadioButton accuweather = (RadioButton) findViewById(R.id.radioAccuweather );

        final RadioButton startNow = (RadioButton) findViewById(R.id.radioOnStart );
        final RadioButton early = (RadioButton) findViewById(R.id.radioEarly );
        final RadioButton late = (RadioButton) findViewById(R.id.radioLate );
        final RadioButton never = (RadioButton) findViewById(R.id.radioNever );

//        final RadioButton backgroundOn = (RadioButton) findViewById(R.id.radioBackgroundScanningOn );
//        final RadioButton backgroundOff = (RadioButton) findViewById(R.id.radioBackgroundScanningOff );

        Button button = (Button) findViewById(R.id.startButton);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Reveal.log( "User made selection from startup screen", "STATE");
                StartupActivity.this.startPressed();

            }
        });

        String url = Reveal.getInstance().getAPIBaseURL();
        if ( url != null ) {
            if ( url.compareTo( "https://accuweather-sdk.revealmobile.com/" ) == 0 )
                accuweather.setChecked( true );
            else if ( url.compareTo( "http://sandboxsdk.revealmobile.com" ) == 0 )
                sandbox.setChecked( true );
            else
                production.setChecked( true );
        }
startNow.setChecked( true );

        updateStartButton = new UpdateStartButton();
        timer.scheduleAtFixedRate(updateStartButton, 0, 1000 );
    }

    protected void startPressed() {
        Reveal.log( "Starting main application", "STATE");
        final RadioButton sandbox = (RadioButton) findViewById(R.id.radioSandbox );
        final RadioButton production = (RadioButton) findViewById(R.id.radioProduction );
        final RadioButton accuweather = (RadioButton) findViewById(R.id.radioAccuweather );

        final RadioButton startNow = (RadioButton) findViewById(R.id.radioOnStart );
        final RadioButton early = (RadioButton) findViewById(R.id.radioEarly );
        final RadioButton late = (RadioButton) findViewById(R.id.radioLate );
        final RadioButton never = (RadioButton) findViewById(R.id.radioNever );

        String type = "production";

        // set the server
        if ( accuweather.isChecked() ) {
            Reveal.getInstance().setAPIEndpointBase("https://accuweather-sdk.revealmobile.com/");
            type = "accuweather";
        }
        else if ( sandbox.isChecked() ) {
            Reveal.getInstance().setServiceType(Reveal.ServiceType.SANDBOX);
            type = "sandbox";
        }
        else
            Reveal.getInstance().setServiceType( Reveal.ServiceType.PRODUCTION );

        // start the SDK (Note this code could be started here, but since it needs that
        // application anyway it made sense to leave the start there and call it since
        // moving it hear would not simplify things and this keeps all of the logic in
        // the application class which makes sense
        MyApplication app = (MyApplication) StartupActivity.this.getApplication();

        app.startReveal();

        if ( startNow.isChecked() )
            app.askForLocationPermission( 50 );
        else if ( early.isChecked() )
            app.askForLocationPermission( 2000 );
        else if ( late.isChecked() )
            app.askForLocationPermission( 30000 );
        else if ( never.isChecked() )
            app.askForLocationPermission( Integer.MAX_VALUE ); // technically not never but may as well be

        // save type for next time
        SharedPreferences settings = getSharedPreferences( MyApplication.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString( MyApplication.SERVER_TYPE_KEY, type);
        editor.apply();

        // launch the main activity and begin displaying beacon info
        Intent intent;
        intent = new Intent( StartupActivity.this, MainActivity.class);
        startActivity(intent);

        if ( timer != null ) {
            timer.cancel();
        }
    }

    class UpdateStartButton extends TimerTask {

        public void run() {
            if ( StartupActivity.this.count > 1 ) {
                StartupActivity.this.count--;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Button button = (Button) findViewById( R.id.startButton );
                        button.setText("Start (" + StartupActivity.this.count + ")");
                    }
                });

            }
            else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        StartupActivity.this.startPressed();
                    }
                });
            }

        }
    }
}
