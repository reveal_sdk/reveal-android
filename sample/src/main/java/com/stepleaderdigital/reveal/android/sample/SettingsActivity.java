package com.stepleaderdigital.reveal.android.sample;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.stepleaderdigital.reveal.Reveal;

public class SettingsActivity extends StatusAwareActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        final RadioButton radio0 = (RadioButton) findViewById(R.id.radio0);
        final RadioButton radio5 = (RadioButton) findViewById(R.id.radio5);
        final RadioButton radio60 = (RadioButton) findViewById(R.id.radio60);
        final RadioButton radio80 = (RadioButton) findViewById(R.id.radio80);

        int level = Reveal.getInstance().getSimulateMemoryWarning();

        switch ( level ) {
            case 5:
                radio5.setChecked( true );
                break;

            case 60:
                radio60.setChecked( true );
                break;

            case 80:
                radio80.setChecked( true );
                break;

            default:
                radio0.setChecked( true );
                break;
        }

        setupView();
    }

    @Override
    protected void setupView() {

        super.setupView();


        Button button = (Button) findViewById(R.id.issueWarning);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                final RadioButton radio0 = (RadioButton) findViewById(R.id.radio0);
                final RadioButton radio5 = (RadioButton) findViewById(R.id.radio5);
                final RadioButton radio60 = (RadioButton) findViewById(R.id.radio60);
                final RadioButton radio80 = (RadioButton) findViewById(R.id.radio80);

                if (radio0.isChecked())
                    Reveal.getInstance().setSimulateMemoryWarning(0);
                else if (radio5.isChecked())
                    Reveal.getInstance().setSimulateMemoryWarning(5);
                else if (radio60.isChecked())
                    Reveal.getInstance().setSimulateMemoryWarning(60);
                else if (radio80.isChecked()) {
                    Reveal.getInstance().setSimulateMemoryWarning(80);
                    MyApplication app = (MyApplication) getApplication();

                    app.cleanup();
                }
            }
        });

        Button stopButton = (Button) findViewById( R.id.stopScanning );

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Reveal.getInstance().stop();
            }
        });

        Button startScanningButton = (Button) findViewById( R.id.startScanning );

        startScanningButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Reveal.getInstance().start( getApplication() );
            }
        });
    }

}
