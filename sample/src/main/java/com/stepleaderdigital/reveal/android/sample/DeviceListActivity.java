package com.stepleaderdigital.reveal.android.sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.stepleaderdigital.reveal.Reveal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class DeviceListActivity extends AppCompatActivity {
    protected ListView listView;
    ArrayAdapter<String> adapter;
    CopyOnWriteArrayList<String> listItems=new CopyOnWriteArrayList<>();
    protected Timer timer = new Timer();
    protected TimerTask updateTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.device_list);

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        updateTable = new UpdateTableTask();
        timer.scheduleAtFixedRate(updateTable, 0, 6000 );
    }

    public void update( ConcurrentHashMap<String, Reveal.BluetoothItem> deviceList ) {
        final ArrayList<String> entries = new ArrayList<String>();

        HashMap<String, Reveal.BluetoothItem> list = new HashMap<String, Reveal.BluetoothItem>();
        list.putAll( deviceList );

        for( Reveal.BluetoothItem device : list.values() ) {
            String text = device.getAddress();

            if ( device.getName() != null )
                text = text + " : " + device.getName();

                text = text + "\n" + device.getBeaconType();

                if ( device.getUuids() != null ) {
                    text = text + "\n";
                    for (String uuid : device.getUuids()) {
                        text = text + uuid + " ";
                    }
                }

            text = text + device.getHex();

            List<Reveal.PDU> pdus = device.getPdus();
            if (device.getPdus() != null) {
                if ( device.getPdus().size() > 0 ) {
                    text = text + "\nPDU: " + pdus.size();

                    for ( Reveal.PDU pdu : pdus ) {
                        text = text + "\n   " + pdu;
                    }

                    text = text + "";
                }
            }

            entries.add( text );
        }

        runOnUiThread(new Runnable() {
            public void run() {

                adapter.clear();
                adapter.addAll( entries );
            }

        });
    }

    class UpdateTableTask extends TimerTask {

        public void run() {
            MyApplication app = (MyApplication) DeviceListActivity.this.getApplication();
            DeviceListActivity.this.update( app.getDevices() );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        List<String> personas = Reveal.getInstance().getPersonas();
        Reveal.RevealLogger.v(personas);

        Intent intent;

        switch ( id ) {
            case R.id.action_logs:
                intent = new Intent(this, LogActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_beacons:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_devices:
                return true;

            case R.id.action_statistics:
                intent = new Intent(this, StatisticsActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_settings:
                return true;

            case R.id.action_release_all:
                Reveal.getInstance().getDwellManager().releaseAll();
                return true;

            case R.id.action_map:
                intent = new Intent(this, MapActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
