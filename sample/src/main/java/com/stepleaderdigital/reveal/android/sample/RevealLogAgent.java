package com.stepleaderdigital.reveal.android.sample;

import com.github.tony19.timber.loggly.LogglyTree;

import java.util.ArrayList;
import java.util.Locale;

import timber.log.Timber;

/**
 * Created by bobby on 7/20/17.
 */

public class RevealLogAgent {
    private String token; // =  "9c4dd9d2-84f7-49ef-875c-38ba1b5b020d";

    private boolean enabled = false;
    private ArrayList<String> groups = new ArrayList<String>();
    private boolean includeError = true;
    private boolean includeWarning = true;
    private boolean includeInfo = true;
    private boolean includeDebug = true;
    private boolean includeVerbose = true;
    private String adId = "Unknown";

    private static RevealLogAgent sharedInstance = null;
    public static synchronized RevealLogAgent getInstance() {
        if (sharedInstance == null)
            sharedInstance = new RevealLogAgent();
        return sharedInstance;
    }

    public RevealLogAgent() {
        super();

        // Plant a LogglyTree for logging with Timber. Use tony19's demo
        // token for testing purposes only. Get your own token from
        // https://www.loggly.com/docs/customer-token-authentication-token/
        this.token = BuildConfig.REVEAL_LOG_KEY;

        //coming from undefined variable it is a string called "null" instead of a null

        if (this.token.equalsIgnoreCase("null"))
            this.token = "381d4a5a-01ed-45b2-8eaa-1fe319efe9c1";

        LogglyTree tree = new LogglyTree(token);

        if ( this.token != null ) {
            Timber.plant( tree );

            Timber.tag("RevealAndroid");
        }
    }

    public void logMessage( String message, String type, String group ) {
        if ( isEnabled() && ( this.token != null ) ) {
            if ( groups.contains( group.toLowerCase(Locale.ENGLISH) ) ) {
                String typeUpper = type.toUpperCase(Locale.ENGLISH );



                if  ( includeError && ( typeUpper.compareTo( "ERROR" ) == 0 ) ) {
                    String logString = adId + " [" + group.toUpperCase(Locale.ENGLISH) + "] " + message;
                    Timber.e( logString );
                }
                else if  ( includeWarning && ( typeUpper.compareTo( "WARNING" ) == 0 ) ) {
                    String logString = adId + " [" + group.toUpperCase(Locale.ENGLISH) + "] " + message;
                    Timber.w( logString );
                }
                else if  ( includeInfo && ( typeUpper.compareTo( "INFO" ) == 0 ) ) {
                    String logString = adId + " [" + group.toUpperCase(Locale.ENGLISH) + "] " + message;
                    Timber.i( logString );
                }
                else if  ( includeDebug && ( typeUpper.compareTo( "DEBUG" ) == 0 ) ) {
                    String logString = adId + " [" + group.toUpperCase(Locale.ENGLISH) + "] " + message;
                    Timber.d( logString );
                }
                else if  ( includeVerbose && ( typeUpper.compareTo( "VERVOSE" ) == 0 ) ) {
                    String logString = adId + " [" + group.toUpperCase(Locale.ENGLISH) + "] " + message;
                    Timber.v( logString );
                }
            }
        }
    }

    public void addGroup( String group ) {
        groups.add( group.toLowerCase(Locale.ENGLISH) );
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isIncludeError() {
        return includeError;
    }

    public void setIncludeError(boolean includeError) {
        this.includeError = includeError;
    }

    public boolean isIncludeWarning() {
        return includeWarning;
    }

    public void setIncludeWarning(boolean includeWarning) {
        this.includeWarning = includeWarning;
    }

    public boolean isIncludeInfo() {
        return includeInfo;
    }

    public void setIncludeInfo(boolean includeInfo) {
        this.includeInfo = includeInfo;
    }

    public boolean isIncludeDebug() {
        return includeDebug;
    }

    public void setIncludeDebug(boolean includeDebug) {
        this.includeDebug = includeDebug;
    }

    public boolean isIncludeVerbose() {
        return includeVerbose;
    }

    public void setIncludeVerbose(boolean includeVerbose) {
        this.includeVerbose = includeVerbose;
    }

    public ArrayList<String> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<String> groups) {
        this.groups = groups;
    }

    public String getAdId() {
        return adId;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }
}
