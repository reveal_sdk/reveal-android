package com.stepleaderdigital.reveal.android.sample;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.stepleaderdigital.reveal.Reveal;

import java.util.HashMap;

/**
 * Created by bobby on 8/22/17.
 */

public class StatusIndicator {

    private Activity context = null;
    private String statusString = null;
    private FloatingActionButton button = null;
    private HashMap<Integer, String> colors = new HashMap<Integer, String>();
    private HashMap<Integer,Integer> icons = new HashMap<Integer,Integer>();

    StatusIndicator( Activity context, String statusName, FloatingActionButton button ) {
        super();

        this.context = context;
        this.statusString = statusName;
        this.button = button;
    }


    StatusIndicator(Activity context, String statusName, Integer buttonId ) {
        super();

        this.context = context;
        this.statusString = statusName;


        this.button = (FloatingActionButton) context.findViewById(buttonId);
    }

    public void update() {
        try {
            if (button != null) {

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Reveal.RevealStatus currentStatus = Reveal.getInstance().getStatus(statusString);

                        if (currentStatus != null) {
                            Snackbar.make(view, currentStatus.getMessage(), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    }
                });
            }

            Reveal.RevealStatus status = Reveal.getInstance().getStatus(statusString);


            if ((status != null) && (button != null) && (icons != null) && (colors != null)) {
                if ((colors.size() > 0) && (icons.size() > 0)) {
                    int image = icons.get(0);
                    String color = colors.get(0);

                    if (icons.containsKey(status.getStatus()))
                        image = icons.get(status.getStatus());

                    if (colors.containsKey(status.getStatus()))
                        color = colors.get(status.getStatus());

                    button.setImageResource(image);
                    button.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(color)));

                }
            }
        }
        catch ( Exception e ) {
            Reveal.log( "StatusIndicator.update exception: " + e, "WARN" );
            e.printStackTrace();
        }
    }

    public void add( int stateNumber, String color ) {
        colors.put( stateNumber, color );
        if ( icons.size() > 0 )
            icons.put( stateNumber, icons.get( 0 ));
    }

    public void add( int stateNumber, String color, int icodId ) {
        colors.put( stateNumber, color );
        icons.put( stateNumber, icodId );
    }

    public Activity getContext() {
        return context;
    }

    public void setContext(Activity context) {
        this.context = context;
    }

    public String getStatusString() {
        return statusString;
    }

    public void setStatusString(String statusString) {
        this.statusString = statusString;
    }

    public HashMap<Integer, String> getColors() {
        return colors;
    }

    public void setColors(HashMap<Integer, String> colors) {
        this.colors = colors;
    }

    public HashMap<Integer, Integer> getIcons() {
        return icons;
    }

    public void setIcons(HashMap<Integer, Integer> icons) {
        this.icons = icons;
    }

    public FloatingActionButton getButton() {
        return button;
    }

    public void setButton(FloatingActionButton button) {
        this.button = button;
    }
}
