package com.stepleaderdigital.reveal.android.sample;

import java.util.Date;

/**
 * Created by bobby on 11/28/16.
 */

public class LogEntry {
    private String message;
    private String type;
    private String group;
    private Date timeStamp;

    @Override
    public String toString() {
        return message + "\n[" + group + "] " + timeStamp.toString();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
