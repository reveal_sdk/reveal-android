package com.stepleaderdigital.reveal.android.sample;

import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.stepleaderdigital.reveal.Reveal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

public class MapActivity extends AppCompatActivity
        implements OnMapReadyCallback {
    private GoogleMap nmap;
    private ArrayList<Reveal.GlobalLocation> currentLocations = new ArrayList<>();
    private ArrayList<Reveal.BluetoothItem> currentBeacons = new ArrayList<>();
    protected Timer timer = new Timer();
    protected TimerTask updateMap;
    protected GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Reveal.log( "Main activity screen loaded", "STATE");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Show status bar
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            // Show Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
            decorView.setSystemUiVisibility(uiOptions);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        updateMap = new MapActivity.UpdateTableTask();
        timer.scheduleAtFixedRate(updateMap, 0, 5000 );
    }

    /**
     * Manipulates the map when it's available.
     * The API invokes this callback when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user receives a prompt to install
     * Play services inside the SupportMapFragment. The API invokes this method after the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapActivity.this.map = googleMap;
        Location location = Reveal.Utils.getLastKnownLocation( MapActivity.this );

        if ( location != null ) {
            LatLng point = new LatLng(location.getLatitude(), location.getLongitude());
            googleMap.addMarker(new MarkerOptions().position(point).title("You are here"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 18));
        }
    }

    public void setToCurrentLocation() {
        GoogleMap googleMap = this.map;

        if ( googleMap != null ) {
            googleMap.clear();
            int count = 0;

            if ( this.currentLocations != null ) {

                for (Reveal.GlobalLocation marker : MapActivity.this.currentLocations) {
                    LatLng point = new LatLng(marker.getLatitude(), marker.getLongitude());

                    final DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                    googleMap.addMarker(new MarkerOptions().position(point).title("@ " + dateFormat.format(marker.getTime()) + " (" + ++count + ")"));
                }

                for (Reveal.BluetoothItem item : MapActivity.this.currentBeacons) {
                    Map<String, Object> beacon = item.getUserData();

                    if (beacon != null) {
                        String name = (String) beacon.get("beacon_type");
                        if (name == null) {
                            name = (String) beacon.get("name");
                            if (name == null)
                                name = "unknown";
                        }

                        String label = (String) beacon.get("identifier");

                        Map<String, Object> location = (Map<String, Object>) beacon.get("location");

                        if ((location != null) && (label != null)) {
                            Double lon = (Double) location.get("lon");
                            Double lat = (Double) location.get("lat");

                            if ((lon != null) && (lat != null)) {
                                count++;

                                // because the beacons are all reported at the location of the user
                                // graphically display them in a small radius around the user so they
                                // can be seen individually, instead of all stacked on top of one
                                // another
                                switch (count % 8) {
                                    case 0:
                                        lat = lat + 0.00001;
                                        break;
                                    case 1:
                                        lat = lat - 0.00001;
                                        break;
                                    case 2:
                                        lon = lon - 0.00001;
                                        break;
                                    case 3:
                                        lon = lon + 0.00001;
                                        lat = lat + 0.00001;
                                        break;
                                    case 4:
                                        lat = lat + 0.00001;
                                        lon = lon + 0.00001;
                                        break;
                                    case 5:
                                        lat = lat - 0.00001;
                                        lon = lon - 0.00001;
                                        break;
                                    case 6:
                                        lon = lon - 0.00001;
                                        lat = lat - 0.00001;
                                        break;
                                    case 7:
                                        lon = lon + 0.00001;
                                        break;
                                }
                                LatLng point = new LatLng(lat, lon);

                                Reveal.log("BEACON data: " + beacon, "DEBUG");
                                googleMap.addMarker(new MarkerOptions()
                                        .position(point)
                                        .title(name + " " + label)
                                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                                );
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        List<String> personas = Reveal.getInstance().getPersonas();
        Reveal.RevealLogger.v(personas);

        Intent intent;

        switch ( id ) {
            case R.id.action_logs:
                intent = new Intent(this, LogActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_devices:
                intent = new Intent(this, DeviceListActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_settings:
                return true;

            case R.id.action_release_all:
                Reveal.getInstance().getDwellManager().releaseAll();
                return true;

            case R.id.action_statistics:
                intent = new Intent(this, StatisticsActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_beacons:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void update( CopyOnWriteArrayList<Reveal.GlobalLocation> logList,
                        ArrayList<Reveal.BluetoothItem> beacons ) {
        ArrayList<Reveal.GlobalLocation> list = new ArrayList<>();
        ArrayList<Reveal.BluetoothItem> beaconList = new ArrayList<>();

        list.addAll( logList );
        beaconList.addAll( beacons );

        // save for refreshes
        this.currentLocations = list;
        this.currentBeacons = beaconList;

        runOnUiThread(new Runnable() {
            public void run() {
                setToCurrentLocation();
            }

        });
    }

    class UpdateTableTask extends TimerTask {

        public void run() {
            MyApplication app = (MyApplication) MapActivity.this.getApplication();
            MapActivity.this.update( app.getLocations(), app.discoveredBeacons() );

        }
    }
}
