package com.stepleaderdigital.reveal.android.sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.stepleaderdigital.reveal.Reveal;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends AppCompatActivity {
    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Reveal.log( "Splash screen loaded", "STATE");

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Intent intent;
                intent = new Intent( SplashScreen.this, StartupActivity.class);

                timer.cancel();

                Reveal.log( "Splash screen timer expired", "STATE");
                startActivity(intent);

            }

        }, 1500, 1500);
    }
}
