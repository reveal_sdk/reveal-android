package com.stepleaderdigital.reveal.android.sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.stepleaderdigital.reveal.Reveal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class StatisticsActivity extends AppCompatActivity {
    protected ListView listView;
    ArrayAdapter<String> adapter;
    CopyOnWriteArrayList<String> listItems=new CopyOnWriteArrayList<String>();
    protected Timer timer = new Timer();
    protected TimerTask updateTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.statistics_list);

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);

        // Assign adapter to ListView
        listView.setAdapter(adapter);

        updateArray();
        MyApplication app = (MyApplication) this.getApplication();

        updateTable = new UpdateTableTask();
        timer.scheduleAtFixedRate(updateTable, 0, 2500 );
    }

    class UpdateTableTask extends TimerTask {

        public void run() {
            updateArray();
        }
    }

    public void updateArray() {
        ConcurrentHashMap<String, Integer> successStats = Reveal.getInstance().getSuccessStats();
        ArrayList<String> keys = new ArrayList<String>( successStats.keySet() );
        final ArrayList<String> results = new ArrayList<String>();
        Collections.sort( keys );

        for( String key : keys ) {
            Integer count = successStats.get( key );
            String line = key + " - " + count;
            String rate = "";
            Date now = new Date();
            long seconds = (now.getTime()-Reveal.getInstance().getStartTime().getTime())/1000;

            if ( seconds > 10.0 ) {

                String unit = "day";

                double perUnit = count / (long) ( (seconds / 86400.0 ) + 1.0);

                if ( seconds > 180.0 ) {
                    if ( perUnit >= 60.0  ) {
                        unit = "hour";
                        perUnit = count / (long)( ( seconds / 3600.0 ) + 1.0);
                    }

                    if ( perUnit >= 200.0 ) {
                        unit = "min";
                        perUnit = count / (long)( (  seconds / 60.0 ) + 1.0);
                    }

                    if ( perUnit >= 1000.0 ) {
                        unit = "sec";
                        perUnit = count / seconds;
                    }
                }

                line = line + " : " + String.format ("%.3f", perUnit)   + "/" + unit;
            }

            results.add( line );
        }

        runOnUiThread(new Runnable() {
            public void run() {
                StatisticsActivity.this.adapter.clear();
                StatisticsActivity.this.adapter.addAll( results );
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        List<String> personas = Reveal.getInstance().getPersonas();
        Reveal.RevealLogger.v(personas);

        Intent intent;

        switch ( id ) {
            case R.id.action_logs:
                intent = new Intent(this, LogActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_beacons:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_devices:
                intent = new Intent(this, DeviceListActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_settings:
                return true;

            case R.id.action_statistics:
                return true;

            case R.id.action_release_all:
                Reveal.getInstance().getDwellManager().releaseAll();
                return true;

            case R.id.action_map:
                intent = new Intent(this, MapActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
