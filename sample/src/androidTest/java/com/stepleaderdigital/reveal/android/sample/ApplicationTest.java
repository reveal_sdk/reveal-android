package com.stepleaderdigital.reveal.android.sample;

import android.app.Application;
import android.test.ApplicationTestCase;

import com.stepleaderdigital.reveal.Reveal;
//import org.junit.Test;
//import java.util.regex.Pattern;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertTrue;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);

        Reveal.RevealLogger.d( "This is a debug test" );
    }

//    @Test
//    public void emailValidator_CorrectEmailSimple_ReturnsTrue() {
//        assertThat(EmailValidator.isValidEmail("name@email.com"), is(true));
//    }
}